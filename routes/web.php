<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login','AuthController@login')->name('auth.login');
Route::post('/postlogin','AuthController@postlogin')->name('auth.postlogin');
Route::get('/logout','AuthController@logout');
Route::get('/register', 'AuthController@register')->name('register');
Route::post('/register', 'AuthController@postregister')->name('postregister');



Route::group(['middleware' => ['auth','checkRole:admin,validator,reviewer,mahasiswa']], function(){
	Route::get('/dashboard','DashboardController@index');
});


Route::group(['middleware' => ['auth','checkRole:admin']], function(){
	//Route untuk menu borang
	Route::get('/st1','SatuController@index');
	Route::get('/tabel','SatuController@tabel');

	Route::get('/st2','DuaController@index');
	Route::get('/st3','TigaController@index');
	Route::get('/st4','EmpatController@index');
	Route::get('/st5','LimaController@index');
	Route::get('/st6','EnamController@index');
	Route::get('/st7','TujuhController@index');
	Route::get('/st8','DelapanController@index');
	Route::get('/st9','SembilanController@index');


	//Route untuk Menu Referensi
	Route::get('/halstan','StandarrController@index');
	Route::get('/tstandar','StandarrController@tst');
	Route::post('/tstandar/store', 'StandarrController@store');
	Route::get('/halstan/edit/{id}', 'StandarrController@edit')->name('standar.edit');
	Route::put('/halstan/update/{id}', 'StandarrController@update')->name('standar.update');
	Route::get('/halstan/hapus/{id}', 'StandarrController@delete')->name('standar.delete');


	Route::get('/halbaku','BakumutuController@index');
	Route::get('/tbaku','BakumutuController@tbaku');
	Route::post('/tbaku/store', 'BakumutuController@store');
	Route::get('/halbaku/edit/{id}', 'BakumutuController@edit')->name('baku_mutu.edit');
	Route::put('/halbaku/update/{id}', 'BakumutuController@update')->name('baku_mutu.update');
	Route::get('/halbaku/hapus/{id}', 'BakumutuController@delete')->name('baku_mutu.delete');


	Route::get('/jenisdok1','JenisdokController@index');
	Route::get('/jenisdok2','JenisdokController@jenis');
	Route::post('/jenisdok2/store', 'JenisdokController@store');
	Route::get('/haljenis/edit/{id}','JenisdokController@edit')->name('jenis_dokumen.edit');
	Route::put('/haljenis/update/{id}', 'JenisdokController@update')->name('jenis_dokumen.update');
	Route::get('/haljenis/hapus/{id}', 'JenisdokController@delete')->name('jenis_dokumen.delete');


	Route::get('/lokasidok1','LokasidokController@index');
	Route::get('/lokasidok2','LokasidokController@tlokasi');
	Route::post('/lokasidok2/store', 'LokasidokController@store');
	Route::get('/lokasidok1/edit/{id}', 'LokasidokController@edit')->name('lokasi_dokumen.edit');
	Route::put('/lokasidok1/update/{id}', 'LokasidokController@update')->name('lokasi_dokumen.update');
	Route::get('/lokasidok1/hapus/{id}', 'LokasidokController@delete')->name('lokasi_dokumen.delete');


	//Route untuk menu Data Master
	Route::get('/mhs1','MahasiswaController@index');
	Route::get('/mhs2','MahasiswaController@tambah');
	Route::post('/mhs2/store', 'MahasiswaController@store');
	Route::get('/mhs1/edit/{id}', 'MahasiswaController@edit')->name('mahasiswa.edit');
	Route::put('/mhs1/update/{id}', 'MahasiswaController@update')->name('mahasiswa.update');
	Route::get('/mhs1/hapus/{id}', 'MahasiswaController@delete')->name('mahasiswa.delete');

	Route::get('/mhsprestasi1','PrestasiMahasiswaController@index');
	Route::get('/mhsprestasi2','PrestasiMahasiswaController@tambah');
	Route::post('/mhsprestasi2/store', 'PrestasiMahasiswaController@store');
	Route::get('/mhsprestasi1/edit/{id}', 'PrestasiMahasiswaController@edit')->name('prestasi.edit');
	Route::put('/mhsprestasi1/update/{id}', 'PrestasiMahasiswaController@update')->name('prestasi.update');
	Route::get('/mhsprestasi1/hapus/{id}', 'PrestasiMahasiswaController@delete')->name('prestasi.delete');

	Route::get('/datadosen1','DosenController@index');
	Route::get('/datadosen2','DosenController@tambah');
	Route::post('/datadosen2/store', 'DosenController@store');
	Route::get('/datadosen1/edit/{id}', 'DosenController@edit')->name('dosen.edit');
	Route::put('/datadosen1/update/{id}', 'DosenController@update')->name('dosen.update');
	Route::get('/datadosen1/hapus/{id}', 'DosenController@delete')->name('dosen.delete');

	Route::get('/dosrekognisi1','PengakuanDosenController@index');
	Route::get('/dosrekognisi2','PengakuanDosenController@tambah');
	Route::post('/dosrekognisi2/store', 'PengakuanDosenController@store');
	Route::get('/dosrekognisi1/edit/{id}', 'PengakuanDosenController@edit')->name('rekognisi.edit');
	Route::put('/dosrekognisi1/update/{id}', 'PengakuanDosenController@update')->name('rekognisi.update');
	Route::get('/dosrekognisi1/hapus/{id}', 'PengakuanDosenController@delete')->name('rekognisi.delete');

	Route::get('/dosjabatan1','DosenJabatanController@index');
	Route::get('/dosjabatan2','DosenJabatanController@tambah');
	Route::post('/dosjabatan2/store', 'DosenJabatanController@store');
	Route::get('/dosjabatan1/edit/{id}', 'DosenJabatanController@edit')->name('jabatan.edit');
	Route::put('/dosjabatan1/update/{id}', 'DosenJabatanController@update')->name('jabatan.update');
	Route::get('/dosjabatan1/hapus/{id}', 'DosenJabatanController@delete')->name('jabatan.delete');


	Route::get('/dosriwayat1','DosenRiwayatController@index');
	Route::get('/dosriwayat2','DosenRiwayatController@tambah');
	Route::post('/dosriwayat2/store', 'DosenRiwayatController@store');
	Route::get('/dosriwayat1/edit/{id}', 'DosenRiwayatController@edit')->name('riwayat.edit');
	Route::put('/dosriwayat1/update/{id}', 'DosenRiwayatController@update')->name('riwayat.update');
	Route::get('/dosriwayat1/hapus/{id}', 'DosenRiwayatController@delete')->name('riwayat.delete');

	Route::get('/dosbidang1','JenisKeahlianController@index');
	Route::get('/dosbidang2','JenisKeahlianController@tambah');
	Route::post('/dosbidang2/store', 'JenisKeahlianController@store');
	Route::get('/dosbidang1/edit/{id}', 'JenisKeahlianController@edit')->name('bidang.edit');
	Route::put('/dosbidang1/update/{id}', 'JenisKeahlianController@update')->name('bidang.update');
	Route::get('/dosbidang1/hapus/{id}', 'JenisKeahlianController@delete')->name('bidang.delete');


	Route::get('/bidangahli1','DosenKeahlianController@index');
	Route::get('/bidangahli2','DosenKeahlianController@tambah');
	Route::post('/bidangahli2/store', 'DosenKeahlianController@store');
	Route::get('/bidangahli1/edit/{id}', 'DosenKeahlianController@edit')->name('keahlian.edit');
	Route::put('/bidangahli1/update/{id}', 'DosenKeahlianController@update')->name('keahlian.update');
	Route::get('/bidangahli1/hapus/{id}', 'DosenKeahlianController@delete')->name('keahlian.delete');


	Route::get('/dosjenjang1','DosenJenjangController@index');
	Route::get('/dosjenjang2','DosenJenjangController@tambah');
	Route::post('/dosjenjang2/store', 'DosenJenjangController@store');
	Route::get('/dosjenjang1/edit/{id}', 'DosenJenjangController@edit')->name('jenjang_pend.edit');
	Route::put('/dosjenjang1/update/{id}', 'DosenJenjangController@update')->name('jenjang_pend.update');
	Route::get('/dosjenjang1/hapus/{id}', 'DosenJenjangController@delete')->name('jenjang_pend.delete');

	Route::get('/dosjenis1','DosenJenisController@index');
	Route::get('/dosjenis2','DosenJenisController@tambah');
	Route::post('/dosjenis2/store','DosenJenisController@store');
	Route::get('/dosjenis1/edit/{id}', 'DosenJenisController@edit')->name('jenis_dosen.edit');
	Route::put('/dosjenis1/update/{id}', 'DosenJenisController@update')->name('jenis_dosen.update');
	Route::get('/dosjenis1/hapus/{id}', 'DosenJenisController@delete')->name('jenis_dosen.delete');

	Route::get('/dospembimbing1','PembimbingTaController@index');
	Route::get('/dospembimbing2','PembimbingTaController@tambah');
	Route::post('/dospembimbing2/store', 'PembimbingTaController@store');
	Route::get('/dospembimbing1/edit/{id}', 'PembimbingTaController@edit')->name('pembimbing.edit');
	Route::put('/dospembimbing1/update/{id}', 'PembimbingTaController@update')->name('pembimbing.update');
	Route::get('/dospembimbing1/hapus/{id}', 'PembimbingTaController@delete')->name('pembimbing.delete');

	Route::get('/ewmp1','EwmpController@index');
	Route::get('/ewmp2','EwmpController@tambah');
	Route::post('/ewmp2/store', 'EwmptController@store');
	Route::get('/ewmp1/edit/{id}', 'EwmpController@edit')->name('ewmp.edit');
	Route::put('/ewmp1/update/{id}', 'EwmpController@update')->name('ewmp.update');
	Route::get('/ewmp1/hapus/{id}', 'EwmpController@delete')->name('ewmp.delete');


	Route::get('/progstud1','ProgStudiController@index');
	Route::get('/progstud2','ProgStudiController@tambah');
	Route::post('/progstud2/store', 'ProgramStudiController@store');
	Route::get('/progstud1/edit/{id}', 'ProgramStudiController@edit')->name('prodi.edit');
	Route::put('/progstud1/update/{id}', 'ProgramStudiController@update')->name('prodi.update');
	Route::get('/progstud1/hapus/{id}', 'ProgramStudiController@delete')->name('prodi.delete');


	Route::get('/jenispengeluarandana1','JenisPengeluaranController@index');
	Route::get('/jenispengeluarandana2','JenisPengeluaranController@tambah');
	Route::post('/jenispengeluarandana2/store','JenisPengeluaranController@store');
	Route::get('/jenispengeluarandana1/edit/{id}', 'JenisPengeluaranController@edit')->name('jenis_pengeluaran.edit');
	Route::put('/jenispengeluarandana1/update/{id}', 'JenisPengeluaranController@update')->name('jenis_pengeluaran.update');
	Route::get('/jenispengeluarandana1/hapus/{id}', 'JenisPengeluaranController@delete')->name('jenis_pengeluaran.delete');


	Route::get('/lulusan1','LulusanController@index');
	Route::get('/lulusan2','LulusanController@tambah');
	Route::post('/lulusan2/store', 'LulusanController@store');
	Route::get('/lulusan1/edit/{id}', 'LulusanController@edit')->name('lulusan.edit');
	Route::put('/lulusan1/update/{id}', 'LulusanController@update')->name('lulusan.update');
	Route::get('/lulusan1/hapus/{id}', 'LulusanController@delete')->name('lulusan.delete');

	Route::get('/kurikulum1','KurikulumController@index');
	Route::get('/kurikulum2','KurikulumController@tambah');
	Route::post('/kurikulum2/store', 'KurikulumController@store');
	Route::get('/kurikulum1/edit/{id}', 'KurikulumController@edit')->name('kurikulum.edit');
	Route::put('/kurikulum1/update/{id}', 'KurikulumController@update')->name('kurikulum.update');
	Route::get('/kurikulum1/hapus/{id}', 'KurikulumController@delete')->name('kurikulum.delete');

	Route::get('/matakuliah1','MatakuliahController@index');
	Route::get('/matakuliah2','MatakuliahController@tambah');
	Route::post('/matakuliah2/store', 'MatakuliahController@store');
	Route::get('/matakuliah1/edit/{id}', 'MatakuliahController@edit')->name('matkul.edit');
	Route::put('/matakuliah1/update/{id}', 'MatakuliahController@update')->name('matkul.update');
	Route::get('/matakuliah1/hapus/{id}', 'MatakuliahController@delete')->name('matkul.delete');

	Route::get('/dayatampung1','DayaTampungController@index');
	Route::get('/dayatampung2','DayaTampungController@tambah');
	Route::post('/dayatampung2/store', 'DayaTampungController@store');
	Route::get('/dayatampung1/edit/{id}', 'DayaTampungController@edit')->name('daya_tampung.edit');
	Route::put('/dayatampung1/update/{id}', 'DayaTampungController@update')->name('daya_tampung.update');
	Route::get('/dayatampung1/hapus/{id}', 'DayaTampungController@delete')->name('daya_tampung.delete');


	Route::get('/kerjasama1','KerjasamaController@index');
	Route::get('/kerjasama2','KerjasamaController@tambah');
	Route::post('/kerjasama2/store', 'KerjasamaController@store');
	Route::get('/kerjasama1/edit/{id}','KerjasamaController@edit')->name('kerjasama.edit');
	Route::put('/kerjasama1/update/{id}', 'KerjasamaController@update')->name('kerjasama.update');
	Route::get('/kerjasama1/hapus/{id}', 'KerjasamaController@delete')->name('kerjasama.delete');

	Route::get('/progstud1','ProgStudiController@index');
	Route::get('/progstud2','ProgStudiController@tambah');
	Route::post('/progstud2/store', 'ProgStudiController@store');
	Route::get('/progstud1/edit/{id}','ProgStudiController@edit')->name('prodi.edit');
	Route::put('/progstud1/update/{id}', 'ProgStudiController@update')->name('prodi.update');
	Route::get('/progstud1/hapus/{id}', 'ProgstudiController@delete')->name('prodi.delete');

	Route::get('/kepuasanmhs1','KepuasanMahasiswaController@index');
	Route::get('/kepuasanmhs2','KepuasanMahasiswaController@tambah');
	Route::post('/kepuasanmhs2/store', 'KepuasanMahasiswaController@store');
	Route::get('/kepuasanmhs1/edit/{id}','KepuasanMahasiswaController@edit')->name('kepuasanmhs.edit');
	Route::put('/kepuasanmhs1/update/{id}', 'KepuasanMahasiswaController@update')->name('kepuasanmhs.update');
	Route::get('/kepuasanmhs1/hapus/{id}', 'KepuasanMahasiswaController@delete')->name('kepuasanmhs.delete');

	Route::get('/kepuasanpenggunalulusan1','KepuasanPenggunaController@index');
	Route::get('/kepuasanpenggunalulusan2','KepuasanPenggunaController@tambah');
	Route::post('/kepuasanpenggunalulusan2/store', 'KepuasanPenggunaController@store');
	Route::get('/kepuasanpenggunalulusan1/edit/{id}','KepuasanPenggunaController@edit')->name('kepuasanmhs.edit');
	Route::put('/kepuasanpenggunalulusan1/update/{id}', 'KepuasanPenggunaController@update')->name('kepuasanmhs.update');
	Route::get('/kepuasanpenggunalulusan1/hapus/{id}', 'KepuasanPenggunaController@delete')->name('kepuasanmhs.delete');

	Route::get('/rekappengeluaran1','RekapPengeluaranController@index');
	Route::get('/rekappengeluaran2','RekapPengeluaranController@tambah');
	Route::post('/rekappengeluaran2/store', 'RekapPengeluaranController@store');
	Route::get('/rekappengeluaran1/edit/{id}','RekapPengeluaranController@edit')->name('rekap_pengeluaran.edit');
	Route::put('/rekappengeluaran1/update/{id}', 'RekapPengeluaranController@update')->name('rekap_pengeluaran.update');
	Route::get('/rekappengeluaran1/hapus/{id}', 'RekapPengeluaranController@delete')->name('rekap_pengeluaran.delete');


	Route::get('/sumberbiaya1','SumberPembiayaanController@index');
	Route::get('/sumberbiaya2','SumberPembiayaanController@tambah');
	Route::post('/sumberbiaya2/store', 'SumberPembiayaanController@store');
	Route::get('/sumberbiaya1/edit/{id}','SumberPembiayaanController@edit')->name('sumber_pembiayaan.edit');
	Route::put('/sumberbiaya1/update/{id}', 'SumberPembiayaanController@update')->name('sumber_pembiayaan.update');
	Route::get('/sumberbiaya1/hapus/{id}', 'SumberPembiayaanController@delete')->name('sumber_pembiayaan.delete');


	Route::get('/publikasikarya1','PublikasiKaryaController@index');
	Route::get('/publikasikarya2','PublikasiKaryaController@tambah');
	Route::post('/publikasikarya2/store', 'PublikasiKaryaController@store');
	Route::get('/publikasikarya1/edit/{id}','PublikasiKaryaController@edit')->name('publikasi_karya.edit');
	Route::put('/publikasikarya1/update/{id}', 'PublikasiKaryaController@update')->name('publikasi_karya.update');
	Route::get('/publikasikarya1/hapus/{id}', 'PublikasiKaryaController@delete')->name('publikasi_karya.delete');

	Route::post('jabar', 'PublikasiKaryaController@jabar')->name('jabar');


	Route::get('/jenispenelitian1','JenisPenelitianController@jenispenelitian');
	Route::get('/jenispenelitian2','JenisPenelitianController@tjenispenelitian');
	Route::post('/jenispenelitian2/store', 'JenisPenelitianController@store');
	Route::get('/jenispenelitian1/edit/{id}','JenisPenelitianController@edit')->name('jenis_penelitian.edit');
	Route::put('/jenispenelitian1/update/{id}', 'JenisPenelitianController@update')->name('jenis_penelitian.update');
	Route::get('/jenispenelitian1/hapus/{id}', 'JenisPenelitianController@delete')->name('jenis_penelitian.delete');


	Route::get('/jenispkm1','JenisPkmController@jenispkm');
	Route::get('/jenispkm2','JenisPkmController@tjenispkm');
	Route::post('/jenispkm/store', 'JenisPkmController@store');
	Route::get('/jenispkm1/edit/{id}','JenisPkmController@edit')->name('jenis_pkm.edit');
	Route::put('/jenispkm1/update/{id}', 'JenisPkmController@update')->name('jenis_pkm.update');
	Route::get('/jenispkm1/hapus/{id}', 'JenisPkmController@delete')->name('jenis_pkm.delete');

	Route::get('/jenispub1','JenisPubController@index');
	Route::get('/jenispub2','JenisPubController@tambah');
	Route::post('/jenispub2/store', 'JenisPubController@store');
	Route::get('/jenispub1/edit/{id}','JenisPubController@edit')->name('jenis_publikasi.edit');
	Route::put('/jenispub1/update/{id}', 'JenisPubController@update')->name('jenis_publikasi.update');
	Route::get('/jenispub1/hapus/{id}', 'JenisPubController@delete')->name('jenis_publikasi.delete');

	Route::get('/kategorikegiatan1','KategoriKegiatanController@index');
	Route::get('/kategorikegiatan2','KategoriKegiatanController@tambah');
	Route::post('/kategorikegiatan2/store', 'KategoriKegiatanController@store');
	Route::get('/kategorikegiatan1/edit/{id}','KategoriKegiatanController@edit')->name('kategori_kegiatan.edit');
	Route::put('/kategorikegiatan1/update/{id}', 'KategoriKegiatanController@update')->name('kategori_kegiatan.update');
	Route::get('/kategorikegiatan1/hapus/{id}', 'KategoriKegiatanController@delete')->name('kategori_kegiatan.delete');

	Route::get('/kategoricapaian1','KategoriCapaianController@index');
	Route::get('/kategoricapaian2','KategoriCapaianController@tambah');
	Route::post('/kategoricapaian2/store', 'KategoriCapaianController@store');
	Route::get('/kategoricapaian1/edit/{id}','KategoriCapaianController@edit')->name('kategori_capaian.edit');
	Route::put('/kategoricapaian1/update/{id}', 'KategoriCapaianController@update')->name('kategori_capaian.update');
	Route::get('/kategoricapaian1/hapus/{id}', 'KategoriCapaianController@delete')->name('kategori_capaian.delete');

	Route::get('/jenisprestasi1','JenisPrestasiController@index');
	Route::get('/jenisprestasi2','JenisPrestasiController@tambah');
	Route::post('/jenisprestasi2/store', 'JenisPrestasiController@store');
	Route::get('/jenisprestasi1/edit/{id}','JenisPrestasiController@edit')->name('jenis_prestasi.edit');
	Route::put('/jenisprestasi1/update/{id}', 'JenisPrestasiController@update')->name('jenis_prestasi.update');
	Route::get('/jenisprestasi1/hapus/{id}', 'JenisPrestasiController@delete')->name('jenis_prestasi.delete');

	Route::get('/jenisaspek1','JenisAspekController@index');
	Route::get('/jenisaspek2','JenisAspekController@tambah');
	Route::post('/jenisaspek2/store','JenisAspekController@store');
	Route::get('/jenisaspek1/edit/{id}','JenisAspekController@edit')->name('jenis_aspek.edit');
	Route::put('/jenisaspek1/update/{id}', 'JenisAspekController@update')->name('jenis_aspek.update');
	Route::get('/jenisaspek1/hapus/{id}', 'JenisAspekController@delete')->name('jenis_aspek.delete');


	Route::get('/jenispenerimaan1','JenisPenerimaanController@index');
	Route::get('/jenispenerimaan2','JenisPenerimaanController@tambah');
	Route::post('/jenispenerimaan2/store','JenisPenerimaanController@store');
	Route::get('/jenispenerimaan1/edit/{id}','JenisPenerimaanController@edit')->name('jenis_penerimaan.edit');
	Route::put('/jenispenerimaan1/update/{id}', 'JenisPenerimaanController@update')->name('jenis_penerimaan.update');
	Route::get('/jenispenerimaan1/hapus/{id}', 'JenisPenerimaanController@delete')->name('jenis_penerimaan.delete');


	Route::get('/jeniskemampuan1','JenisKemampuanController@index');
	Route::get('/jeniskemampuan2','JenisKemampuanController@tambah');
	Route::post('/jeniskemampuan2/store','JenisKemampuanController@store');
	Route::get('/jeniskemampuan1/edit/{id}','JenisKemampuanController@edit')->name('jenis_kemampuan.edit');
	Route::put('/jeniskemampuan1/update/{id}', 'JenisKemampuanController@update')->name('jenis_kemampuan.update');
	Route::get('/jeniskemampuan1/hapus/{id}', 'JenisKemampuanController@delete')->name('jenis_kemampuan.delete');


	Route::get('/jenispenulis1','JenisPenulisController@index');
	Route::get('/jenispenulis2','JenisPenulisController@tambah');
	Route::post('/jenispenulis2/store','JenisPenulisController@store');
	Route::get('/jenispenulis1/edit/{id}','JenisPenulisController@edit')->name('jenis_penulis.edit');
	Route::put('/jenispenulis1/update/{id}', 'JenisPenulisController@update')->name('jenis_penulis.update');
	Route::get('/jenispenulis1/hapus/{id}', 'JenisPenulisController@delete')->name('jenis_penulis.delete');


	Route::get('/jenisbimbingan1','JenisBimbinganController@index');
	Route::get('/jenisbimbingan2','JenisBimbinganController@tambah');
	Route::post('/jenisbimbingan2/store','JenisBimbinganController@store');
	Route::get('/jenisbimbingan1/edit/{id}','JenisBimbinganController@edit')->name('jenis_bimbingan.edit');
	Route::put('/jenisbimbingan1/update/{id}', 'JenisBimbinganController@update')->name('jenis_bimbingan.update');
	Route::get('/jenisbimbingan1/hapus/{id}', 'JenisBimbinganController@delete')->name('jenis_bimbingan.delete');


	Route::get('/rujukanpenelitian1','PenelitianController@rujukanpenelitian');
	Route::get('/rujukanpenelitian2','PenelitianController@trujukan');


	Route::get('/tahunmasuk1','TahunMasukController@index');
	Route::get('/tahunmasuk2','TahunMasukController@tambah');


	Route::get('/jenishki1','JenisHkiController@index');
	Route::get('/jenishki2','JenisHkiController@tambahhki');


	Route::get('/dosprodukjasa1','ProdukJasaController@index');
	Route::get('/dosprodukjasa2','ProdukJasaController@tambah');
	Route::post('/dosprodukjasa2/store','ProdukJasaController@store');
	Route::get('/dosprodukjasa1/edit/{id}','ProdukJasaController@edit')->name('produk_jasa.edit');
	Route::put('/dosprodukjasa1/update/{id}', 'ProdukJasaController@update')->name('produk_jasa.update');
	Route::get('/dosprodukjasa1/hapus/{id}', 'ProdukJasaController@delete')->name('produk_jasa.delete');



	Route::get('/user1','UserController@index')->name('index');
	Route::get('/user2','UserController@tambah');
	Route::post('/user2/store','UserController@store');
	Route::get('/user1/edit/{id}','UserController@edit');
	Route::put('/user1/update/{id}', 'UserController@update');
	Route::get('/user1/hapus/{id}', 'UserController@delete');
});

Route::group(['middleware' => ['auth','checkRole:admin,validator']], function(){
//Route untuk Menu Dokumen
	Route::get('/dokumen','UploadController@index');
	Route::get('/download/{id}','UploadController@download')->name('dokumen.download');

	//Route untuk menu Upload Dokumen
	Route::get('/uploaddokumen1','UploadController@index');
	Route::get('/uploaddokumen2','UploadController@tambah');
	Route::post('/uploaddokumen2/store', 'UploadController@store');
	Route::get('/uploaddokumen1/edit/{id}', 'UploadController@edit')->name('dokumen.edit');
	Route::put('/uploaddokumen1/update/{id}', 'UploadController@update')->name('dokumen.update');
	Route::get('/uploaddokumen1/hapus/{id}', 'UploadController@delete')->name('dokumen.delete');
});



Route::group(['middleware' => ['auth','checkRole:validator']], function(){
	Route::get('/validasi1','ValidasiController@index')->name('validasi');
	Route::get('/validasi1/disetujui/{id}','ValidasiController@disetujui')->name('validasi.disetujui');
	Route::get('/validasi1/ditolak/{id}','ValidasiController@ditolak')->name('validasi.ditolak');
});


