@extends('layouts.backend')

@section('title',' EWMP Dosen')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid"> 
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">EWMP Dosen</a></li>
              <li class="breadcrumb-item active">Daftar EWMP Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar EWMP Dosen</i></h5>
            <tr>
              <td>
                <a href="ewmp2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah EWMP Dosen</i></button></a><br>
              </td><br>
            </tr>
             <br/>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                              <th width="20px">No.</th>
                              <th width="200px">Nama Dosen</th>
                              <th width="200px">Jenis Dosen</th>
                              <th width="200px">Jumlah Sks</th>
                              <th width="200px">Konversi Kredit Ke Jam</th>
                              <th width="200px">Rata-Rata per Semester</th>
                              <th width="100px">Aksi</th>
                            </tr>
                        </thead>
                       
                    </table>


        </div>
   
        <!-- /.card-body -->
       
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection