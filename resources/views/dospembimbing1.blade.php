@extends('layouts.backend')

@section('title','Dosen Pembimbing Utama TA')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dosen Pembimbing Utama TA</a></li>
              <li class="breadcrumb-item active">Daftar Dosen Pembimbing Utama TA</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Dosen Pembimbing Utama TA</i></h5>
            <tr>
              <td>
                <a href="dospembimbing2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Daftar Data</i></button></a><br>
              </td><br>
            </tr>
             <br>
              <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                               <th width="20px">No.</th>
                                <th width="200px">Dosen Pembimbing</th>
                                <th width="200px">Mahasiswa Bimbingan</th>
                                <th width="150px">Program Studi</th></th>
                                <th width="150px">Jenis Bimbingan</th>
                                <th width="80px">Aksi</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php $i = 0; ?>
                            @foreach($pembimbing_tugas_akhir as $p)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $p->Dosen->nama_dosen }}</td>
                                <td>{{ $p->Mahasiswa->nama_mhs }}</td>
                                <td>{{ $p->ProgramStudi->nama_prodi }}</td>
                                <td>{{ $p->JenisBimbingan->jenis_bimbingan }}</td>
                                <td>
                                  <a href="{{route('pembimbing.edit',$p->id_pembimbing_utama)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

                                  <a href="{{route('pembimbing.delete',$p->id_pembimbing_utama)}}" <i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>


        </div>
      </div>
    </section>
  </div>
@endsection