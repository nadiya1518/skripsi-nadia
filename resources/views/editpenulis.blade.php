@extends('layouts.backend')

@section('title','Edit Jenis Penulis')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Penulis</a></li>
              <li class="breadcrumb-item active">Edit Jenis Penulis/li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Jenis Penulis</h3>
          </div>

                <form method="post" action="{{route('jenis_penulis.update',$jenis_penulis->id_penulis)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Jenis Penulis*</label>
                    <input type="string" name="nama_jenis_penulis"  id="nama_jenis_penulis" class="form-control" placeholder="Jenis Penulis" value=" {{ $jenis_penulis->nama_jenis_penulis }} ">

                    @if($errors->has('nama_jenis_penulis'))
                                <div class="text-danger">
                                    {{ $errors->first('nama jenis penulis')}}
                                </div>
                    @endif

                  </div>

                
                
                <!-- /.card-body -->
                  <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection