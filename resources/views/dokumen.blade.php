@extends('layouts.backend')

@section('title','Dokumen')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dokumen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
      <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Dokumen</i></h5>
            <tr>
              <td>
                <a href="uploaddokumen2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Dokumen</i></button></a><br>
              </td>
            </tr>
      </div>
   <body>   

    <table class="table table-bordered table-hover table-striped">
      <thead>
          <tr>
            <th width="100px">Standar</th>
            <th width="400px">Nama Dokumen</th> 
            <th width="200px">Lokasi Dokumen</th>
            <th width="200px">Jenis Dokumen</th>
            <th width="150px">File</th>
            <th width="50px">Aksi</th>
            </tr>
      </thead>
    <tbody>
       @foreach($upload as $b)
         <tr>
         <td>{{ $b->Standar->nama_standar }}</td>
         <td>{{ $b->Dokumen->nama_dok }}</td>
         <td>{{ $b->LokasiDokumen->lokasi_dokumen }}</td>
         <td>{{ $b->Dokumen->JenisDokumen->jenis_dokumen }}</td>
         <td>{{ $b->Dokumen->file }}
         </td>
         <td>
         <a href="{{route('dokumen.delete',$b->id_upload)}}"<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
         </td>
         </tr>
         @endforeach
    </tbody>
    </table>
</body>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection