@extends('layouts.backend')

@section('title','Tambah Daftar Penelitian')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Penelitian Rujukan Tema Tesis/Disertasi</a></li>
              <li class="breadcrumb-item active">Tambah Penelitian</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Penelitian</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
                  <div class="form-group">
                    <label>Luaran Penelitian dan PkM</label>
                    <input type="string" name="luaran_pen_pkm" class="form-control" placeholder="Luaran Penelitian dan PkM">
                  </div>
                  <div class="form-group">
                    <label>Tahun</label>
                    <input type="string" name="tahun" class="form-control" placeholder="Tahun">
                  </div>
                  <div class="form-group">
                    <label>Keterangan</label>
                    <input type="string" name="keterangan" class="form-control" placeholder="Keterangan">
                  </div>
                  <div class="form-group">
                    <label>Apakah Dosen</label>

                     <div class="form-check">
                          <input class="form-check-input" type="radio" name="tingkat">
                          <label class="form-check-label">Ya</label>
                     </div>

                     <div class="form-check">
                          <input class="form-check-input" type="radio" name="tingkat">
                          <label class="form-check-label">Tidak</label>  
                     </div>
                   </label>
                 </div>
                 <div class="form-group">
                    <label>Apakah Mahasiswa</label>

                     <div class="form-check">
                          <input class="form-check-input" type="radio" name="tingkat">
                          <label class="form-check-label">Ya</label>
                     </div>

                     <div class="form-check">
                          <input class="form-check-input" type="radio" name="tingkat">
                          <label class="form-check-label">Tidak</label>  
                     </div>
                   </label>
                  </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon far fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection