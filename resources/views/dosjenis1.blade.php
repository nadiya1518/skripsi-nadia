@extends('layouts.backend')

@section('title','Jenis Dosen')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid"> 
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Dosen</a></li>
              <li class="breadcrumb-item active">Daftar Jenis Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Jenis Dosen</i></h5>
            <tr>
              <td>
                <a href="dosjenis2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Jenis Dosen</i></button></a><br>
              </td><br>
            </tr>
             <br/>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                              <th width="20px">No.</th>
                              <th width="900px">Jenis Dosen</th>
                              <th width="100px">Aksi</th>
                            </tr>
                        </thead>
                       <tbody>
                        <?php $i=0; ?>
                         @foreach($jenis_dosen as $p)
                        
                        <tr>
                          <td>{{ ++$i }}</td>
                          <td>{{ $p->jenis_dosen }}</td>
                          <td>

                          <a href="{{route('jenis_dosen.edit',$p->id_jenis_dosen)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

                          <a href="{{route('jenis_dosen.delete',$p->id_jenis_dosen)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                          </td>
                        </tr>
                        @endforeach
                     </tbody>
                    </table>
              </div>
          </div>
      </section>
  </div>
@endsection