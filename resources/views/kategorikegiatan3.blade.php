@extends('layouts.backend')

@section('title','Edit Kategori Kegiatan')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kategori Kegiatan</a></li>
              <li class="breadcrumb-item active">Edit Kategori Kegiatan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Kategori Kegiatan</h3>
          </div>

        <form method="post" action="{{route('kategori_kegiatan.update',$kategori_kegiatan->id_kategori)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Nama Kategori Kegiatan</label>
                    <input type="text" name="nama_kategori" class="form-control" rows="3" placeholder="Nama Kategori Kegiatan" value="{{ $kategori_kegiatan->nama_kategori }}">

                    @if($errors->has('kategori'))
                                <div class="text-danger">
                                    {{ $errors->first('kategori')}}
                                </div>
                     @endif

                    </div>
                  </div>
                
              

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </body>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection