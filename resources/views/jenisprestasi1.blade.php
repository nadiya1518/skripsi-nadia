@extends('layouts.backend')

@section('title','Jenis Prestasi Mahasiswa')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Prestasi Mahasiswa</a></li>
              <li class="breadcrumb-item active">Daftar Jenis Prestasi Mahasiswa</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Jenis Prestasi Mahasiswa</i></h5>
            <tr>
              <td>
                <a href="jenisprestasi2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Jenis Prestasi</i></button></a><br>
              </td><br>
            </tr>

             <br/>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Jenis Prestasi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                       <tbody>
                         @foreach($jenis_prestasi as $p)
                        <tr>
                          <td>{{ $p->jenis_prestasi }}</td>
                          
                          <td>
                          <a href="{{route('jenis_prestasi.edit',$p->id_prestasi)}}" <i class="fas fa-edit btn btn-warning"></i></a>
                          <a href="{{route('jenis_prestasi.delete',$p->id_prestasi)}}" <i class="far fa-trash-alt btn btn-danger"></i></a>
                          </td>
                        </tr>
                        @endforeach
                     </tbody>
                    </table>

        </div>
      </div>
   
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection