@extends('layouts.backend')

@section('title','Tambah Jenis PkM')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis PkM</a></li>
              <li class="breadcrumb-item active">Tambah Jenis PkM</li>
            </ol>
          </div> 
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Jenis PkM</h3>
          </div>
              <form method="post" action="/jenispkm/store">
              {{ csrf_field() }}

              <div class="card-body">
                  <div class="form-group">
                    <label>Jenis PkM</label>
                    <input type="string" name="nama_jenis_pkm" class="form-control" placeholder="Jenis PkM">
                  
                  @if($errors->has('namajenispkm'))
                   <div class="text-danger">
                      {{ $errors->first('nama jenis pkm')}}
                   </div>
                      @endif
                </div>

                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="jenispkm1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection