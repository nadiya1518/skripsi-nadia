@extends('layouts.backend')

@section('title','Edit Data Sumber Pembiayaan')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Sumber Pembiayaan</a></li>
              <li class="breadcrumb-item active">Edit Data Sumber Pembiayaan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Sumber Pembiayaan</h3>
          </div>

                <form method="post" action="{{route('sumber_pembiayaan.update',$sumber_pembiayaan->id_sumber_pembiaayaan)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Sumber Pembiayaan*</label>
                    <input type="string" name="sumber_pembiayaan" class="form-control" placeholder="Sumber Pembiayaan" value=" {{ $sumber_pembiayaan->sumber_pembiayaan }} ">

                    @if($errors->has('sumberpembiayaan'))
                                <div class="text-danger">
                                    {{ $errors->first('sumberpembiayaan')}}
                                </div>
                    @endif

                  </div>
                
                <!-- /.card-body -->
                  <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection