@extends('layouts.backend')

@section('title','Tambah Standar')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Standar</a></li>
              <li class="breadcrumb-item active">Tambah Standar</li>
            </ol>
          </div>
        </div> 
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Tambah Standar</i></h5>
        </div>
                <form method="post" action="/tstandar/store">
                {{ csrf_field() }} 
                <div class="card-body">
                  <div class="form-group">
                    <label>Kode Standar*</label>
                   
                    <input type="number" name="kode_standar" class="form-control" value="1" placeholder="Kode Standar">

                    @if($errors->has('kodestandar'))
                                <div class="text-danger">
                                    {{ $errors->first('kode standar')}}
                                </div>
                    @endif
                    </div>

                    <div class="form-group">
                    <label>Nama Standar*</label>
                    <input type="string" name="nama_standar" class="form-control" placeholder="Nama Standar">

                    @if($errors->has('namastandar'))
                                <div class="text-danger">
                                    {{ $errors->first('nama standar')}}
                                </div>
                    @endif
                    </div>

                 <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="halstan"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>
                

      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection