@extends('layouts.backend')

@section('title','Tambah Dokumen')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Upload Dokumen</a></li>
              <li class="breadcrumb-item active">Tambah Dokumen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
 <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Dokumen</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/uploaddokumen2/store" method="POST" enctype="multipart/form-data">
					    {{ csrf_field() }}
              
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Standar*</label>
                    <select name="id_standar" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($standar as $semuastandar)
                      <option value="{{$semuastandar->id_standar}}">{{ $semuastandar->nama_standar}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama_standar'))
                                <div class="text-danger">
                                    {{ $errors->first('id_standar')}}
                                </div>
                    @endif
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Baku Mutu*</label>
                    <select name="id_baku" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($bakumutu as $satuanbakumutu)
                     <option value="{{$satuanbakumutu->id_baku}}">{{ $satuanbakumutu->baku_mutu}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('baku_mutu'))
                                <div class="text-danger">
                                    {{ $errors->first('id_baku')}}
                                </div>
                    @endif
                </div>

                  <div class="form-group">
                    <label for="exampleInputLokasi1">Lokasi Dokumen*</label>
                    <select name="id_lok" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($lokasidokumen as $semualokasi)
                       <option value="{{$semualokasi->id_lok}}">{{ $semualokasi->lokasi_dokumen}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('lokasi_dokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('id_lok')}}
                                </div>
                    @endif
                </div>
                  
                  <div class="form-group">
                    <label for="exampleInputJenis1">Jenis Dokumen*</label>
                    <select name="id_jenis" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenisdokumen as $semuajenis)
                       <option value="{{$semuajenis->id_jenis}}">{{ $semuajenis->jenis_dokumen}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('jenis_dokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('id_jenis')}}
                                </div>
                    @endif
                  </div>


                  <div class="form-group">
                    <label for="exampleInputFile">File</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input name="file" type="file" class="custom-file-input" id="exampleInputFile">
                        @if($errors->has('file'))
                                <div class="text-danger">
                                    {{ $errors->first('file')}}
                                </div>
                        @endif
                        <label class="custom-file-label" for="exampleInputFile">Pilih file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Keterangan*</label>
                    <input type="text" name="keterangan" class="form-control" placeholder="Keterangan">

                    @if($errors->has('keterangan'))
                                <div class="text-danger">
                                    {{ $errors->first('keterangan')}}
                                </div>
                    @endif
                    </div>
                  <div class="form-group">
                    <label for="exampleInputLink1">Link Url</label>
                    <input name="link_url" type="string" class="form-control" id="exampleInputLink1" placeholder="Masukkan Link Url">
                    @if($errors->has('link_url'))
                                <div class="text-danger">
                                    {{ $errors->first('link_url')}}
                                </div>
                    @endif
                     <br><tr>
                      <td>
                        <a href="#"><button type="button" class="btn btn-block btn-primary btn-small"><i class="nav-icon fas fa-search">&ensp;Pilih dari daftar dokumen terupload</i></button></a><br>
                      </td>
                    </tr>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputNamadokumen1">Nama Dokumen*</label>
                    <input name="nama_dok" type="string" class="form-control" id="exampleInputNamadokumen1" placeholder="Masukkan Nama Dokumen">
                    @if($errors->has('nama_dok'))
                                <div class="text-danger">
                                    {{ $errors->first('nama_dok')}}
                                </div>
                    @endif
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-flat btn-primary nav-icon fas fa-save">&ensp;Simpan</button>
                </div>
              </form>
            </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection