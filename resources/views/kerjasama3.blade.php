@extends('layouts.backend')

@section('title','Edit Daftar Lulusan')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Lulusan</a></li>
              <li class="breadcrumb-item active">Edit Daftar Lulusan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Baku Mutu</h3>
          </div>

                <form method="post" action="{{route('kerjasama.update',$kerjasama->id_kerjasama)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Lembaga Mitra*</label>
                    <input type="string" name="lembaga_mitra" class="form-control" placeholder="Lembaga Mitra" value=" {{ $kerjasama->lembaga_mitra }} ">

                    @if($errors->has('lembagamitra'))
                                <div class="text-danger">
                                    {{ $errors->first('lembagamitra')}}
                                </div>
                    @endif
                  </div>


                  <div class="form-group">
                        <label>Tingkat</label>
                        <select name="tingkat" class="form-control select2" style="width: 100%;">">
                          <option>--Pilih--</option>
                          <option>Wilayah/Lokal</option>
                          <option>Nasional</option>
                          <option>Internasional</option>
                        </select>
                        @if($errors->has('tingkat'))
                                <div class="text-danger">
                                    {{ $errors->first('tingkat')}}
                                </div>
                    @endif
                      </div>

                    <br>
                    <div class="form-group">
                      <label>Judul Kegiatan Kerjasama</label>
                      <input type="string" name="judul_kegiatan_kerjasama" class="form-control" placeholder="Judul Kegiatan Kerjasama" value="{{ $kerjasama->judul_kegiatan_kerjasama }}">
                      @if($errors->has('judul_kegiatan_kerjasama'))
                                <div class="text-danger">
                                    {{ $errors->first('judul_kegiatan_kerjasama')}}
                                </div>
                    @endif
                  </div>

                    <div class="form-group">
                      <label>Manfaat Bagi Program Studi Yang Diakreditasi</label>
                      <input type="string" name="manfaat_kerjasama" class="form-control" placeholder="Manfaat Bagi Program Studi Yang Diakreditasi" value="{{ $kerjasama->manfaat_kerjasama }}">
                      @if($errors->has('manfaat_kerjasama'))
                                <div class="text-danger">
                                    {{ $errors->first('manfaat_kerjasama')}}
                                </div>
                    @endif
                  </div>

                    <div class="form-group">
                      <label>Waktu Mulai</label>
                      <input type="string" name="waktu_mulai_kerjasama" class="form-control" placeholder="Waktu dan Durasi" value="{{ $kerjasama->waktu_mulai_kerjasama }}">
                      @if($errors->has('waktu_mulai'))
                                <div class="text-danger">
                                    {{ $errors->first('waktu_mulai')}}
                                </div>
                    @endif
                  </div>

                    <div class="form-group">
                      <label>Waktu Berakhirnya Kerjasama</label>
                      <input type="string" name="waktu_berakhir_kerjasama" class="form-control" placeholder="Tahun Berakhirnya Kerjasama" value="{{ $kerjasama->waktu_berakhir_kerjasama }}">
                      @if($errors->has('tahun_berakhir'))
                                <div class="text-danger">
                                    {{ $errors->first('tahun_berakhir')}}
                                </div>
                    @endif

                </div>
                  
                
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </body>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection