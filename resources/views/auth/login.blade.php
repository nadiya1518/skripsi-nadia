<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="backend/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="backend/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <style>
  .header-img {
  width: 100%;
  height: 800px;
  background: url('../backend/dist/img/rektorat.png');
  background-size: cover;
}
</style>
</head>


<body class="hold-transition login-page">
  <!-- <div class="content">
    <div class="header-img">
      <div class="container">
        <div class="row mb-2">
        <div class="col-sm-6"> -->
          <div class="login-box">
          <div class="login-logo">
          <b>AKREDITASI</b>Unila</a>
          </div>
      
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg"><img src="backend/dist/img/Logo.png">
        <br><b>Login</b>APLIKASI</a></p>
        <h8>Masuk dengan email dan password</h8>
        
      <br>
      <form action="{{route('auth.postlogin')}}" method="post">
        {{ csrf_field() }}
        <div class="input-group {{ $errors->has('email') ? ' has-error ' : ''}} mb-3">
          <input id="email" value="{{ old('email') }}" name="email" type="email" class="form-control" placeholder="Email">
          @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }} </strong>
            </span>
            @endif
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>

        <div class="input-group {{ $errors->has('password') ? ' has-error ' : ''}} mb-3">
          <input id="password" value="{{ old('password') }}" name="password" type="password" class="form-control" placeholder="Password">
          @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }} </strong>
            </span>
            @endif

          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
       
          <!-- /.col -->
          <div class="row">
            <div class="col-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">LOG IN</button>
            </div>
           
          <!-- /.col -->
        </div>
      </form>
      <!-- /.social-auth-links -->
      <br>
      <p class="mb-0">
        <a href="{{route('register')}}" class="text-center"><b>Register</b></a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>

<!-- /.login-box -->

<!-- jQuery -->
<script src="backend/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
</html>

<!-- {{ route('register')}} -->
