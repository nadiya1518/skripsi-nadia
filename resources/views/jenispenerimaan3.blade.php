<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>Dashboard Akreditasi Program Studi|Edit Standar</title>
    </head>

    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    <strong>Edit Jenis Penerimaan</strong>
                </div>

                <form method="post" action="{{route('jenis_penerimaan.update',$jenis_penerimaan->id_jenis_penerimaan)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Jenis Penerimaan*</label>
                    <input type="string" name="jenis_penerimaan" class="form-control" placeholder="Jenis Penerimaan" value=" {{ $jenis_penerimaan->jenis_penerimaan }} ">

                    @if($errors->has('jenispenerimaan'))
                                <div class="text-danger">
                                    {{ $errors->first('jenispenerimaan')}}
                                </div>
                    @endif
                  </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-flat btn-primary nav-icon fas fa-save">&ensp;Simpan</button>
                </div>
              </form>
 
                </div>
            </div>
        </div>
    </body>
</html>