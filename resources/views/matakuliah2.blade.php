@extends('layouts.backend')

@section('title','Tambah Mata Kuliah')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Mata Kuliah</a></li>
              <li class="breadcrumb-item active">Tambah Mata Kuliah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Mata Kuliah</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
               <form method="post" action="/matakuliah2/store">
                {{ csrf_field() }}
              <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Nama Kurikulum</label>
                    <select name="id_kurikulum" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($kurikulum as $semuakurikulum)
                     <option value="{{$semuakurikulum->id_kurikulum}}">{{ $semuakurikulum->nama_kurikulum}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama kurikulum'))
                                <div class="text-danger">
                                    {{ $errors->first('id_kurikulum')}}
                                </div>
                    @endif
                </div>

                <div class="form-group">
                    <label>Nama Mata Kuliah</label>
                    <input type="string" name="nama_matkul" class="form-control" placeholder="Nama Mata Kuliah">

                    @if($errors->has('nama_matkul'))
                                <div class="text-danger">
                                    {{ $errors->first('nama_matkul')}}
                                </div>
                    @endif
                  </div>

                    <div class="form-group">
                      <label>Semester</label>
                      <input type="number" name="semester" class="form-control" placeholder="Semester">
                      @if($errors->has('semester'))
                                <div class="text-danger">
                                    {{ $errors->first('semester')}}
                                </div>
                    @endif
                  </div>

                    <div class="form-group">
                      <label>Kode Mata Kuliah</label>
                      <input type="string" name="kode_matkul" class="form-control" placeholder="Kode Mata Kuliah">
                      @if($errors->has('kode_matkul'))
                                <div class="text-danger">
                                    {{ $errors->first('kode matkul')}}
                                </div>
                    @endif
                  </div>

                    <div class="form-group">
                    <label for="exampleInputBakumutu1">Program Studi</label>
                    <select name="id_prodi" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($program_studi as $semuaprodi)
                     <option value="{{$semuaprodi->id_prodi}}">{{ $semuaprodi->nama_prodi}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama_prodi'))
                                <div class="text-danger">
                                    {{ $errors->first('id_prodi')}}
                                </div>
                    @endif
                  </div>
                  

                  <div class="form-group">
                      <label>Jenis Mata Kuliah</label>
                      <input type="string" name="jenis_matkul" class="form-control" placeholder="Jenis Mata Kuliah">
                      @if($errors->has('jenis_matkul'))
                                <div class="text-danger">
                                    {{ $errors->first('jenis_matkul')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                      <label>Kelompok Mata Kuliah</label>
                      <input type="string" name="kelompok_matkul" class="form-control" placeholder="Kelompok Mata Kuliah">
                      @if($errors->has('kelompok matkul'))
                                <div class="text-danger">
                                    {{ $errors->first('kelompok_matkul')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                      <label>SKS Matakuliah</label>
                      <input type="string" name="sks_mk" class="form-control" placeholder="SKS Matakuliah">
                      @if($errors->has('sks mk'))
                                <div class="text-danger">
                                    {{ $errors->first('sks mk')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                      <label>SKS Responsi/Tutorial</label>
                      <input type="string" name="sks_tm" class="form-control" placeholder="SKS Matakuliah">
                      @if($errors->has('sks tm'))
                                <div class="text-danger">
                                    {{ $errors->first('sks tm')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                      <label>SKS Praktikum</label>
                      <input type="string" name="sks_prak" class="form-control" placeholder="SKS Matakuliah">
                      @if($errors->has('sks prak'))
                                <div class="text-danger">
                                    {{ $errors->first('sks prak')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                      <label>SKS Praktik Lapangan</label>
                      <input type="string" name="sks_prak_lap" class="form-control" placeholder="SKS Praktik Lapangan">
                      @if($errors->has('sks praktik lap'))
                                <div class="text-danger">
                                    {{ $errors->first('sks praktik lap')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                      <label>SKS Seminar</label>
                      <input type="string" name="sks_sim" class="form-control" placeholder="SKS Seminar">
                      @if($errors->has('sks seminar'))
                                <div class="text-danger">
                                    {{ $errors->first('sks seminar')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                      <label>Konversi Kredit Ke Jam</label>
                      <input type="string" name="konversi_kredit" class="form-control" placeholder="Konversi Kredit Ke Jam">
                      @if($errors->has('konversi kredit'))
                                <div class="text-danger">
                                    {{ $errors->first('konversi kredit')}}
                                </div>
                    @endif
                  </div>
   

                  <div class="form-group">
                      <label>Metode Pelaksanaan</label>
                      <input type="string" name="metode_pelaksanaan" class="form-control" placeholder="Metode Pelaksanaan">
                      @if($errors->has('metode_pelaksanaan'))
                                <div class="text-danger">
                                    {{ $errors->first('metode pelaksanaan')}}
                                </div>
                    @endif
                  </div>

                   <div class="form-group">
                        <label>Capaian Sikap</label>
                        <select name="cp_sikap" class="form-control select2" style="width: 100%;">">
                          <option>--Pilih--</option>
                          <option>Sangat Baik</option>
                          <option>Baik</option>
                          <option>Cukup</option>
                          <option>Kurang</option>
                        </select>
                        @if($errors->has('sikap'))
                                <div class="text-danger">
                                    {{ $errors->first('sikap')}}
                                </div>
                    @endif
                      </div>

                       <div class="form-group">
                        <label>Capaian Pengetahuan</label>
                        <select name="cp_pengetahuan" class="form-control select2" style="width: 100%;">">
                          <option>--Pilih--</option>
                          <option>Sangat Baik</option>
                          <option>Baik</option>
                          <option>Cukup</option>
                          <option>Kurang</option>
                        </select>
                        @if($errors->has('pengetahuan'))
                                <div class="text-danger">
                                    {{ $errors->first('pengetahuan')}}
                                </div>
                    @endif
                      </div>

                   <div class="form-group">
                        <label>Capaian Keterampilan Umum</label>
                        <select name="cp_ket_umum" class="form-control select2" style="width: 100%;">">
                          <option>--Pilih--</option>
                          <option>Sangat Baik</option>
                          <option>Baik</option>
                          <option>Cukup</option>
                          <option>Kurang</option>
                        </select>
                        @if($errors->has('keterampilan umum'))
                                <div class="text-danger">
                                    {{ $errors->first('keterampilan umum')}}
                                </div>
                    @endif
                      </div>

                   <div class="form-group">
                        <label>Capaian Keterampilan Khusus</label>
                        <select name="cp_ket_khusus" class="form-control select2" style="width: 100%;">">
                          <option>--Pilih--</option>
                          <option>Sangat Baik</option>
                          <option>Baik</option>
                          <option>Cukup</option>
                          <option>Kurang</option>
                        </select>
                        @if($errors->has('keterampilan khusus'))
                                <div class="text-danger">
                                    {{ $errors->first('keterampilan khusus')}}
                                </div>
                    @endif
                      </div>


                <!-- /.card-body -->

                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="matakuliah1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection