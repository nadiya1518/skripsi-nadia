@extends('layouts.backend')

@section('title','Borang Akreditasi')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Borang Akreditasi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Borang Akreditasi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Instrumen Akreditasi 4.0</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          
                  <tr>
                    <td>
                      <a href="/st1"><button type="button" class="btn btn-block btn-primary">STANDAR 1</button></a><br>
                    </td>
                    <td>
                      <a href="/st2"><button type="button" class="btn btn-block btn-primary">STANDAR 2</button></a><br>
                    </td>
                    <td>
                      <a href="/st3"><button type="button" class="btn btn-block btn-primary">STANDAR 3</button></a><br>
                    </td>
                    <td>
                      <a href="/st4"><button type="button" class="btn btn-block btn-primary">STANDAR 4</button></a><br>
                    </td>
                    <td>
                      <a href="/st5"><button type="button" class="btn btn-block btn-primary">STANDAR 5</button></a><br>
                    </td>
                    <td>
                     <a href="/st6"> <button type="button" class="btn btn-block btn-primary">STANDAR 6</button></a><br>
                    </td>
                    <td>
                      <a href="/st7"><button type="button" class="btn btn-block btn-primary">STANDAR 7</button></a><br>
                    </td>
                    <td>
                      <a href="/st8"><button type="button" class="btn btn-block btn-primary">STANDAR 8</button></a><br>
                    </td>
                    <td>
                      <a href="/st9"><button type="button" class="btn btn-block btn-primary">STANDAR 9</button></a><br>
                    </td>
                </tr>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>



@endsection