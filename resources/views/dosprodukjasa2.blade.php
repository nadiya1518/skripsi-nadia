@extends('layouts.backend')

@section('title','Tambah Data Produk/Jasa Dosen')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Produk dan Jasa Dosen</a></li>
              <li class="breadcrumb-item active">Tambah Data Produk dan Jasa Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Produk/Jasa Dosen</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
              <div class="form-group">
                  <label>Nama Dosen</label>
                  <input type="string" name="nama_dosen" class="form-control" placeholder="Nama Dosen">
              </div>

              <div class="form-group">
                  <label>Nama Produk/Jasa</label>
                  <input type="string" name="nama_dosen" class="form-control" placeholder="Nama Dosen PJ">
              </div>

              <div class="form-group">
                  <label>Deskripsi Produk/Jasa</label>
                  <input type="string" name="kode_matakuliah" class="form-control" placeholder="Kode Matakuliah">
              </div>

               <div class="form-group">
                    <label for="exampleInputFile">Bukti</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input name="file" type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Pilih file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div>
                </div>
                  </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
              </form>
            </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection