@extends('layouts.backend')

@section('title','Tambah Integrasi Kegiatan Penelitian')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Integrasi Kegiatan Penelitian</a></li>
              <li class="breadcrumb-item active">Tambah Integrasi Kegiatan Penelitian</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Integrasi Kegiatan Penelitian</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
                  <div class="form-group">
                    <label>Judul Penelitian/PkM</label>
                    <input type="string" name="judul_penelitian_pkm" class="form-control" placeholder="Judul Penelitian/PkM">
                  </div>
                  <div class="form-group">
                    <label>Nama Dosen</label>
                    <input type="string" name="nama_dosen" class="form-control" placeholder="Nama Dosen">
                  </div>
                  <div class="form-group">
                    <label>Mata Kuliah</label>
                    <input type="string" name="mata_kuliah" class="form-control" placeholder="Mata Kuliah">
                  </div>
                  <div class="form-group">
                    <label>Bentuk Integrasi</label>
                    <input type="string" name="bentuk_integrasi" class="form-control" placeholder="Bentuk Integrasi">
                  </div>
                  <div class="form-group">
                    <label>Tahun</label>
                    <input type="string" name="tahun" class="form-control" placeholder="Tahun">
                  </div>
                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon far fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection