@extends('layouts.backend')

@section('title','Edit Data Mahasiswa')

@section('content')

<?php
$keaktifan=[1=>'aktif',0=>'tidak aktif', ];

$mhsasing = [1=>'ya',0=>'tidak',];
?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Data Mahasiswa</a></li>
              <li class="breadcrumb-item active">Edit Data Mahasiswa</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Data Mahasiswa</h3>
          </div>
                <form method="post" action="{{route('mahasiswa.update',$mahasiswa->id_mhs)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Nama Mahasiswa*</label>
                    <input type="string" name="nama_mhs" class="form-control" placeholder="Masukkan Nama Mahasiswa" value=" {{ $mahasiswa->nama_mhs }} ">

                    @if($errors->has('nama_mahasiswa'))
                                <div class="text-danger">
                                    {{ $errors->first('nama_mahasiswa')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>NPM*</label>
                    <input type="string" name="npm" class="form-control" rows="3" placeholder="Nama Standar" value="{{ $mahasiswa->npm }}">

                    @if($errors->has('npm'))
                                <div class="text-danger">
                                    {{ $errors->first('npm')}}
                                </div>
                     @endif
                    </div>

                    <div class="form-group">
                    <label>Jurusan*</label>
                    <input type="text" name="jurusan" class="form-control" rows="3" placeholder="Nama Standar" value="{{ $mahasiswa->jurusan }}">

                    @if($errors->has('jurusan'))
                                <div class="text-danger">
                                    {{ $errors->first('jurusan')}}
                                </div>
                     @endif
                    </div>

                    <div class="form-group">
                    <label>Program Studi*</label>
                    <input type="text" name="prodi" class="form-control" rows="3" placeholder="Nama Standar" value="{{ $mahasiswa->prodi }}">

                    @if($errors->has('prodi'))
                                <div class="text-danger">
                                    {{ $errors->first('prodi')}}
                                </div>
                     @endif
                    </div>

                    <div class="form-group">
                    <label>Waktu Lulus*</label>
                    <input type="string" name="waktu_lulus" class="form-control" rows="3" placeholder="Waktu Lulus" value="{{ $mahasiswa->waktu_lulus }}">

                    @if($errors->has('waktu_lulus'))
                                <div class="text-danger">
                                    {{ $errors->first('waktu_lulus')}}
                                </div>
                     @endif
                    </div>

                    <div class="form-group">
                    <label>Tanggal dan Tahun Transfer*</label>
                    <input type="date" name="tanggal_dan_tahun_transfer" class="form-control" rows="3" placeholder="Tanggal dan Tahun Transfer" value="{{ $mahasiswa->tanggal_dan_tahun_transfer }}">

                    @if($errors->has('tanggal_dan_tahun_transfer'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal_dan_tahun_transfer')}}
                                </div>
                     @endif
                    </div>

                    


                  <div class="form-group">
                  <label>Status Keaktifan</label>
                    <select name="status_keaktifan" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($keaktifan as $keaktifan =>$aktif)
                      <option value="{{$keaktifan}}">{{$aktif}}</option>
                      @endforeach
                    </select>
                      @if($errors->has('status_keaktifan'))
                                <div class="text-danger">
                                    {{ $errors->first('$status_keaktifan')}}
                                </div>
                      @endif
                    </select>
                  </div>

                  <div class="form-group">
                  <label for="exampleInputBakumutu1">Jenis Penerimaan*</label>
                    <select name="id_jenis_penerimaan" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenispenerimaan as $satuanjalur)
                     <option value="{{$satuanjalur->id_jenis_penerimaan}}">{{ $satuanjalur->jenis_penerimaan}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('jenis_penerimaan'))
                                <div class="text-danger">
                                    {{ $errors->first('id_jenis_penerimaan')}}
                                </div>
                    @endif
                </div>

                    <div class="form-group">
                      <label>Jenis Mahasiswa</label>
                        <select name="jenis_mahasiswa" class="form-control select2" style="width: 100%;">">
                          <option>--Pilih--</option>
                          <option>Reguler</option>
                          <option>Transfer</option>
                        </select>
                         @if($errors->has('jenis_mhs'))
                                <div class="text-danger">
                                    {{ $errors->first('jenis_mhs')}}
                                </div>
                    @endif
                    
                
                <!-- /.card-body -->
                  <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </body>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection