<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('backend/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('backend/https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('backend/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('backend/plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('backend/dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('backend/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('backend/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('backend/plugins/summernote/summernote-bs4.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="{{asset('backend/https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet')}}">
      <style>
      .kiri{
        float:left;display:block;width:57px
      }
      th {
        text-align: center;
      }
      .tekskiri{
        text-align: left;
      }
      body {font-family: Arial;}


/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
.footer {
   position: fixed;
   left: 0;
   bottom: 0;
   width: 100%;
   background-color: red;
   color: white;
   text-align: center;
}
body, html {
  height: 100%;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}

.header-img {
  width: 100%;
  height: 800px;
  background: url('../backend/dist/img/rektorat.png');
  background-size: cover;
  /*filter: blur(4px);
  -webkit-filter: blur(4px);*/
}
.bg-text {
  /*background-color: rgb(0,0,0);*/ /* Fallback color */
  /*background-color: rgba(0,0,0, 0.4);*/ /* Black w/opacity/see-through */
  color: white;
  font-weight: bold;
  /*border: 3px solid #f1f1f1;*/
  position: absolute;
  top: 30%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 2;
  width: 80%;
  padding: 10px;
  text-align: center;
}

</style>
</head>


<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

  <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                  <!-- Left navbar links -->
                  <ul class="navbar-nav">
                    <li class="nav-item">
                      <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                      <a href="../../index3.html" class="nav-link">Home</a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                      <a href="#" class="nav-link">Contact</a>
                    </li>
                    <li class="nav-item d-none d-sm-inline-block">
                      <a href="index" class="nav-link">User</a>
                    </li>
                  </ul>


                  <!-- SEARCH FORM -->
                  <form class="form-inline ml-3">
                    <div class="input-group input-group-sm">
                      <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
                      <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                          <i class="fas fa-search"></i>
                        </button>
                      </div>
                    </div>
                  </form>

                  <ul class="navbar-nav ml-auto">
                    <div class="sidebar">
                      <div class="user-panel">
                      <div class="image">
                      <!-- <img src="{{asset('backend/dist/img/a.png')}}" class="img-circle elevation-2" alt="User Image">&ensp; -->
                      <!-- <i class="nav-icon fas fa-user-alt"></i></button><span><font color="black" size="2">&ensp;<b>{{auth()->user()->username}}</b></font></span>
                      <br> -->

                      <a href="{{ url('/logout') }}"><i class="nav-icon fas fa-sign-out-alt">&ensp;Logout</i></font></a>
                    </div> 



                  <!-- Dropdown Menu -->
                  <!-- <ul class="navbar-nav ml-auto">
                    <div class="sidebar">
                      <div class="user-panel mt-3 pb-3 mb-3 d-flex"> -->
                    <!-- <li class="nav-item dropdown"> -->
                    <!-- <a href="{{ url('/logout') }}"> -->
                    <!-- <span class="glyphicon glyphicon-log-out"><i class='fas fa-sign-out-alt'></i><font color="white">Log out</font></span> -->
                    <!-- </a> -->
                </nav>
              </div>

  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-info elevation-4">
    <a class="navbar-brand" href="#">
      <img class="center" src="{{asset('backend/dist/img/header.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-4" style="opacity: .8">
    </a>

      
    <!-- Sidebar -->

    <div class="sidebar">
      <div class="user-panel mt-3 pb-8 mb-6 d-flex">
        <div class="info">
          <a href="#" class="d-block"></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <div class="vertical-menu">
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

          <li class="nav-item has-treeview menu-open">
            <a href="dashboard" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
           <li class="nav-item has-treeview">
            <a href="dokumen" class="nav-link">
              <i class="nav-icon fas fa-folder-open"></i>
              <p>
                Dokumen
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>
                Borang Akreditasi
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="st1" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Standar 1</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="st2" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Standar 2</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="st3" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Standar 3</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="st4" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Standar 4</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="st5" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Standar 5</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="st6" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Standar 6</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="st7" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Standar 7</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="st8" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Standar 8</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="st9" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Standar 9</p>
                </a>
              </li>
            </ul>
          </li>
            <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>
                Referensi
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Referensi Dokumen</p>
                </a>

                <ul class="nav nav-treeview"> 
                <a href="halstan" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Standar</p>
                </a>
              </ul>

              <ul class="nav nav-treeview"> 
                <a href="halbaku" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Baku Mutu</p>
                </a>
              </ul>

              <ul class="nav nav-treeview"> 
                <a href="jenisdok1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Jenis Dokumen</p>
                </a>
              </ul>

              <ul class="nav nav-treeview"> 
                <a href="lokasidok1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Lokasi Dokumen</p>
                </a>
              </ul> 
            </li>
          </ul>
           <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                  <p>Data Referensi Dosen</p>
              </a>

            <ul class="nav nav-treeview"> 
              <a href="dosjenis1" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                  <p>Jenis Dosen</p>
              </a>
            </ul>

              <ul class="nav nav-treeview"> 
                <a href="dosbidang1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Jenis Bidang Keahlian</p>
                </a>
              </ul>

              <ul class="nav nav-treeview"> 
                <a href="jenisbimbingan1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Jenis Bimbingan</p>
                </a>
              </ul>
            </li>
          </ul>

          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                  <p>Data Referensi Prodi</p>
              </a>

            <ul class="nav nav-treeview"> 
              <a href="jenispengeluarandana1" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                  <p>Jenis Pengeluaran Dana</p>
              </a>
            </ul>

              <ul class="nav nav-treeview"> 
                <a href="sumberbiaya1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Sumber Pembiayaan</p>
                </a>
              </ul>
            </li>
          </ul>

          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                  <p>Data Referensi Kurikulum</p>
              </a>

            <ul class="nav nav-treeview"> 
              <a href="#" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                  <p>Kode Matakuliah</p>
              </a>
            </ul>

              <ul class="nav nav-treeview"> 
                <a href="#" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Jenis Mata Kuliah</p>
                </a>
              </ul>

            </li>
          </ul>

          <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Referensi 
                    <br>Mahasiswa</p>
                </a>

                <ul class="nav nav-treeview"> 
                <a href="jenispenerimaan1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Jenis Penerimaan</p>
                </a>
              </ul>

              <ul class="nav nav-treeview"> 
                <a href="jenisaspek1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Jenis Aspek Kepuasan 
                    <br>Mahasiswa</p>
                </a>
              </ul>

              <ul class="nav nav-treeview"> 
                <a href="jeniskemampuan1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Jenis Kemampuan</p>
                </a>
              </ul>
            </li>
          </ul>

          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                  <p>Data Referensi 
                    <br>Publikasi Karya</p>
              </a>

            <ul class="nav nav-treeview"> 
              <a href="kategorikegiatan1" class="nav-link">
                <i class="fas fa-circle nav-icon"></i>
                  <p>Kategori Kegiatan</p>
              </a>
            </ul>

              <ul class="nav nav-treeview"> 
                <a href="kategoricapaian1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Kategori Capaian</p>
                </a>
              </ul>

              <ul class="nav nav-treeview"> 
                <a href="jenispub1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Jenis Publikasi</p>
                </a>
              </ul>

              <ul class="nav nav-treeview"> 
                <a href="jenishki1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Jenis HKI</p>
                </a>
              </ul>

              <ul class="nav nav-treeview"> 
                <a href="jenispenulis1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Jenis Penulis</p>
                </a>
              </ul>

              <ul class="nav nav-treeview"> 
                <a href="jenispkm1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Jenis PkM</p>
                </a>
              </ul>
            </li>
          </ul>

        </li>
        
          @if(auth()->user()->role == 'admin')
           <li class="nav-item has-treeview">
            <a href="uploaddokumen2" class="nav-link">
              <i class="nav-icon fas fa-upload"></i>
              <p>
                Upload Dokumen
              </p>
            </a>
          </li>
          @endif


          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-file"></i>
              <p>
                Data Master
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Mahasiswa
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">            
                <a href="mhs1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Data Mahasiswa</p>
                </a>

                <a href="mhsprestasi1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Prestasi Mahasiswa</p>
                </a>

                <a href="#" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Karya Ilmiah 
                  <br>Mahasiswa</p>
                </a>

                <a href="mhspublikasi" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Publikasi Ilmiah 
                  <br>Mahasiswa</p>
                </a>    
              </ul>

              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dosen
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">            
                <a href="datadosen1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Data Dosen</p>
                </a>

                <a href="dosjenjang1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Jenjang Pendidikan 
                  <br>Dosen</p>
                </a>
              
                <a href="bidangahli1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Bidang Keahlian 
                  <br>Dosen</p>
                </a>

                <a href="dosriwayat1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Riwayat Pendidikan 
                  <br>Dosen</p>
                </a>
             
                <a href="dospembimbing1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Dosen Pembimbing 
                  <br>Utama TA</p>
                </a>
            
                <a href="dosjabatan1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Jabatan Fungsional 
                  <br>Dosen</p>
                </a>

                <a href="dosrekognisi1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Pengakuan/Rekognisi
                  <br>Dosen</p>
                </a>
                <a href="ewmp1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>EWMP</p>
                </a>
                <a href="dospenelitian1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Penelitian Dosen</p>
                </a>
                <a href="dospkm1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>PkM Dosen</p>
                </a>
                <a href="#" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Karya Ilmiah Dosen</p>
                </a>
                <a href="dospublikasi" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Publikasi Ilmiah Dosen</p>
                </a>

                <a href="dosprodukjasa1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Produk/Jasa Dosen</p>
                </a>       
              </ul>

               <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Program Studi
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>

                <ul class="nav nav-treeview">            
                <a href="progstud1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Data Program Studi</p>
                </a> 

                <a href="rekappengeluaran1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Rekapitulasi
                  <br>Pengeluaran Dana</p>
                </a>

                <a href="lulusan1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Lulusan</p>
                </a>    

                <a href="dayatampung1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Daya Tampung</p>
                </a>
            
                <a href="kerjasama1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Kerjasama</p>
                </a>

                <a href="kurikulum1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Kurikulum</p>
                </a>

                <a href="matakuliah1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Matakuliah</p>
                </a>

                 <a href="kepuasanmhs1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Kepuasan Mahasiswa</p>
                </a>
              
                <a href="kepuasanpenggunalulusan1" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Kepuasan Pengguna 
                  <br>Lulusan</p>
                </a>
              </ul>

              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Publikasi Karya
                  <i class="fas fa-angle-left right"></i>
                </p>
                </a>

                <ul class="nav nav-treeview">
                  <a href="publikasikarya1" class="nav-link">
                    <i class="fas fa-circle nav-icon"></i>
                      <p>Data Publikasi</p>
                  </a>

                  <a href="kurikulum1" class="nav-link">
                    <i class="fas fa-circle nav-icon"></i>
                      <p>Dokumen Publikasi</p>
                  </a>
                </ul>
              </li>


              <li class="nav-item">
                <a href="pkmdosmhs1" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>PkM Dosen dan 
                  <br>Mahasiswa</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="penelitiandosmhs1" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Penelitian Dosen dan 
                  <br>Mahasiswa</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="luaranpenelitian1" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Luaran Penelitian/PkM 
                  <br>Lainnya</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="rujukanpenelitian1" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Penelitian Rujukan 
                  <br>Tema Tesis/Disertasi</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="integrasipenelitian1" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Integrasi Kegiatan 
                  <br>Penelitian PkM Dalam 
                  <br>Pembelajaran</p>
                </a>
              </li>
            </li>
          </ul>
          

            @if(auth()->user()->role == 'validator')
              <li class="nav-item has-treeview">
            <a href="validasi1" class="nav-link">
              <i class="nav-icon fas fa-check"></i>
              <p>
                Validasi
              </p>
            </a>
          </li>
          @endif
              
              
         </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  @yield('content')

  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2019 <a href="http://if.unila.ac.id/">Teknik Informatika Universitas Lampung</a>.</strong>
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 2.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="{{asset('backend/plugins/jquery/jquery.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('backend/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('backend/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->

<!-- JQVMap -->

<!-- daterangepicker -->
<script src="{{asset('backend/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('backend/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('backend/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('backend/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('backend/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('backend/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->


</body>
</html>
