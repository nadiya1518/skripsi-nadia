@extends('layouts.backend')

@section('title','Tambah Jenis Publikasi')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Publikasi</a></li>
              <li class="breadcrumb-item active">Tambah Jenis Publikasi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Jenis Publikasi</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/jenispub2/store">
              {{ csrf_field() }}
              
              <div class="card-body">
                  <div class="form-group">
                    <label>Jenis Publikasi</label>
                    <input type="string" name="jenis_publikasi" class="form-control" placeholder="Jenis Publikasi">
                @if($errors->has('jenis_publikasi'))
                   <div class="text-danger">
                      {{ $errors->first('jenis publikasi ')}}
                   </div>
                      @endif
                </div>

                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="jenispub1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection