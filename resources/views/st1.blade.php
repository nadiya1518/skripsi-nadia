@extends('layouts.backend')

@section('title','Standar 1')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Standar 1</a></li>
              <li class="breadcrumb-item active">Visi, Misi dan Sasaran</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header bg-info">
          <h1 class="card-title">Visi, Misi dan Sasaran</h1>
        </div>
        <tr>
        <div class="card-body">

          
         <h4><b>Visi</b></h4>

         <p><b>"Program Studi Teknik Informatika Sepuluh Terbaik Nasional Tahun 2025 dengan Kekhasan Dalam Bidang ICT in Agriculture"</b></p>

         <h4><b>Misi</b></h4>

          <ol>
            <li>Menyelenggarakan pendidikan sarjana teknik informatika yang berkualitas internasional.</li>
            <li>Mengembangkan riset-riset unggulan yang berorientasi paten dan terpublikasi internasional.</li>
            <li>Menyelenggarakan kegiatan pengabdian kepada masyarakat terutama dalam penerapan teknologi informasi berbasis hasil penelitian.</li>
            <li>Menjalin dan meningkatkan kerjasama kemitraan yang saling menguntungkan di tingkat lokal, nasional dan internasional.</li><br>
          </ol>

        <h4><b>Tujuan</b></h4>

          </p><ol>
           <li>Menghasilkan lulusan sarjana Teknik Informatika yang berkualitas internasional, beriman, bertakwa dan mempunyai kemampuan dalam bidang ICT in agriculture dan enterpreneurship.</li>
            <li>Menghasilkan produk yang berpotensi paten melalui penelitian terapan.</li>
            <li>Menghasilkan teknologi informasi berbasis hasil penelitian untuk pengabdian kepada masyarakat dan bangsa.</li>
            <li>Menghasilkan Program Studi Teknik Informatika yang menjadi pusat kepakaran dalam bidang ICT in agriculture.</li>
          </ol><br>

        <h4><b>Sasaran</b></h4>
          <p>Sasaran Strategis Program Studi Teknik Informatika Unila ada 4, yaitu:</p>
        <ol>
          <li>Meningkatnya kualitas pembelajaran dan kreativitas mahasiswa</li>
          <li>Meningkatnya relevansi dan produktivitas penelitian</li>
          <li>Meningkatnya kualitas dan kuantitas pengabdian kepada masyarakat</li>
          <li>Meningkatnya kualitas dan kuantitas pendukung tri darma perguruan tinggi</li>
        </ol>

          </ol>


        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  @endsection