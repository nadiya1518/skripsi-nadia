@extends('layouts.backend')

@section('title','Rekapitulasi Pengeluaran Dana')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Pengeluaran Dana</a></li>
              <li class="breadcrumb-item active">Daftar Rekapitulasi Pengeluaran Dana</li>
            </ol>
          </div> 
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Rekapitulasi Pengeluaran Dana</i></h5>
            <tr>
              <td>
                <a href="rekappengeluaran2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Daftar Rekapitulasi</i></button></a><br>
              </td><br>
            </tr>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th width="5%">No.</th>
                                <th width="20%">Jenis Pengeluaran</th>
                                <th width="20%">Sumber Pembiayaan</th>
                                <th width="10%">Waktu Pengeluaran</th>
                                <th width="10%">Unit Pelaksana Prodi</th>
                                <th width="10%">Prodi</th>
                                <th width="10%">Biaya</th>
                                <th width="10%">Aksi</th> 
                            </tr>
                        </thead>
                       <tbody>
                        <?php $i =0; ?>
                         @foreach($rekap_pengeluaran as $p)
                        <tr>
                          <td>{{ ++$i }}</td>
                          <td>{{$p->Pengeluaran->nama_jenis_pengeluaran }} </td>
                          <td>{{ $p->Sumber->sumber_pembiayaan }}</td>
                          <td>{{ $p->waktu_pengeluaran }}</td>

                          @if($p->apakah_pd_upps ==1)
                          <td>
                            <i class="fa fa-check"></i>
                          </td>
                          @elseif ($p->apakah_pd_upps ==0)
                          <td>
                          <i class="fas fa-times"></i>
                          </td>
                          @endif()
                          </td>

                           @if($p->apakah_pd_ps ==1)
                          <td><i class="fa fa-check"></i></td>
                          @elseif ($p->apakah_pd_ps ==0)
                          <td><i class="fa fa-times"></i></td>
                          @endif()
                          </td>

                          <td>{{ ($p->biaya)+0 }}</td>
                          <td>
                          <a href="{{route('rekap_pengeluaran.edit',$p->id_rekap_pengeluaran)}}"<i class="far fa-edit btn btn-sm btn-success"></i></a>
                          <a href="{{route('rekap_pengeluaran.delete',$p->id_rekap_pengeluaran)}}" <i class="far fa-trash-alt btn btn-sm btn-danger">
                          </td>
                        </tr>
                        @endforeach
                     </tbody>
                    </table>


        </div>
   
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection