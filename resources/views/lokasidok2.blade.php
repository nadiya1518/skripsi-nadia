@extends('layouts.backend')

@section('title','Tambah Lokasi Dokumen')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Lokasi Dokumen</a></li>
              <li class="breadcrumb-item active">Tambah Lokasi Dokumen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Tambah Lokasi Dokumen</i></h5>
      </div>
                
                <form method="post" action="/lokasidok2/store">
                {{ csrf_field() }} 
                <div class="card-body">
                  <div class="form-group">
                    <label>Lokasi Dokumen*</label>
                    <input type="text" name="lokasi_dokumen" class="form-control" placeholder="Lokasi Dokumen">

                    @if($errors->has('lokasidokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('lokasidokumen')}}
                                </div>
                    @endif
                    </div>

                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="lokasidok1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>

      </div>
    </section>
  </div>
@endsection