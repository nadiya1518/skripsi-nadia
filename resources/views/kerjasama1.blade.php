@extends('layouts.backend')

@section('title','Kerjasama')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kerjasama</a></li>
              <li class="breadcrumb-item active">Daftar Kerjasama</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Kerjasama</i></h5>
            <tr>
              <td>
                <a href="kerjasama2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Data Kerjasama</i></button></a><br>
              </td><br>
            </tr>

        <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th width="20px">No.</th>
              <th width="300px">Lembaga Mitra</th>
              <th>Tingkat</th>
              <th width="300px">Judul Kegiatan Kerjasama</th>
              <th width="200px">Manfaat Kerjasama</th>
              <th width="50px">Waktu Mulai Kerjasama</th>
              <th width="50px">Waktu Berakhir Kerjasama</th>
              <th width="150px">Aksi</th>
            </tr>
          </thead>
        <tbody>
       <?php $i = 0; ?>
       @foreach($kerjasama as $b)
         <tr>
          <td>{{ ++$i }}</td>
         <td>{{ $b->lembaga_mitra }}</td>
         <td>{{ $b->tingkat }}</td>
         <td>{{ $b->judul_kegiatan_kerjasama}}</td>
         <td>{{ $b->manfaat_kerjasama}}</td>
         <td>{{ $b->waktu_mulai_kerjasama}}</td>
         <td>{{ $b->waktu_berakhir_kerjasama}}</td>
         <td>
           <a href="{{route('kerjasama.edit',$b->id_kerjasama)}}"<i class="far fa-edit btn btn-sm btn-success"></i></a>
           <a href="{{route('kerjasama.delete',$b->id_kerjasama)}}"<i class="far fa-trash-alt btn btn-sm btn-danger"></i>
         </td>
         @endforeach
    </tbody>
    </table>
</body>



        </div>
   
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection