@extends('layouts.backend')

@section('title','Tambah Daftar Prestasi')

@section('content')

<?php
$prestasimhs=[1=>'Akademik',0=>'Non-Akademik', ];

?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Prestasi Mahasiswa</a></li>
              <li class="breadcrumb-item active">Tambah Daftar Prestasi Mahasiswa</li>
            </ol>
          </div> 
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Daftar Prestasi Mahasiswa</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="/mhsprestasi2/store" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}

              <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Nama Mahasiswa*</label>
                    <select name="nama_mhs" class="form-control select2" style="width: 100%;">
                      <option selected="selected"></option>
                      @foreach($mahasiswa as $semuaprestasi)
                     <option value="{{$semuaprestasi->nama_mhs}}">{{ $semuaprestasi->nama_mhs}} ({{$semuaprestasi->npm}}) </option>
                      @endforeach
                    </select>
                    @if($errors->has('namamhs'))
                                <div class="text-danger">
                                    {{ $errors->first('id_mhs')}}
                                </div>
                    @endif
                </div>

                  <div class="form-group">
                    <label>Nama Kegiatan</label>
                    <input type="string" name="nama_kegiatan" class="form-control" placeholder="Masukkan Nama Kegiatan">
                    @if($errors->has('namakegiatan'))
                                <div class="text-danger">
                                    {{ $errors->first('namakegiatan')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Waktu Perolehan</label>
                    <input type="string" name="waktu_perolehan" class="form-control" placeholder="Waktu Perolehan">
                    @if($errors->has('waktu_perolehan'))
                                <div class="text-danger">
                                    {{ $errors->first('waktu_perolehan')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                        <label>Tingkat</label>
                        <select name="tingkat" class="form-control select2" style="width: 100%;">">
                          <option>--Pilih--</option>
                          <option>Wilayah/Lokal</option>
                          <option>Nasional</option>
                          <option>Internasional</option>
                        </select>
                        @if($errors->has('tingkat'))
                                <div class="text-danger">
                                    {{ $errors->first('tingkat')}}
                                </div>
                    @endif
                      </div>
                     

                  <div class="form-group">
                  <label>Jenis Prestasi</label>
                    <select name="jenis_prestasi" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($prestasimhs as $prestasimhs =>$prestasi)
                      <option value="{{$prestasimhs}}">{{$prestasi}}</option>
                      @endforeach
                    </select>
                      @if($errors->has('prestasi_mhs'))
                                <div class="text-danger">
                                    {{ $errors->first('$prestasi_mhs')}}
                                </div>
                      @endif
                    </select>
                  </div>


                    <div class="form-group">
                        <label>Prestasi Yang Dicapai</label>
                        <input type="string" name="prestasi_yang_dicapai" class="form-control" placeholder="Prestasi Yang Dicapai">
                        @if($errors->has('prestasi_yang_dicapai'))
                                <div class="text-danger">
                                    {{ $errors->first('prestasi_yang_dicapai')}}
                                </div>
                    @endif
                    </div>

                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon far fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection