@extends('layouts.backend')

@section('title','Edit Riwayat Pendidikan Dosen')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Riwayat Pendidikan</a></li>
              <li class="breadcrumb-item active">Edit Riwayat Pendidikan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Riwayat Pendidikan</h3>
          </div>

          <form method="post" action="{{route('riwayat.update',$riwayat_pend_formal_dosen->id_riwayatt)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Nama Dosen</label>
                    <select name="id_dosen" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($dosen as $semuadosen)
                      <option value="{{$semuadosen->id_dosen}}">{{ $semuadosen->nama}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama_dosen'))
                                <div class="text-danger">
                                    {{ $errors->first('id_dosen')}}
                                </div>
                    @endif
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Perguruan Tinggi</label>
                    <input type="string" name="perguruan_tinggi" class="form-control" placeholder="Perguruan Tinggi">
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('nama')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Fakultas</label>
                    <input type="string" name="fakultas" class="form-control" placeholder="Fakultas">
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('nama')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Jurusan</label>
                    <input type="string" name="jurusan" class="form-control" placeholder="Jurusan">
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('nama')}}
                                </div>
                    @endif
                  </div>



                  <div class="form-group">
                    <label>Program Studi</label>
                    <input type="string" name="prodi" class="form-control" placeholder="Program Studi">
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('nama')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Jenjang Pendidikan</label>
                    <select name="id_jenjang" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenjang_pendidikan as $semuajenjang)
                      <option value="{{$semuajenjang->id_jenjang}}">{{ $semuajenjang->nama_jenjang}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama_jenjang'))
                                <div class="text-danger">
                                    {{ $errors->first('id_jenjang')}}
                                </div>
                    @endif
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Tahun Lulus</label>
                    <input type="string" name="waktu_lulus" class="form-control" placeholder="Tanggal dan Tahun Lulus">
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('nama')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Judul Tugas AKhir</label>
                    <input type="string" name="judul_ta" class="form-control" placeholder="Judul Tugas AKhir">
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('nama')}}
                                </div>
                    @endif
                  </div>

            
                  
                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection