<!doctype html>
<html> 
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>Dashboard Akreditasi Program Studi|Edit Standar</title>
    </head>

    <body>
        <div class="container">
            <div class="card mt-5">
                <div class="card-header text-center">
                    <strong>Edit Dokumen</strong> 
                </div>

                <form method="post" action="{{route('dokumen.update',$upload->id_upload)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Standar*</label>
                    <input type="string" name="{{ $standar->nama_standar }}" class="form-control" placeholder="Kode Standar" value=" {{ $standar->nama_standar }} ">

                    @if($errors->has('kodestandar'))
                                <div class="text-danger">
                                    {{ $errors->first('kodestandar')}}
                                </div>
                    @endif

                  </div>


                    <div class="form-group">
                    <label>Baku Mutu*</label>
                    <input type="text" name="{{ $bakumutu->baku_mutu }}" class="form-control" rows="3" placeholder="Nama Standar" value="{{ $bakumutu->baku_mutu }}">

                    @if($errors->has('bakumutu'))
                                <div class="text-danger">
                                    {{ $errors->first('bakumutu')}}
                                </div>
                     @endif
                    </div>

                    <div class="form-group">
                    <label>Lokasi Dokumen*</label>
                    <input type="text" name="{{ $lokasidokumen->lokasi_dokumen }}" class="form-control" rows="3" placeholder="Nama Standar" value="{{ $lokasidokumen->lokasi_dokumen }}">

                    @if($errors->has('lokasidokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('lokasidokumen')}}
                                </div>
                     @endif
                    </div>

                    <div class="form-group">
                    <label>Jenis Dokumen*</label>
                    <input type="text" name="{{ $jenisdokumen->jenis_dokumen }}" class="form-control" rows="3" placeholder="Nama Standar" value="{{ $jenisdokumen->jenis_dokumen }}">

                    @if($errors->has('jenisdokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('jenisdokumen')}}
                                </div>
                     @endif
                    </div>


                    <div class="form-group">
                    <label for="exampleInputFile">File</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input name="file" type="file" class="custom-file-input" id="exampleInputFile">
                        @if($errors->has('file'))
                                <div class="text-danger">
                                    {{ $errors->first('file')}}
                                </div>
                        @endif
                        <label class="custom-file-label" for="exampleInputFile">Pilih file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Keterangan*</label>
                    <input type="text" name="{{ $dokumen->keterangan }}" class="form-control" rows="3" placeholder="Nama Standar" value="{{ $dokumen->keterangan }}">

                    @if($errors->has('keterangan'))
                                <div class="text-danger">
                                    {{ $errors->first('keterangan')}}
                                </div>
                     @endif
                    </div>

                    <div class="form-group">
                    <label>Nama Dokumen*</label>
                    <input type="text" name="{{ $dokumen->nama_dok }}" class="form-control" rows="3" placeholder="Nama Standar" value="{{ $dokumen->nama_dok }}">

                    @if($errors->has('namadokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('namadokumen')}}
                                </div>
                     @endif
                    </div>
                
                <!-- /.card-body -->
                  <button type="submit" class="btn btn-primary btn-flat" value="Simpan">Simpan</button>
                  <br>

                  <br><a href="/halstan" class="btn btn-danger btn-flat">Kembali</a>
                  </div>
                  </form>
 
                </div>
            </div>
        </div>
    </body>
</html>