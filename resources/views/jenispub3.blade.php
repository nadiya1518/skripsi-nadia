@extends('layouts.backend')

@section('title','Edit Jenis Publikasi')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Publikasi</a></li>
              <li class="breadcrumb-item active">Edit Jenis Publikasi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Jenis Publikasi</h3>
          </div>

                <form method="post" action="{{route('jenis_publikasi.update',$jenis_publikasi->id_jenis_publikasi)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Jenis Publikasi*</label>
                    <input type="string" name="jenis_publikasi" class="form-control" placeholder="Jenis Publikasi" value=" {{ $jenis_publikasi->jenis_publikasi }} ">

                    @if($errors->has('jenispublikasi'))
                                <div class="text-danger">
                                    {{ $errors->first('jenispublikasi')}}
                                </div>
                    @endif

                  </div>
                
                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>
              </div>
            </form>
          </div>


      </div>
    </section>
  </div>
@endsection