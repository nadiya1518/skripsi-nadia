@extends('layouts.backend')

@section('title','Tambah Daftar Penelitian Dosen dan Mahasiswa')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Penelitian Dosen dan Mahasiswa</a></li>
              <li class="breadcrumb-item active">Tambah Daftar Penelitian</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Daftar Penelitian</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
                  <div class="form-group">
                    <label>Nama Dosen</label>
                    <input type="string" name="nama_dosen" class="form-control" placeholder="Nama Dosen">
                  </div>
                  <div class="form-group">
                    <label>Nama Mahasiswa</label>
                    <input type="string" name="nama_mhs" class="form-control" placeholder="Nama Mahasiswa">
                  </div>
                  <div class="form-group">
                    <label>Tema Penelitian Sesuai Roadmap</label>
                    <input type="string" name="tema_penelitian" class="form-control" placeholder="Tema Penelitian Sesuai Roadmap">
                  </div>
                  <div class="form-group">
                    <label>Judul Kegiatan</label>
                    <input type="string" name="judul_kegiatan" class="form-control" placeholder="Judul Kegiatan">
                  </div>
                  <div class="form-group">
                    <label>Tahun</label>
                    <input type="string" name="tahun" class="form-control" placeholder="Tahun">
                  </div>
                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon far fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection