@extends('layouts.backend')

@section('title','Jenis Aspek Kepuasan Mahasiswa')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Aspek Kepuasan</a></li>
              <li class="breadcrumb-item active">Daftar Jenis Aspek Kepuasan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Jenis Aspek Kepuasan</i></h5>
            <tr>
              <td>
                <a href="jenisaspek2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Jenis Aspek</i></button></a><br>
              </td><br>
            </tr>
            <br/>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th width="20px">No.</th>
                                <th width="900px">Aspek yang Diukur</th>
                                <th width="100px">Aksi</th>
                            </tr>
                        </thead>
                       <tbody>
                        <?php $i = 0; ?>
                         @foreach($jenis_aspek_kepuasan_mhs as $p)
                        <tr>
                          <td>{{ ++$i }}</td>
                          <td>{{ $p->jenis_aspek_kepuasan }}</td>
                          <td>
                            <a href="{{route('jenis_aspek.edit',$p->id_jenis_aspek)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

                            <a href="{{route('jenis_aspek.delete',$p->id_jenis_aspek)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                          </td>
                        </tr>
                        @endforeach
                     </tbody>
                    </table>

        </div>
 
      </div>

    </section>

  </div>

@endsection