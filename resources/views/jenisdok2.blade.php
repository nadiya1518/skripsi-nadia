@extends('layouts.backend')

@section('title','Tambah Jenis Dokumen')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Dokumen</a></li>
              <li class="breadcrumb-item active">Tambah Jenis Dokumen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Tambah Jenis Dokumen</i></h5>
      </div>

                <form method="post" action="/jenisdok2/store">
                {{ csrf_field() }}   

                <div class="card-body">
                  <div class="form-group">
                    <label>Jenis Dokumen*</label>
                    <input type="text" name="jenis_dokumen" class="form-control" placeholder="Jenis Dokumen">

                     @if($errors->has('jenisdokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('jenisdokumen')}}
                                </div>
                    @endif
                    </div>
              
                <!-- /.card-body -->
                  <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="haljenis"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>
        <!-- /.card-body -->

        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection