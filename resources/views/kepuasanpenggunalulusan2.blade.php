@extends('layouts.backend')

@section('title','Tambah Data Kepuasan Pengguna Lulusan')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kepuasan Pengguna Lulusan</a></li>
              <li class="breadcrumb-item active">Tambah Data Kepuasan Pengguna Lulusan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Kepuasan Pengguna Lulusan</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
                  <div class="form-group">
                    <label>Jenis Kemampuan</label>
                    <input type="string" name="jenis_kemampuan" class="form-control" placeholder="Jenis Kemampuan">
                  </div>

                  <div class="form-group">
                    <label>Tingkat</label>

                     <div class="form-check">
                          <input class="form-check-input" type="radio" name="tingkat">
                          <label class="form-check-label">Sangat Baik</label>
                     </div>

                     <div class="form-check">
                          <input class="form-check-input" type="radio" name="tingkat">
                          <label class="form-check-label">Baik</label>  
                     </div>

                     <div class="form-check">
                          <input class="form-check-input" type="radio" name="tingkat">
                          <label class="form-check-label">Cukup</label>
                     </div>

                     <div class="form-check">
                          <input class="form-check-input" type="radio" name="tingkat">
                          <label class="form-check-label">Kurang</label>
                     </div>
                  </div>
                        
                  <br>
                  <div class="form-group">
                    <label>Rencana Tindak Lanjut oleh UPPS/PS</label>
                    <input type="string" name="rencana_upps_ps" class="form-control" placeholder="Masukkan Rencana Tindak Lanjut oleh UPPS/PS">
                  </div>
                  
                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection