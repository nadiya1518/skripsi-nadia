@extends('layouts.backend')

@section('title','Kategori Capaian')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kategori Capaian</a></li>
              <li class="breadcrumb-item active">Daftar Kategori Capaian</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Kategori Capaian</i></h5>
            <tr>
              <td>
                <a href="kategoricapaian2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Kategori Capaian</i></button></a><br>
              </td><br>
            </tr>

                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th width="20px">No.</th>
                                <th>Kategori Capaian</th>
                                <th width="100px">Aksi</th>
                            </tr>
                        </thead>
                       <tbody>
                        <?php $i =0; ?>
                         @foreach($kategori_capaian as $p)
                        <tr>
                          <td>{{ ++$i }}</td>
                          <td>{{ $p->kategori_capaian }}</td>
                          
                          <td>
                          <a href="{{route('kategori_capaian.edit',$p->id_kat_capaian)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>
                          <a href="{{route('kategori_capaian.delete',$p->id_kat_capaian)}}" <i class="far fa-trash-alt btn btn-sm btn-danger">
                          </td>
                        </tr>
                        @endforeach
                     </tbody>
                    </table>


        </div>
      </div>
    </section>
  </div>

@endsection