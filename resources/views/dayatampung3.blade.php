@extends('layouts.backend')

@section('title','Edit Data Daya Tampung')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Daya Tampung</a></li>
              <li class="breadcrumb-item active">Edit Data Daya Tampung</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Data Daya Tampung</h3>
          </div>

                <form method="post" action="{{route('daya_tampung.update',$daya_tampung->id_daya_tampung)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Pelaksana</label>
                    <input type="string" name="pelaksana" class="form-control" placeholder="Pelaksana" value=" {{ $daya_tampung->pelaksana }} ">

                    @if($errors->has('pelaksana'))
                                <div class="text-danger">
                                    {{ $errors->first('pelaksana')}}
                                </div>
                    @endif

                  </div>
                   

                   <div class="form-group">
                    <label for="exampleInputBakumutu1">Jenis Penerimaan*</label>
                    <select name="id_jenis_penerimaan" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenispenerimaan as $satuanjalur)
                     <option value="{{$daya_tampung->id_jenis_penerimaan}}">{{ $satuanjalur->jenis_penerimaan}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('jenis_penerimaan'))
                                <div class="text-danger">
                                    {{ $errors->first('id_jenis_penerimaan')}}
                                </div>
                    @endif
                </div>

                  <div class="form-group">
                    <label>Total Daya Tampung</label>
                    <input type="string" name="total_daya_tampung" class="form-control" placeholder="Total Daya Tampung" value=" {{ $daya_tampung->total_daya_tampung}} ">

                    @if($errors->has('total'))
                                <div class="text-danger">
                                    {{ $errors->first('total')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Pendaftar</label>
                    <input type="string" name="pendaftar" class="form-control" placeholder="Pendaftar" value=" {{ $daya_tampung->pendaftar }} ">

                    @if($errors->has('pendaftar'))
                                <div class="text-danger">
                                    {{ $errors->first('pendaftar')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Lulus Seleksi</label>
                    <input type="string" name="lulus_seleksi" class="form-control" placeholder="Lulus Seleksi" value=" {{ $daya_tampung->lulus_seleksi }} ">

                    @if($errors->has('lulus'))
                                <div class="text-danger">
                                    {{ $errors->first('lulus')}}
                                </div>
                    @endif

                  </div>

                  
                
                <!-- /.card-body -->
                  <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection