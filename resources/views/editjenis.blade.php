@extends('layouts.backend')

@section('title','Edit Jenis Dokumen')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Dokumen</a></li>
              <li class="breadcrumb-item active">Edit Jenis Dokumen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Jenis Dokumen</h3>
          </div>

                <form method="post" action="{{route('jenis_dokumen.update',$jenis_dokumen->id_jenis)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Jenis Dokumen*</label>
                    <input type="string" name="jenis_dokumen" class="form-control" placeholder="Jenis Dokumen" value=" {{ $jenis_dokumen->jenis_dokumen }} ">

                    @if($errors->has('jenisdokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('jenisdokumen')}}
                                </div>
                    @endif
                  </div>
                
                <!-- /.card-body -->
                   <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </body>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection