@extends('layouts.backend')

@section('title','Edit Lokasi Dokumen')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Lokasi Dokumen</a></li>
              <li class="breadcrumb-item active">Edit Lokasi Dokumen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Lokasi Dokumen</h3>
          </div>
                <form method="post" action="{{route('lokasi_dokumen.update',$lokasi_dokumen->id_lok)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                  <div class="card-body">
                  <div class="form-group">
                    <label>Lokasi Dokumen*</label>
                    <input type="string" name="lokasi_dokumen" class="form-control" placeholder="Lokasi Dokumen" value=" {{ $lokasi_dokumen->lokasi_dokumen }} ">

                    @if($errors->has('lokasidokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('lokasidokumen')}}
                                </div>
                    @endif
                  </div>
                
                <!-- /.card-body -->
                  <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </body>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection