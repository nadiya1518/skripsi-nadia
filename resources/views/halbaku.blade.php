@extends('layouts.backend')

@section('title','Baku Mutu')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Referensi</a></li>
              <li class="breadcrumb-item active">Baku Mutu</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Baku Mutu</i></h5>
            <tr>
              <td>
                <a href="tbaku"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Baku Mutu</i></button></a><br>
              </td>
            </tr>
      </div>
   <body>   

    <table class="table table-bordered table-hover table-striped">
      <thead>
          <tr>
            <th width="20px">No.</th>
            <th>Standar</th>
            <th>Butir</th>
            <th>Nama Baku Mutu</th>
            <th width="100">Aksi</th>
            </tr>
      </thead>
    <tbody>
       <?php $i = 0; ?>
       @foreach($baku_mutu as $b)
         <tr> 
          <td>{{ ++$i }}</td>
          <td>{{ $b->standar }}</td>
          <td>{{ $b->butir }}</td>
          <td>{{ $b->baku_mutu }}</td>
          <td>
            <a href="{{route('baku_mutu.edit',$b->id_baku)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

            <a href="{{route('baku_mutu.delete',$b->id_baku)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
          </td>
         </tr>
         @endforeach
  </tbody>
  </table>
  </body>

</div>
</section>
</div>
@endsection