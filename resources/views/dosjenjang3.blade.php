@extends('layouts.backend')

@section('title','Edit Jenjang Pendidikan Dosen')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenjang Pendidikan</a></li>
              <li class="breadcrumb-item active">Edit Jenjang Pendidikan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Jenjang Pendidikan</h3>
          </div>

                <form method="post" action="{{route('jenjang_pend.update',$jenjang_pendidikan->id_jenjang)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                   <form role="form">
                    <div class="card-body">
                    <div class="form-group">
                    <label>Jenjang Pendidikan*</label>
                    <input type="string" name="nama_jenjang" class="form-control" placeholder="Jenjang Pendidikan" value=" {{ $jenjang_pendidikan->nama_jenjang }} ">

                    @if($errors->has('nama_jenjang'))
                                <div class="text-danger">
                                    {{ $errors->first('nama_jenjang')}}
                                </div>
                    @endif

                  </div>
                <!-- /.card-body -->

                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>
              </div>
            </form>
          </div>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection