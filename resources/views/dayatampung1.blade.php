@extends('layouts.backend')

@section('title','Daya Tampung')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Daya Tampung</a></li>
              <li class="breadcrumb-item active">Daftar Data Daya Tampung</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Data Daya Tampung</i></h5>
            <tr>
              <td>
                <a href="dayatampung2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Data Daya Tampung</i></button></a><br>
              </td><br>
            </tr>

            <br>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                               <th width="20px">No.</th>
                                <th width="500px">Pelaksana</th>
                                <th width="300">Tahun Masuk</th>
                                <th width="500px">Jenis Penerimaan</th>
                                <th width="400px">Total Daya Tampung</th>
                                <th width="100px">Pendaftar</th>
                                <th width="500px">Lulus Seleksi</th>
                                <th width="300px">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; ?>
                            @foreach($daya_tampung as $p)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $p->apakah_pd_ps }}</td>
                                @if($p->apakah_pd_ps ==1)
                                <td>Teknik Informatika</td>
                                @elseif ($p->apakah_pd_ps ==0)
                                <td>Tidak Aktif</td>
                                @endif()
                                </td>
                                <td>{{ $p->tahun_masuk }}</td>
                                <td>{{ $p->JenisPenerimaan->jenis_penerimaan }}</td>
                                <td>{{ $p->total_daya_tampung }}</td>
                                <td>{{ $p->pendaftar }}</td>
                                <td>{{ $p->lulus_seleksi }}</td>
                                <td>
                                   <a href="{{route('daya_tampung.edit',$p->id_daya_tampung)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

                                   <a href="{{route('daya_tampung.delete',$p->id_daya_tampung)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


        </div>
   
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection