@extends('layouts.backend')

@section('title','Mahasiswa')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Mahasiswa</a></li>
              <li class="breadcrumb-item active">Daftar Mahasiswa</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Mahasiswa</i></h5>
            <tr>
              <td>
                <a href="mhs2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Daftar Mahasiswa</i></button></a><br>
              </td><br>
            </tr> 

            <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th width="20px">No.</th>
              <th width="400px">Nama Mahasiswa</th>
              <th width="100px">NPM</th>
              <th width="250px">Program Studi</th>
              <th width="80px">Status</th>
              <th width="200px">Jenis Mahasiswa</th>
              <th width="200">Jenis Penerimaan</th>
              <th width="150px">Aksi</th>
            </tr>
          </thead>
        <tbody>
       <?php $i = 0; ?>
       @foreach($mahasiswa as $b)
         <tr>
          <td>{{ ++$i }}</td>
         <td>{{ $b->nama_mhs }}</td>
         <td>{{ $b->npm }}</td>
         <td>{{ $b->ProgramStudi->nama_prodi}}</td>

         @if($b->status_keaktifan ==1)
         <td>Aktif</td>
         @elseif ($b->status_keaktifan ==0)
         <td>Tidak Aktif</td>
         @endif()
         </td>

         <td>{{ $b->jenis_mahasiswa}}</td>
         <td>{{ $b->JenisPenerimaan->jenis_penerimaan}}</td>

         

         <td>
           <a href="{{route('mahasiswa.edit',$b->id_mhs)}}"<i class="far fa-edit btn btn-sm btn-success"></i></a>
           <a href="{{route('mahasiswa.delete',$b->id_mhs)}}"<i class="far fa-trash-alt btn btn-sm btn-danger"></i>
         </td>
         @endforeach
    </tbody>
    </table>
</body>


        </div>

      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection