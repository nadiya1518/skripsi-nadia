@extends('layouts.backend')

@section('title','Tambah PkM Dosen')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">PkM Dosen</a></li>
              <li class="breadcrumb-item active">Tambah PkM Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah PkM Dosen</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
              <div class="form-group">
                  <label>Nama Dosen</label>
                  <input type="string" name="nama_dosen" class="form-control" placeholder="Nama Dosen">
              </div>

              <div class="form-group">
                  <label>Judul PkM</label>
                  <input type="string" name="jdl_pkm_dos" class="form-control" placeholder="Judul PkM">
              </div>

              <div class="form-group">
                  <label>Jenis PkM</label>
                  <input type="string" name="jenis_pkm" class="form-control" placeholder="Jenis PkM">
              </div>

              <div class="form-group">
                  <label>Lokasi PkM</label>
                  <input type="string" name="lokasi_pkm" class="form-control" placeholder="Lokasi PkM">
              </div>

              <div class="form-group">
                  <label>Tahun PkM</label>
                  <input type="string" name="tahun_pkm" class="form-control" placeholder="Tahun PkM">
              </div>

              <div class="form-group">
                  <label>Abstrak</label>
                  <input type="string" name="abstrak" class="form-control" placeholder="Abstrak">
              </div>

              <div class="form-group">
                  <label>Biaya PkM</label>
                  <input type="string" name="biaya_pkm" class="form-control" placeholder="Biaya PkM">
              </div>

              <div class="form-group">
                <label>Sumber Pembiayaan</label>
                  <select name="sumber_biaya" class="form-control select2" style="width: 100%;">">
                    <option>--Pilih--</option>
                    <option>Perguruan Tinggi</option>
                    <option>Mandiri</option>
                    <option>Lembaga Dalam Negeri (Diluar Perguruan Tinggi)</option>
                    <option>Lembaga Luar Negeri</option>
                  </select>
              </div>
                </div>
                  </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
              </form>
            </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection