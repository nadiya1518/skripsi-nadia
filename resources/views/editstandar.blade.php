@extends('layouts.backend')

@section('title','Edit Standar')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Standar</a></li>
              <li class="breadcrumb-item active">Edit Standar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Standar</h3>
          </div>

        <form method="post" action="{{route('standar.update',$standar->id_standar)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Kode Standar*</label>
                    <input type="string" name="kode_standar" class="form-control" placeholder="Kode Standar" value=" {{ $standar->kode_standar }} ">

                    @if($errors->has('kode'))
                                <div class="text-danger">
                                    {{ $errors->first('kode')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Nama Standar*</label>
                    <input type="text" name="nama_standar" class="form-control" rows="3" placeholder="Nama Standar" value="{{ $standar->nama_standar}}">

                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('nama')}}
                                </div>
                     @endif

                    </div>
                  </div>
                
              

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </body>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection