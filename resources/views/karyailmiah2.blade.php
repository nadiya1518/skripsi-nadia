@extends('layouts.backend')

@section('title','Tambah Karya Ilmiah')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Karya Ilmiah</a></li>
              <li class="breadcrumb-item active">Tambah Karya Ilmiah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Karya Ilmiah</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
                  <div class="form-group">
                    <label>Judul Artikel</label>
                    <input type="string" name="judul_artikel" class="form-control" placeholder="Judul Artikel">
                  </div>

                  <div class="form-group">
                    <label>Jumlah Sitasi</label>
                    <input type="string" name="jumlah_sitasi" class="form-control" placeholder="Jumlah Sitasi">
                  </div>


                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection