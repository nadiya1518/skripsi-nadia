@extends('layouts.backend')

@section('title','Tambah Jenis Prestasi')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Prestasi</a></li>
              <li class="breadcrumb-item active">Tambah Jenis Prestasi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Jenis Prestasi</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/jenisprestasi2/store">
              {{ csrf_field() }}
              <div class="card-body"> 
                  <div class="form-group">
                    <label>Jenis Prestasi</label>
                    <input type="string" name="jenis_prestasi" class="form-control" placeholder="Jenis Prestasi">
                  </div>
                </div>

                @if($errors->has('jenisprestasi'))
                   <div class="text-danger">
                      {{ $errors->first('jenis prestasi')}}
                   </div>
                      @endif
                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection