@extends('layouts.backend')

@section('title','Edit Daftar Pembimbing Utama')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) --> 
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Pembimbing Utama Ta</a></li>
              <li class="breadcrumb-item active">Edit Daftar Pembimbing Utama</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Daftar Pembimbing Utama</h3>
          </div>

                <form method="post" action="{{route('pembimbing.update',$pembimbing_tugas_akhir->id_pembimbing_utama)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Nama Dosen Pembimbing</label>
                    <select name="id_dosen" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($dosen as $semuadosen)
                     <option value="{{$semuadosen->id_dosen}}">{{ $semuadosen->nama_dosen}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('id_dosen')}}
                                </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="exampleInputBakumutu1">Nama Mahasiswa Bimbingan</label>
                    <select name="id_mhs" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($mahasiswa as $semuamhs)
                     <option value="{{$semuamhs->id_mhs}}">{{ $semuamhs->nama_mhs}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('id_mhs')}}
                                </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="exampleInputBakumutu1">Program Studi</label>
                    <select name="id_prodi" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($prodi as $semuaprodi)
                     <option value="{{$semuaprodi->id_prodi}}">{{ $semuaprodi->nama_prodi}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('id_prodi')}}
                                </div>
                    @endif
                </div>

                <div class="form-group">
                    <label for="exampleInputBakumutu1">Jenis Bimbingan</label>
                    <select name="id_jenis_bimbingan" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenis_bimbingan as $semuajenis)
                     <option value="{{$semuajenis->id_jenis_bimbingan}}">{{ $semuajenis->jenis_bimbingan}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('id_jenis_bimbingan')}}
                                </div>
                    @endif
                </div>
                  
                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="dosbidang1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>

        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection