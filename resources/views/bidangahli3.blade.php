@extends('layouts.backend')

@section('title','Edit Bidang Keahlian Dosen')

@section('content')

<?php
$sesuaikompetensi=[0=>'sesuai', 1=>'tidak sesuai'];

$sesuaimatkul = [0=>'sesuai',1=>'tidak sesuai'];
?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Bidang Keahlian Dosen</a></li>
              <li class="breadcrumb-item active">Edit Bidang Keahlian Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Bidang Keahlian Dosen</h3>
          </div>

                <form method="post" action="{{route('keahlian.update',$bidang_keahlian->id_bidang_dosen)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                   <form role="form">
                    <div class="card-body">
                   <div class="form-group">
                    <label for="exampleInputBakumutu1">Nama Dosen</label>
                    <select name="id_dosen" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($datadosen as $semuadosen)
                     <option value="{{$semuadosen->id_dosen}}">{{ $semuadosen->nama_dosen}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('namadosen'))
                                <div class="text-danger">
                                    {{ $errors->first('id_dosen')}}
                                </div>
                    @endif
                </div>

                 <div class="form-group">
                    <label for="exampleInputBakumutu1">Bidang Keahlian</label>
                    <select name="id_jenis_keahlian" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenisbidang as $semuabidang)
                     <option value="{{$semuabidang->id_jenis_keahlian}}">{{ $semuabidang->nama_jenis_bidang}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('bidangahli'))
                                <div class="text-danger">
                                    {{ $errors->first('id_bidang_dosen')}}
                                </div>
                    @endif
                </div>

                <div class="form-group">
                  <label>Kesesuaian Bidang Keahlian Dengan Kompetensi Inti Prodi</label>
                    <select name="bidang_sesuai_kompetensi" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($sesuaikompetensi as $sesuaikompetensi =>$kompetensi)
                      <option value="{{$sesuaikompetensi}}">{{$kompetensi}}</option>
                      @endforeach
                    </select>
                      @if($errors->has('bidang_sesuai_kompetensi'))
                                <div class="text-danger">
                                    {{ $errors->first('$bidang_sesuai_kompetensi')}}
                                </div>
                      @endif
                    </select>
                  </div>


                  <div class="form-group">
                    <label>Mata Kuliah yang Diampu</label>
                    <input type="string" name="matkul_dlm_ps_a" class="form-control" placeholder="Mata Kuliah yang Diampu" value="{{$bidang_keahlian->matkul_dlm_ps_a}}">
                    @if($errors->has('matkul_pd_ps'))
                                <div class="text-danger">
                                    {{ $errors->first('matkul_pd_ps')}}
                                </div>
                    @endif
                  </div>


                  <!-- <div class="form-group">
                    <label>Mata Kuliah yang Diampu Pada Program Studi Lain</label>
                    <input type="string" name="matkul_pada_ps_lain" class="form-control" placeholder="Tahun Transfer" value=" {{$bidang_keahlian->matkul_pada_ps_lain}}">>
                    @if($errors->has('matkul_pd_ps'))
                                <div class="text-danger">
                                    {{ $errors->first('matkul_pd_ps')}}
                                </div>
                    @endif
                  </div> -->

                   <div class="form-group">
                  <label>Kesesuaian Bidang Keahlian dengan Matakuliah yang Diampu</label>
                    <select name="bidang_sesuai_matkul" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($sesuaimatkul as $sesuaimatkul =>$matkul)
                      <option value="{{$sesuaimatkul}}">{{$matkul}}</option>
                      @endforeach
                    </select>
                      @if($errors->has('bidang_sesuai_matkul'))
                                <div class="text-danger">
                                    {{ $errors->first('$bidang_sesuai_matkul')}}
                                </div>
                      @endif
                    </select>
                  </div>

                  

                <!-- /.card-body -->

               <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection