@extends('layouts.backend')

@section('title','Bidang Keahlian Dosen')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dosen</a></li>
              <li class="breadcrumb-item active">Daftar Bidang Keahlian Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Bidang Keahlian Dosen</i></h5>
            <tr>
              <td>
                <a href="bidangahli2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Bidang Keahlian Dosen</i></button></a><br>
              </td>
            </tr>
              <br>
              <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                               <th width="20px">No.</th>
                                <th>Nama Dosen</th>
                                <th>Bidang Keahlian</th>
                                <th>Kesesuaian dengan Kompetensi Inti Prodi</th>
                                <th>Kesesuaian dengan Matakuliah yang Diampu</th>  
                                <th>Matakuliah yang Diampu</th>
                                <th width="100px">Aksi</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php $i = 0; ?>
                            @foreach($bidang_keahlian as $p)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $p->Dosen->nama_dosen }}</td>
                                <td>{{ $p->JenisBidang->nama_jenis_bidang }}</td>
                                
                            @if($p->bidang_sesuai_kompetensi ==1)
                                <td>tidak sesuai</td>
                            @elseif ($p->bidang_sesuai_kompetensi ==0)
                                <td>sesuai</td>
                            @endif()
                                </td>

                            @if($p->bidang_sesuai_matkul ==1)
                                <td>tidak sesuai</td>
                            @elseif ($p->bidang_sesuai_matkul ==0)
                                <td>sesuai</td>
                            @endif()
                                </td>
                                <td>{{ $p->matkul_dlm_ps_a }}</td>
                                <td>
                                   <a href="{{route('keahlian.edit',$p->id_bidang_dosen)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

                                   <a href="{{route('keahlian.delete',$p->id_bidang_dosen)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>

        </div>

      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection