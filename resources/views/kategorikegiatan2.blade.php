@extends('layouts.backend')

@section('title','Tambah Kategori Kegiatan')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kategori Kegiatan</a></li>
              <li class="breadcrumb-item active">Tambah Kategori Kegiatan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Kategori Kegiatan</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/kategorikegiatan2/store">
              {{ csrf_field() }}
              <div class="card-body">
                  <div class="form-group">
                    <label>Nama Kategori Kegiatan</label>
                    <input type="string" name="nama_kategori" class="form-control" placeholder="Nama Kategori Kegiatan">
                  </div>
                </div>
                @if($errors->has('kategori_kegiatan'))
                   <div class="text-danger">
                      {{ $errors->first('kategori kegiatan')}}
                   </div>
                      @endif
                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection