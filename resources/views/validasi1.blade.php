@extends('layouts.backend')

@section('title','Validasi')

@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

<body>
  <section class="content">
    <div class="card">
      <div class="card-header p-2">

        <a href="{{ (route('validasi')) }}" class="tablinks">Diajukan</a>
        <a href="{{ (route('validasi')).'?kode=DISETUJUI' }}" class="tablinks">Disetujui</a>
        <a href="{{ (route('validasi')).'?kode=DITOLAK' }}" class="tablinks">Ditolak</a>  

      <div class="card-body">
        <h3>Dokumen {{ $kode }}</h3>
          <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th width="20px">No.</th>
                                <th width="250px">Nama Dokumen</th>
                                <th width="150px">Jenis Dokumen</th>
                                <th width="200px">Lokasi Dokumen</th>
                                <th width="20px">File</th>
                                <th width="180px">Aksi</th> 
                            </tr>
                        </thead>
                       <tbody>
                        <?php $i=0; ?>
                         @foreach($upload as $p)
                         <tr>
                          <td>{{ ++$i }}</td>
                          <td>{{ $p->Dokumen->nama_dok }} </td>
                          <td>{{ $p->Dokumen->JenisDokumen->jenis_dokumen }}</td>
                          <td>{{ $p->LokasiDokumen->lokasi_dokumen }}</td>
                          <td><a href="{{route('dokumen.download',$p->id_dok) }}"><i class="fa fa-download"></a></td>
                          <td>
                          <a href="{{ route('validasi.disetujui',$p->id_upload) }}"  class="btn btn-sm btn-primary"><i class="fa fa-check"> Disetujui</i></a>

                          <a href="{{ route('validasi.ditolak',$p->id_upload) }}" class="btn btn-sm btn-danger"><i class="fa fa-times"> Ditolak</i></a>
                          </td>
                        </tr>
                        @endforeach
                     </tbody>
                    </table>
                  </div>
    </div>
    </section>
   
</body>
 
@endsection
