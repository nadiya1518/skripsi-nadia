@extends('layouts.backend')

@section('title','Edit Dokumen')

@section('content')

<body>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Standar</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Upload Dokumen</a></li>
              <li class="breadcrumb-item active">Edit Dokumen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

   <section class="content">

      <!-- Default box -->
      <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3><i class="card-title">Edit Dokumen</h3>
              </div>

                <form method="post" action="/haldok/update/{{ $dokumen->id }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Baku Mutu*</label>
                    <select name="baku_mutu" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($bakumutu as $satuanbakumutu)
                      <option>{{ $satuanbakumutu->butir}} {{$satuanbakumutu->baku_mutu}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('baku_mutu'))
                                <div class="text-danger">
                                    {{ $errors->first('baku_mutu')}}
                                </div>
                    @endif
                </div>

                  <div class="form-group">
                    <label for="exampleInputLokasi1">Lokasi Dokumen*</label>
                    <select name="lokasi_dok" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($lokasidokumen as $semualokasi)
                      <option>{{ $semualokasi->lokasi_dokumen}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('lokasi_dokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('lokasi_dokumen')}}
                                </div>
                    @endif
                </div>
                  
                  <div class="form-group">
                    <label for="exampleInputJenis1">Jenis Dokumen*</label>
                    <select name="jenis_dok" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>

                      @foreach($jenisdokumen as $semuajenis)
                      <option>{{ $semuajenis->jenis_dokumen}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('jenis_dokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('jenis_dokumen')}}
                                </div>
                    @endif
                  </div>


                  <div class="form-group">
                    <label for="exampleInputFile">File</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input name="file" type="file" class="custom-file-input" id="exampleInputFile">
                        @if($errors->has('file'))
                                <div class="text-danger">
                                    {{ $errors->first('file')}}
                                </div>
                        @endif
                        <label class="custom-file-label" for="exampleInputFile">Pilih file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Keterangan*</label>
                    <input type="text" name="keterangan" class="form-control" placeholder="Keterangan">

                    @if($errors->has('keterangan'))
                                <div class="text-danger">
                                    {{ $errors->first('keterangan')}}
                                </div>
                    @endif
                    </div>
                  <div class="form-group">
                    <label for="exampleInputLink1">Link Url</label>
                    <input name="link_url" type="string" class="form-control" id="exampleInputLink1" placeholder="Masukkan Link Url">
                    @if($errors->has('link_url'))
                                <div class="text-danger">
                                    {{ $errors->first('link_url')}}
                                </div>
                    @endif
                     <br><tr>
                      <td>
                        <a href="#"><button type="button" class="btn btn-block btn-primary btn-small"><i class="nav-icon fas fa-search">&ensp;Pilih dari daftar dokumen terupload</i></button></a><br>
                      </td>
                    </tr>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputNamadokumen1">Nama Dokumen*</label>
                    <input name="nama_dok" type="string" class="form-control" id="exampleInputNamadokumen1" placeholder="Masukkan Nama Dokumen">
                    @if($errors->has('nama_dok'))
                                <div class="text-danger">
                                    {{ $errors->first('nama_dok')}}
                                </div>
                    @endif
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </body>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection