@extends('layouts.backend')

@section('title','Program Studi')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Program Studi</a></li>
              <li class="breadcrumb-item active">Daftar Program Studi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Program Studi</i></h5>
            <tr>
              <td>
                <a href="progstud2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Program Studi</i></button></a><br>
              </td><br>
            </tr>

            <br>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                               <th width="20px">No.</th>
                                <th width="500px">Nama Program Studi</th>
                                <th width="300">Status Peringkat</th>
                                <th width="400px">No.SK</th>
                                <th width="300px">Tanggal SK</th>
                                <th width="400px">Tanggal Kadaluarsa</th>
                                <th width="300px">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; ?>
                            @foreach($program_studi as $p)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $p->nama_prodi }}</td>
                                <td>{{ $p->status_peringkat }}</td>
                                <td>{{ $p->nomor_sk }}</td>
                                 <td>{{ $p->tanggal_sk }}</td>
                                <td>{{ $p->tanggal_kadaluarsa }}</td>
                                <td>
                                   <a href="{{route('prodi.edit',$p->id_prodi)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

                                   <a href="{{route('prodi.delete',$p->id_prodi)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


        </div>
   
      </div>

    </section>

  </div>

@endsection