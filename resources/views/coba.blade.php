@extends('layouts.backend')

@section('title','Edit Baku Mutu')

@section('content')

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blank Page</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Title</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body"><form method="post" action="/halbaku/update/{{ $baku_mutu->id }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}


                  <div class="form-group">
                    <label>Butir*</label>
                    <input type="string" name="butir" class="form-control" placeholder="Butir" value=" {{ $baku_mutu->butir }} ">

                    @if($errors->has('butir'))
                                <div class="text-danger">
                                    {{ $errors->first('butir')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Baku Mutu*</label>
                    <input type="text" name="baku_mutu" class="form-control" rows="3" placeholder="Baku Mutu" value="{{ $baku_mutu->baku_mutu }}">

                    @if($errors->has('bakumutu'))
                                <div class="text-danger">
                                    {{ $errors->first('bakumutu')}}
                                </div>
                     @endif

                    </div>

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection