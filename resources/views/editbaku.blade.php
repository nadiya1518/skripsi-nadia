@extends('layouts.backend')

@section('title','Edit Baku Mutu')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Baku Mutu</a></li>
              <li class="breadcrumb-item active">Edit Baku Mutu</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Baku Mutu</h3>
          </div>

        <form method="post" action="{{route('baku_mutu.update',$baku_mutu->id_baku)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Butir*</label>
                    <input type="string" name="butir" class="form-control" placeholder="Butir" value=" {{ $baku_mutu->butir }} ">

                    @if($errors->has('butir'))
                                <div class="text-danger">
                                    {{ $errors->first('butir')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Baku Mutu*</label>
                    <input type="text" name="baku_mutu" class="form-control" rows="3" placeholder="Baku Mutu" value="{{ $baku_mutu->baku_mutu }}">

                    @if($errors->has('bakumutu'))
                                <div class="text-danger">
                                    {{ $errors->first('bakumutu')}}
                                </div>
                     @endif

                    </div>
                  </div>
                
              

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </body>


        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection