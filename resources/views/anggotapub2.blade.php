@extends('layouts.backend')

@section('title','Tambah Daftar Anggota Publikasi')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Anggota Publikasi</a></li>
              <li class="breadcrumb-item active">Tambah Daftar Anggota Publikasi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Daftar Anggota Publikasi</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
                  <div class="form-group">
                    <label>Urutan Penulis</label>
                    <input type="string" name="urutan_penulis" class="form-control" placeholder="Urutan Penulis">
                  </div>
                  <div class="form-group">
                    <label>Afiliansi</label>
                    <input type="string" name="afiliansi" class="form-control" placeholder="Afiliansi">
                  </div>
                  <div class="form-group">
                    <label>Email Penulis</label>
                    <input type="string" name="email_penulis" class="form-control" placeholder="Email Penulis">
                  </div>
                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon far fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection