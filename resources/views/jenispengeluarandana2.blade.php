@extends('layouts.backend')

@section('title','Tambah Jenis Penggunaan Dana')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Penggunaan Dana</a></li>
              <li class="breadcrumb-item active">Tambah Jenis Penggunaan Dana</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Jenis Penggunaan Dana</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/jenispengeluarandana2/store">
              {{ csrf_field() }}
              <div class="card-body">
                  <div class="form-group">
                    <label>Jenis Pengeluaran Dana</label>
                    <input type="string" name="nama_jenis_pengeluaran" class="form-control" placeholder="Jenis Penggunaan">
                @if($errors->has('nama_jenis_pengeluaran'))
                   <div class="text-danger">
                      {{ $errors->first('nama jenis pengeluaran ')}}
                   </div>
                      @endif
                </div>

                <!-- /.card-body -->

                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="jenispengeluarandana1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>


      </div>

    </section>

  </div>

@endsection