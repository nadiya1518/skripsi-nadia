@extends('layouts.backend')

@section('title','Edit Data Dosen')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dosen</a></li>
              <li class="breadcrumb-item active">Edit Data Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Data Dosen</h3>
          </div>

                <form method="post" action="{{route('dosen.update',$dosen->id_dosen)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                   <form role="form">
                    <div class="card-body">
                    <div class="form-group">
                    <label>Nama Dosen*</label>
                    <input type="string" name="nama" class="form-control" placeholder="Nama Dosen" value=" {{ $dosen->nama }} ">

                    @if($errors->has('nama_dosen'))
                                <div class="text-danger">
                                    {{ $errors->first('nama_dosen')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>NIP/NIDN*</label>
                    <input type="string" name="nidn" class="form-control" placeholder="NIP/NIDN" value=" {{ $dosen->nidn }} ">

                    @if($errors->has('nidn'))
                                <div class="text-danger">
                                    {{ $errors->first('nidn')}}
                                </div>
                    @endif

                  </div>


                   <div class="form-group">
                    <label for="exampleInputBakumutu1">Status</label>
                    <select name="id_jenis_dosen" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenisdosen as $semuajenis)
                     <option value="{{$semuajenis->id_jenis_dosen}}">{{ $semuajenis->jenis_dosen}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('status'))
                                <div class="text-danger">
                                    {{ $errors->first('status')}}
                                </div>
                    @endif
                </div>

                 <div class="form-group">
                    <label for="exampleInputBakumutu1">Program Studi</label>
                    <select name="id_prodi" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($prodi as $semuaprodi)
                     <option value="{{$semuaprodi->id_prodi}}">{{ $semuaprodi->nama_prodi}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('namaprodi'))
                                <div class="text-danger">
                                    {{ $errors->first('namaprodi')}}
                                </div>
                    @endif
                </div>
                <!-- /.card-body -->

               <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection