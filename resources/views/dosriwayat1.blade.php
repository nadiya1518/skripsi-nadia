@extends('layouts.backend')

@section('title','Daftar Riwayat Pendidikan Dosen')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Riwayat Pendidikan Dosen</a></li>
              <li class="breadcrumb-item active">Daftar Riwayat Pendidikan Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Riwayat Pendidikan Dosen</i></h5>
            <tr>
              <td>
                <a href="dosriwayat2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Riwayat Pendidikan Dosen</i></button></a><br>
              </td><br>
            </tr>
            <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                              <th width="20px">No.</th>
                              <th width="300px">Nama Dosen</th>
                              <th width="300px">Perguruan Tinggi</th>
                              <th width="300px">Fakultas</th>
                              <th width="300px">Jurusan</th>
                              <th width="300px">Program Studi</th>
                              <th width="300px">Jenjang Pendidikan</th>
                              <th width="300px">Judul Tugas Akhir</th>
                              <th width="100px">Aksi</th>
                            </tr>
                        </thead>
                       <tbody>
                        <?php $i=0; ?>
                         @foreach($riwayat_pend_formal_dosen as $p)
                        
                        <tr>
                          <td>{{ ++$i }}</td>
                          <td>{{ $p->Dosen->nama }}</td>
                          <td>{{ $p->perguruan_tinggi }}</td>
                          <td>{{ $p->fakultas }}</td>
                          <td>{{ $p->jurusan}}</td>
                          <td>{{ $p->prodi }}</td>
                          <td>{{ $p->JenjangPendidikan->nama_jenjang }}</td>
                          <td>
                          <a href="{{route('riwayat.edit',$p->id_riwayatt)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

                          <a href="{{route('riwayat.delete',$p->id_riwayatt)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                          </td>
                        </tr>
                        @endforeach
                     </tbody>
                    </table>


        </div>
   
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection