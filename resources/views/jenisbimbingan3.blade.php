@extends('layouts.backend')

@section('title','Edit Jenis Bimbingan')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Bimbingan</a></li>
              <li class="breadcrumb-item active">Edit Jenis Bimbingan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Jenis Bimbingan</h3>
          </div>

                <form method="post" action="{{route('jenis_bimbingan.update',$jenis_bimbingan->id_jenis_bimbingan)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Jenis Bimbingan</label>
                    <input type="string" name="jenis_bimbingan" class="form-control" placeholder="Jenis Bimbingan" value=" {{ $jenis_bimbingan->jenis_bimbingan }} ">

                    @if($errors->has('jenisbimbingan'))
                                <div class="text-danger">
                                    {{ $errors->first('jenisbimbingan')}}
                                </div>
                    @endif

                  </div>
                
                <!-- /.card-body -->
                  <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection