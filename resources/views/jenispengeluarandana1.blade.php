@extends('layouts.backend')

@section('title','Jenis Pengeluaran Dana')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Pengeluaran Dana</a></li>
              <li class="breadcrumb-item active">Daftar Jenis Pengeluaran Dana</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Jenis Pengeluaran Dana</i></h5>
            <tr>
              <td>
                <a href="jenispengeluarandana2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Jenis Pengeluaran Dana</i></button></a><br>
              </td><br>
            </tr>
 
            <br/>
            <table class="table table-bordered table-hover table-striped">
                <thead>
                  <tr>
                    <th width="20px">No.</th>
                    <th>Jenis Pengeluaran Dana</th>
                    <th width="100px">Aksi</th>
                  </tr>
                </thead>
              <tbody>
                <?php $i =0; ?>
                @foreach($jenis_pengeluaran as $p)
                <tr>
                  <td>{{ ++$i }} </td>
                  <td>{{ $p->nama_jenis_pengeluaran }}</td>
                  <td>
                    <a href="{{route('jenis_pengeluaran.edit',$p->id_jenis_pengeluaran)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>
                    
                    <a href="{{route('jenis_pengeluaran.delete',$p->id_jenis_pengeluaran)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>

        </div>
   
      </div>

    </section>

  </div>

@endsection