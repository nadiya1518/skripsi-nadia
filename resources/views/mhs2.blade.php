@extends('layouts.backend')

@section('title','Tambah Dokumen')

@section('content')

<?php
$keaktifan=[1=>'aktif',0=>'tidak aktif', ];

$mhsasing = [1=>'ya',0=>'tidak',];
?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Upload Dokumen</a></li>
              <li class="breadcrumb-item active">Tambah Dokumen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Dokumen</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/mhs2/store" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}
              
              <div class="card-body">
                  <div class="form-group">
                    <label>Nama</label>
                    <input type="string" name="nama_mhs" class="form-control" placeholder="Masukkan Nama Mahasiswa">
                     @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('nama')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>NPM</label>
                    <input type="string" name="npm" class="form-control" placeholder="Masukkan NPM Mahasiswa">
                     @if($errors->has('npm'))
                                <div class="text-danger">
                                    {{ $errors->first('npm')}}
                                </div>
                    @endif
                  </div>


                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Program Studi</label>
                    <select name="id_prodi" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($program_studi as $prodi)
                     <option value="{{$prodi->id_prodi}}">{{ $prodi->nama_prodi}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama prodi'))
                                <div class="text-danger">
                                    {{ $errors->first('id_prodi')}}
                                </div>
                    @endif
                </div>

                      <!-- select -->
                  <div class="form-group">
                        <label>Jenis Mahasiswa</label>
                        <select name="jenis_mahasiswa" class="form-control select2" style="width: 100%;">">
                          <option>--Pilih--</option>
                          <option>Reguler</option>
                          <option>Transfer</option>
                        </select>
                         @if($errors->has('jenis_mhs'))
                                <div class="text-danger">
                                    {{ $errors->first('jenis_mhs')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Jenis Penerimaan*</label>
                    <select name="id_jenis_penerimaan" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenispenerimaan as $satuanjalur)
                     <option value="{{$satuanjalur->id_jenis_penerimaan}}">{{ $satuanjalur->jenis_penerimaan}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('jenis_penerimaan'))
                                <div class="text-danger">
                                    {{ $errors->first('id_jenis_penerimaan')}}
                                </div>
                    @endif
                </div>
                  
                  <div class="form-group">
                    <label>Tahun Masuk</label>
                    <input type="string" name="waktu_masuk" class="form-control" placeholder="Tahun Masuk">
                     @if($errors->has('waktu masuk'))
                                <div class="text-danger">
                                    {{ $errors->first('waaktu masuk')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Tahun Transfer</label>
                    <input type="date" name="tahun_trf" class="form-control" placeholder="Tahun Transfer">
                     @if($errors->has('tahun_transfer'))
                                <div class="text-danger">
                                    {{ $errors->first('tahun_transfer')}}
                                </div>
                    @endif
                  </div>

                   <div class="form-group">
                    <label>Tahun Lulus</label>
                    <input type="date" name="waktu_lulus" class="form-control" placeholder="Tahun Lulus">
                     @if($errors->has('tahun_lulus'))
                                <div class="text-danger">
                                    {{ $errors->first('tahun_lulus')}}
                                </div>
                    @endif
                  </div>                  

                  <div class="form-group">
                  <label>Status Keaktifan</label>
                    <select name="status_keaktifan" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($keaktifan as $keaktifan =>$aktif)
                      <option value="{{$keaktifan}}">{{$aktif}}</option>
                      @endforeach
                    </select>
                      @if($errors->has('status_keaktifan'))
                                <div class="text-danger">
                                    {{ $errors->first('$status_keaktifan')}}
                                </div>
                      @endif
                    </select>
                  </div>

                  <div class="form-group">
                  <label>Apakah Mahasiswa Asing</label>
                    <select name="apakah_mhs_asing" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($mhsasing as $mhsasing =>$asing)
                      <option value="{{$mhsasing}}">{{$asing}}</option>
                      @endforeach
                    </select>
                      @if($errors->has('mhs_asing'))
                                <div class="text-danger">
                                    {{ $errors->first('$mhs_asing')}}
                                </div>
                      @endif
                    </select>
                  </div>

                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="mhs1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>

      </div>

    </section>

  </div>

@endsection