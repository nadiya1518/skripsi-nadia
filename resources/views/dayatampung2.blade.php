@extends('layouts.backend')

@section('title','Daya Tampung')

@section('content')

<?php
$Studi = [0=>'tidak',1=>'ya'];
?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Daya Tampung</a></li>
              <li class="breadcrumb-item active">Tambah Data Daya Tampung</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Daya Tampung</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/dayatampung2/store" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}


              <div class="card-body">
                  <div class="form-group">
                    <label>Apakah Pada Program Studi Yang Diakreditasi</label>
                      <select name="apakah_pd_ps" class="form-control select2" style="width: 100%;">
                        <option selected="selected">Pilih</option>
                          @foreach($Studi as $Studi =>$stu)
                        <option value="{{$Studi}}">{{$stu}}</option>
                          @endforeach
                      </select>
                          @if($errors->has('apakah_pd_ps'))
                            <div class="text-danger">
                              {{ $errors->first('$apakah_pd_ps')}}
                            </div>
                          @endif
                      </select>
                  </div>


                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Jenis Penerimaan*</label>
                    <select name="id_jenis_penerimaan" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenispenerimaan as $satuanjalur)
                     <option value="{{$satuanjalur->id_jenis_penerimaan}}">{{ $satuanjalur->jenis_penerimaan}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('jenis_penerimaan'))
                                <div class="text-danger">
                                    {{ $errors->first('id_jenis_penerimaan')}}
                                </div>
                    @endif
                </div>

                  <div class="form-group">
                    <label>Total Daya Tampung</label>
                    <input type="string" name="total_daya_tampung" class="form-control" placeholder="Total Daya Tampung">
                    @if($errors->has('total'))
                                <div class="text-danger">
                                    {{ $errors->first('total')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Pendaftar</label>
                    <input type="string" name="pendaftar" class="form-control" placeholder="Pendaftar">
                    @if($errors->has('pendaftar'))
                                <div class="text-danger">
                                    {{ $errors->first('pendaftar')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Lulus Seleksi</label>
                    <input type="string" name="lulus_seleksi" class="form-control" placeholder="Lulus Seleksi">
                    @if($errors->has('lulus'))
                                <div class="text-danger">
                                    {{ $errors->first('lulus')}}
                                </div>
                    @endif
                  </div>

                </div>
              </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
              </form>
            </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection