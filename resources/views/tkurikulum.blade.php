@extends('layouts.backend')

@section('title','Dosen')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kurikulum</a></li>
              <li class="breadcrumb-item active">Tambah Data Kurikulum</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Kurikulum</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
                <div class="form-group">
                  <label>Nama Matakuliah</label>
                  <input type="string" name="nama_matkul" class="form-control" placeholder="Nama Matakuliah">
                  </div>

                  <div class="form-group">
                    <label>Kode Matakuliah</label>
                    <input type="string" name="kode_matkul" class="form-control" placeholder="Kode Matakuliah">
                  </div>

                   <div class="form-group">
                  <label>Semester</label>
                  <input type="string" name="semester" class="form-control" placeholder="Semester">
                  </div>

                  <div class="form-group">
                    <label>Bobot Kredit(sks)</label>
                    <input type="string" name="kode_matkul" class="form-control" placeholder="Bobot Kredit(sks)">
                  </div>

                   <div class="form-group">
                  <label>Konversi Kredit ke Jam</label>
                  <input type="string" name="nama_matkul" class="form-control" placeholder="Konversi Kredit ke Jam">
                  </div>

                  <div class="form-group">
                    <label>Capaian Pembelajaran</label>
                    <input type="string" name="kode_matkul" class="form-control" placeholder="Capaian Pembelajaran">
                  </div>

                   <div class="form-group">
                  <label>Dokumen Rencana Pembelajaran</label>
                  <input type="string" name="nama_matkul" class="form-control" placeholder="Dokumen Rencana Pembelajaran">
                  </div>

                  <div class="form-group">
                    <label>Unit Penyelenggara</label>
                    <input type="string" name="kode_matkul" class="form-control" placeholder="Unit Penyelenggara">
                  </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
              </form>
            </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection