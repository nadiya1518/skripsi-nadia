@extends('layouts.backend')

@section('title','Edit Jenis PkM')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis PkM</a></li>
              <li class="breadcrumb-item active">Edit Jenis PkM</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Jenis PkM</h3>
          </div>

                <form method="post" action="{{route('jenis_pkm.update',$jenis_pkm->id_jenis_pkm)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Jenis Publikasi*</label>
                    <input type="string" name="nama_jenis_pkm" class="form-control" placeholder="Jenis Publikasi" value=" {{ $jenis_pkm->nama_jenis_pkm }} ">

                    @if($errors->has('jenispkm'))
                                <div class="text-danger">
                                    {{ $errors->first('jenispkm')}}
                                </div>
                    @endif

                  </div>
                
                <!-- /.card-body -->
                  <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection