@extends('layouts.backend')

@section('title','Tambah Daftar Lulusan')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Daftar Lulusan</a></li>
              <li class="breadcrumb-item active">Tambah Daftar Lulusan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Daftar Lulusan</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/kepuasanmhs2/store">
                {{ csrf_field() }}
                
              <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Aspek Kepuasan Mahasiswa</label>
                    <select name="id_jenis_aspek" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenisaspek as $semuaspek)
                     <option value="{{$semuaspek->id_jenis_aspek}}">{{ $semuaspek->jenis_aspek_kepuasan}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('baku_mutu'))
                                <div class="text-danger">
                                    {{ $errors->first('id_baku')}}
                                </div>
                    @endif
                </div>

                <div class="form-group">
                        <label>Tingkat</label>
                        <select name="tingkat" class="form-control select2" style="width: 100%;">">
                          <option>--Pilih--</option>
                          <option>Sangat Baik</option>
                          <option>Baik</option>
                          <option>Cukup</option>
                          <option>Kurang</option>
                        </select>
                        @if($errors->has('tingkat'))
                                <div class="text-danger">
                                    {{ $errors->first('tingkat')}}
                                </div>
                    @endif
                      </div>

                  <div class="form-group">
                    <label>Rencana Tindak Lanjut oleh UPPS/PS</label>
                    <input type="string" name="rencana_tindak_lanjut" class="form-control" placeholder="Masukkan Rencana Tindak Lanjut oleh  UPPS/PS">
                  </div>

                  
                  
                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection