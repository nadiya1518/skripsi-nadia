@extends('layouts.backend')

@section('title','Jenis Kemampuan')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Kemampuan</a></li>
              <li class="breadcrumb-item active">Daftar Jenis Kemampuan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content"> 

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Jenis Kemampuan</i></h5>
            <tr>
              <td>
                <a href="jeniskemampuan2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Jenis Kemampuan</i></button></a><br>
              </td><br>
            </tr>

                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th width="20">No.</th>
                                <th width="900">Jenis Kemampuan</th>
                                <th width="100">Aksi</th>
                            </tr>
                        </thead>
                       <tbody>
                        <?php $i=0; ?>
                         @foreach($jenis_kemampuan as $p)
                        <tr>
                          <td>{{ ++$i }}</td>
                          <td>{{ $p->jenis_kemampuan }}</td>
                          
                          <td>
                          <a href="{{route('jenis_kemampuan.edit',$p->id_jenis_kemampuan)}}"<i class="far fa-edit btn btn-sm btn-success"></i></a>
                          
                          <a href="{{route('jenis_kemampuan.delete',$p->id_jenis_kemampuan)}}" <i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                          </td>
                        </tr>
                        @endforeach
                     </tbody>
                    </table>

        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
  </div>

@endsection