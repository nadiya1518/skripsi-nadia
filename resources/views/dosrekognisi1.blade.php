@extends('layouts.backend')

@section('title','Pengakuan/Rekognisi Dosen')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Pengakuan/Rekognisi Dosen</a></li>
              <li class="breadcrumb-item active">Daftar Data Pengakuan/Rekognisi Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Data Pengakuan/Rekognisi Dosen</i></h5>
            <tr>
              <td>
                <a href="dosrekognisi2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Data Pengakuan/Rekognisi Dosen</i></button></a><br>
              </td><br>
            </tr>
            <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                               <th width="20px">No.</th>
                                <th width="200px">Nama Dosen</th>
                                <th width="200px">Bidang Keahlian</th>
                                <th width="200px">Bukti Pendukung</th>
                                <th width="100px">Tingkat</th>  
                                <th width="100px">Tahun</th>
                                <th width="100px">Aksi</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php $i = 0; ?>
                            @foreach($rekognisi_dosen as $p)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $p->Dosen->nama }}</td>
                                <td>{{ $p->BidangKeahlian->bidang_keahlian_dosen }}</td>
                                <td>{{ $p->bukti_pendukung }}</td>
                                <td>{{ $p->tingkat }}</td>
                                <td>{{ $p->tahun }}</td>
                                <td>
                                   <a href="{{route('rekognisi.edit',$p->id_keahlian)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

                                   <a href="{{route('rekognisi.delete',$p->id_keahlian)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>

                    </table>


        </div>
   
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection