@extends('layouts.backend')

@section('title','Tambah Data EWMP')

@section('content')


  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">EWMP</a></li>
              <li class="breadcrumb-item active">Tambah Data EWMP</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data EWMP</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
                  <form method="post" action="/dosriwayat2/store">
                    {{ csrf_field() }}

                  <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Nama Dosen</label>
                    <select name="id_dosen" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($dosen as $semuadosen)
                      <option value="{{$semuadosen->id_dosen}}">{{ $semuadosen->nama_dosen}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama_dosen'))
                                <div class="text-danger">
                                    {{ $errors->first('id_dosen')}}
                                </div>
                    @endif
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Jenis Dosen</label>
                    <select name="id_jenis_dosen" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenisdosen as $semuajenis)
                      <option value="{{$semuajenis->id_jenis_dosen}}">{{ $semuajenis->jenis_dosen}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('jenis_dosen'))
                                <div class="text-danger">
                                    {{ $errors->first('id_jenis_dosen')}}
                                </div>
                    @endif
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Universitas</label>
                    <input type="string" name="univ" class="form-control" placeholder="Universitas">
                    @if($errors->has('universitas'))
                                <div class="text-danger">
                                    {{ $errors->first('universitas')}}
                                </div>
                    @endif
                  </div>



                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Program Studi</label>
                    <select name="id_prodi" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($prodi as $semuaprodi)
                      <option value="{{$semuaprodi->id_prodi}}">{{ $semuaprodi->nama_prodi}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('prodi'))
                                <div class="text-danger">
                                    {{ $errors->first('id_prodi')}}
                                </div>
                    @endif
                    </select>
                  </div>


                  <div class="form-group">
                    <label>EWMP dalam Pendidikan/Pembelajaran</label>
                    <input type="string" name="pend_ps_luar_pt" class="form-control" placeholder="Pendidikan/Pembelajaran(sks)">
                    @if($errors->has('ps lain luar pt'))
                                <div class="text-danger">
                                    {{ $errors->first('ps lain luar pt')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>EWMP dalam Penelitian (sks) </label>
                    <input type="string" name="penelitian" class="form-control" placeholder="Penelitian (sks)">
                    @if($errors->has('penelitian'))
                                <div class="text-danger">
                                    {{ $errors->first('penelitian')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>EWMP dalam PKM (sks) </label>
                    <input type="string" name="pkm" class="form-control" placeholder="PKM (sks)">
                    @if($errors->has('pkm'))
                                <div class="text-danger">
                                    {{ $errors->first('pkm')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>EWMP dalam Tugas Tambahan/Pendukung</label>
                    <input type="string" name="tgs_tambahan" class="form-control" placeholder="Tugas Tambahan">
                    @if($errors->has('tugas'))
                                <div class="text-danger">
                                    {{ $errors->first('tugas')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Jumlah (sks) </label>
                    <input type="string" name="prodi" class="form-control" placeholder="Jumlah (sks)">
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('nama')}}
                                </div>
                    @endif
                  </div>


                  <div class="form-group">
                    <label>Konversi Kredit ke Jam</label>
                    <input type="string" name="konversi_kredit" class="form-control" placeholder="Konversi Kredit ke Jam">
                    @if($errors->has('konversi'))
                                <div class="text-danger">
                                    {{ $errors->first('konversi')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Rata-Rata per Semester (sks)</label>
                    <input type="string" name="rata_rata_per_smt" class="form-control" placeholder="Rata-Rata per Semester (sks)">
                    @if($errors->has('rata-rata'))
                                <div class="text-danger">
                                    {{ $errors->first('rata-rata')}}
                                </div>
                    @endif
                  </div>
                         
                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection