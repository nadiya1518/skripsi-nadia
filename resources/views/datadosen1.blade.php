@extends('layouts.backend')

@section('title','Dosen')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dosen</a></li>
              <li class="breadcrumb-item active">Daftar Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Data Dosen</i></h5>
            <tr>
              <td>
                <a href="datadosen2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Data Dosen</i></button></a><br>
              </td>
            </tr>
              <br>
              <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                               <th width="20px">No.</th>
                                <th width="200px">Nama Dosen</th>
                                <th width=50px">NIDN</th>
                                <th width="100px">Status</th></th>
                                <th width="200px">Program Studi</th>
                                <th width="80px">Aksi</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            <?php $i = 0; ?>
                            @foreach($dosen as $p)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $p->nama_dosen }}</td>
                                <td>{{ $p->nidn }}</td>
                                <td>{{ $p->JenisDosen->jenis_dosen }}</td>
                                <td>{{ $p->ProgramStudi->nama_prodi }}</td>
                                <td>
                                   <a href="{{route('dosen.edit',$p->id_dosen)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

                                   <a href="{{route('dosen.delete',$p->id_dosen)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>
   

      </div>

    </section>

  </div>

@endsection