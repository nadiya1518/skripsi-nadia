@extends('layouts.backend')

@section('title','Publikasi Karya Ilmiah')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Data Master</a></li>
              <li class="breadcrumb-item active">Publikasi Karya Ilmiah</li>
            </ol>
          </div> 
        </div> 
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content --> 
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Publikasi Karya Ilmiah</i></h5>
            <tr>
              <td>
                <a href="publikasikarya2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Publikasi Karya</i></button></a><br>
              </td>
            </tr>
      </div>
      <body>   

    <table class="table table-bordered table-hover table-striped">
      <thead>
          <tr>
            <th>Judul Artikel</th>
            <th>Nama Jurnal</th>
            <th>Kategori Kegiatan</th>
            <th>Aksi</th>
            </tr>
      </thead>
    <tbody>
       @foreach($publikasi_karya_ilmiah as $b)
         <tr>
         <td>{{ $b->judul_artikel}}</td>
         <td>{{ $b->nama_jurnal }}</td>
         <td>{{ $b->kategori_kegiatan }}</td>
         <td>
         <a href="{{route('publikasi_karya.edit',$b->id_pub_karya_ilmiah)}}"<i class="fas fa-edit btn btn-warning"></i></a>
         <a href="{{route('publikasi_karya.delete',$b->id_pub_karya_ilmiah)}}" <i class="fas fa-trash-alt btn btn-danger"></i></a>
         </td>
         </tr>
         @endforeach
    </tbody>
    </table>
</body>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection