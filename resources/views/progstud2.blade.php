@extends('layouts.backend')

@section('title','Program Studi')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Program Studi</a></li>
              <li class="breadcrumb-item active">Tambah Program Studi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Program Studi</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/progstud2/store" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}

              <div class="card-body">

                  <div class="form-group">
                    <label>Nama Program Studi</label>
                    <input type="string" name="nama_prodi" class="form-control" placeholder="Nama Program Studi">
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('nama')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Status/Peringkat</label>
                    <input type="string" name="status_peringkat" class="form-control" placeholder="Status/Peringkat">
                    @if($errors->has('status'))
                                <div class="text-danger">
                                    {{ $errors->first('status')}}
                                </div>
                    @endif
                  </div>

                   <div class="form-group">
                    <label>No.SK</label>
                    <input type="string" name="nomor_sk" class="form-control" placeholder="No.SK">
                    @if($errors->has('nomor'))
                                <div class="text-danger">
                                    {{ $errors->first('nomor')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Tanggal SK</label>
                    <input type="date" name="tanggal_sk" class="form-control" placeholder="Tanggal SK">
                    @if($errors->has('tanggal'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Tanggal Kadaluarsa</label>
                    <input type="date" name="tanggal_kadaluarsa" class="form-control" placeholder="Tanggal Kadaluarsa">
                    @if($errors->has('tanggal'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal')}}
                                </div>
                    @endif
                  </div>


                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="progstud1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>

      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection