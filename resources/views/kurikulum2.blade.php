@extends('layouts.backend')

@section('title','Tambah Data Kurikulum')

@section('content')

<?php
$status=[0=>'ya', 1=>'tidak'];
?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kurikulum</a></li>
              <li class="breadcrumb-item active">Tambah Data Kurikulum</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Kurikulum</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/kurikulum2/store">
                {{ csrf_field() }}

              <div class="card-body">
              <div class="form-group">
                  <label>Nama Kurikulum</label>
                  <input type="string" name="nama_kurikulum" class="form-control" placeholder="Nama Kurikulum">
                  @if($errors->has('nama_kurikulum'))
                                <div class="text-danger">
                                    {{ $errors->first('nama_kurikulum')}}
                                </div>
                    @endif
              </div>

              <div class="form-group">
                  <label>Tanggal Mulai Berlaku</label>
                  <input type="date" name="tgl_mulai_berlaku" class="form-control" placeholder="Tanggal Mulai Berlaku">
                  @if($errors->has('tanggal'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal')}}
                                </div>
                    @endif
              </div>

              <div class="form-group">
                  <label>Tahun Mulai</label>
                  <input type="string" name="tahun_mulai" class="form-control" placeholder="Tahun Mulai">
                  @if($errors->has('tahun'))
                                <div class="text-danger">
                                    {{ $errors->first('tahun')}}
                                </div>
                    @endif
              </div>

              <div class="form-group">
                  <label>Apakah Kurikulum Sedang Digunakan?</label>
                    <select name="apakah_sdg_digunakan" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($status as $status =>$digunakan)
                      <option value="{{$status}}">{{$digunakan}}</option>
                      @endforeach
                    </select>
                      @if($errors->has('apakah_sdg_digunakan'))
                                <div class="text-danger">
                                    {{ $errors->first('$apakah_sdg_digunakan')}}
                                </div>
                      @endif
                    </select>
                  </div>

                <br>
                <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>
                  <tr>
                    <td>
                      <a href="kurikulum1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                    </td>
                  </tr>
          </div>
        </form>
      </div>
 
    </div>
  </section>
</div>
@endsection