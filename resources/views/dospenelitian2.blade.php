@extends('layouts.backend')

@section('title','Tambah Penelitian Dosen')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Penelitian Dosen</a></li>
              <li class="breadcrumb-item active">Tambah Penelitian Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Penelitian Dosen</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
                  <div class="form-group">
                    <label>Nama Dosen</label>
                    <input type="string" name="jdl_penelitian" class="form-control" placeholder="Masukkan Nama Dosen">
                  </div>
        
                  <div class="form-group">
                    <label>Judul Penelitian</label>
                    <input type="string" name="jdl_penelitian" class="form-control" placeholder="Masukkan Judul Penelitian">
                  </div>

                  <div class="form-group">
                    <label>Jenis Penelitian</label>
                    <input type="string" name="thn_penelitian" class="form-control" placeholder="Masukkan Jenis Penelitian">

                  <div class="form-group">
                    <label>Lokasi Penelitian</label>
                    <input type="string" name="jdl_penelitian" class="form-control" placeholder="Masukkan Lokasi Penelitian">
                  </div>

                  <div class="form-group">
                    <label>Tahun Penelitian</label>
                    <input type="string" name="thn_penelitian" class="form-control" placeholder="Masukkan Tahun Penelitian">

                    <div class="form-group">
                    <label>Abstrak</label>
                    <input type="string" name="jdl_penelitian" class="form-control" placeholder="Masukkan Abstrak">
                  </div>

                  <div class="form-group">
                    <label>Biaya Penelitian</label>
                    <input type="string" name="thn_penelitian" class="form-control" placeholder="Biaya Penelitian">

                  <div class="form-group">
                  <label>Sumber Pembiayaan</label>
                    <select name="sumber_biaya" class="form-control select2" style="width: 100%;">">
                      <option>--Pilih--</option>
                      <option>Perguruan Tinggi</option>
                      <option>Mandiri</option>
                      <option>Lembaga Dalam Negeri (Diluar Perguruan Tinggi)</option>
                      <option>Lembaga Luar Negeri</option>
                    </select>
              </div>  
                  </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
              </form>
            </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection