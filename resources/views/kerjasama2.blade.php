@extends('layouts.backend')

@section('title','Kerjasama')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kerjasama</a></li>
              <li class="breadcrumb-item active">Tambah Daftar Kerjasama</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Daftar Kerjasama</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
               <form method="post" action="/kerjasama2/store">
                {{ csrf_field() }}
              <div class="card-body">
                  <div class="form-group">
                    <label>Lembaga Mitra</label>
                    <input type="string" name="lembaga_mitra" class="form-control" placeholder="Lembaga Mitra">

                    @if($errors->has('lembaga_mitra'))
                                <div class="text-danger">
                                    {{ $errors->first('lembaga_mitra')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                        <label>Tingkat</label>
                        <select name="tingkat" class="form-control select2" style="width: 100%;">">
                          <option>--Pilih--</option>
                          <option>Wilayah/Lokal</option>
                          <option>Nasional</option>
                          <option>Internasional</option>
                        </select>
                        @if($errors->has('tingkat'))
                                <div class="text-danger">
                                    {{ $errors->first('tingkat')}}
                                </div>
                    @endif
                      </div>

                    <div class="form-group">
                      <label>Judul Kegiatan Kerjasama</label>
                      <input type="string" name="judul_kegiatan_kerjasama" class="form-control" placeholder="Judul Kegiatan Kerjasama">
                      @if($errors->has('judul_kegiatan_kerjasama'))
                                <div class="text-danger">
                                    {{ $errors->first('judul_kegiatan_kerjasama')}}
                                </div>
                    @endif
                  </div>

                    <div class="form-group">
                      <label>Manfaat Bagi Program Studi Yang Diakreditasi</label>
                      <input type="string" name="manfaat_kerjasama" class="form-control" placeholder="Manfaat Bagi Program Studi Yang Diakreditasi">
                      @if($errors->has('manfaat_kerjasama'))
                                <div class="text-danger">
                                    {{ $errors->first('manfaat_kerjasama')}}
                                </div>
                    @endif
                  </div>

                    <div class="form-group">
                      <label>Waktu Mulai Kerjasama</label>
                      <input type="string" name="waktu_mulai_kerjasama" class="form-control" placeholder="Waktu Mulai Kerjasama">
                      @if($errors->has('waktu_dan_durasi'))
                                <div class="text-danger">
                                    {{ $errors->first('waktu_dan_kerjasama')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                      <label>Waktu Berakhirnya Kerjasama</label>
                      <input type="string" name="waktu_berakhir_kerjasama" class="form-control" placeholder="Tahun Berakhirnya Kerjasama">
                      @if($errors->has('tahun_berakhirnya_kerjasama'))
                                <div class="text-danger">
                                    {{ $errors->first('tahun_berakhirnya_kerjasama')}}
                                </div>
                    @endif
                  </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
              </form>
            </div>


        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection