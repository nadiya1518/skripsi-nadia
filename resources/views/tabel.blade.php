@extends('layouts.backend')

@section('title','Tabel')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Tabel Borang</a></li>
              <li class="breadcrumb-item active">Borang</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header bg-info">
          <h1 class="card-title bg-info">Borang Perstandar</h1>
        </div>

        <div class="card-body">
        
          <thead>
          <table style="width:80%">
          <tr>
                <th rowspan="2">No.</th>
                <th rowspan="2">Jenis Program</th>
                <th rowspan="2">Nama <br>Program Studi</th>
                <th colspan="3">Akreditasi Program Studi</th>
                <th rowspan="2">Jumlah Mahasiswa saat TS</th>
          </tr>
          <tr>
                <th>Status/Peringkat</th>
                <th>No. dan Tgl SK</th>
                <th>Tgl Kadaluarsa</th>
          </tr>
          </thead>                         
          </table>

            <br>
            <thead>
            <table style="width:80%">
            <tr>
                <th rowspan="2">No.</th>
                <th rowspan="2">Lembaga Mitra</th>
                <th colspan="3">Tingkat</th>
                <th rowspan="2">Judul Kegiatan Kerjasama</th>
                <th rowspan="2">Manfaat Bagi PS yang diakreditasi</th>
                <th rowspan="2">Waktu dan Durasi</th>
                <th rowspan="2">Bukti Kerjasama</th>
                <th rowspan="2">Tahun Berakhirnya Kerjasama (YYYY)</th> 
            </tr>
            <tr>
                <th>Internasional</th>
                <th>Nasional</th>
                <th>Wilayah/Lokal</th>
            </tr>
            <tr>
              <td>1</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            </thead>                         
            </table>

            <br>
            <thead>
            <table style="width:90%">
            <tr>
                <th rowspan="2">Tahun Akademik</th>
                <th rowspan="2">Daya Tampung</th>
                <th colspan="2">Jumlah Calon Mahasiswa</th>
                <th colspan="2">Jumlah Mahasiswa Baru</th>
                <th colspan="2">Jumlah Mahasiswa Aktif</th>
            </tr>
            <tr>
                <th>Pendaftar</th>
                <th>Lulus Seleksi</th>      
                <th>Reguler</th>
                <th>Transfer*</th>
                <th>Reguler</th>
                <th>Transfer*</th> 
            </tr>
            <tr>
              <td>2018-2019</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            </thead>                         
            </table>

            <br>
        <thead>
          <table style="width: 90%">
            <tr>
              <th rowspan="2">No.</th>
              <th rowspan="2">Program Studi</th>
              <th colspan="3">Jumlah Mahasiswa Aktif</th>
              <th colspan="3">Jumlah Mahasiswa Asing Penuh Waktu</th>
              <th colspan="3">Jumlah Mahasiswa Asing Paruh Waktu</th>
            </tr>

            <tr>
              <th rowspan="1">TS-2</th>
              <th rowspan="1">TS-1</th>
              <th rowspan="1">TS</th>
              <th rowspan="1">TS-2</th>
              <th rowspan="1">TS-1</th>
              <th rowspan="1">TS</th>
              <th rowspan="1">TS-2</th>
              <th rowspan="1">TS-1</th>
              <th rowspan="1">TS</th>
            </tr>   
          </table>
        </thead>

            <br>
            <thead>
            <table style="width:80%">
            <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Nama Dosen</th>
                <th rowspan="1">Pendidikan Pasca Sarjana</th>
                <th rowspan="1">Bidang Keahlian</th>
                <th rowspan="1">Kesesuaian dengan Kompetensi Inti PS</th>
                <th rowspan="1">Jabatan Akademik</th>
                <th rowspan="1">Sertifikat Pendidik Profesional</th>
                <th rowspan="1">Sertifikat Kompetensi/ Profesi/ Industri</th>
                <th rowspan="1">Mata Kuliah yang Diampu pada PS yang Diakreditasi</th>
                <th rowspan="1">Kesesuaian Bidang Keahlian dengan Mata Kuliah yang Diampu</th>
                <th rowspan="1">Mata Kuliah yang Diampu pada PS Lain</th>
            </tr>
            </thead>                         
            </table>

          <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="3">No.</th>
                <th rowspan="3">Nama Dosen (DT)</th>
                <th rowspan="3">DTPS</th>
                <th colspan="7">Ekuivalen Waktu Mengajar Penuh (EWMP) pada saat TS dalam satuan kredit semester (sks)</th>
                <th rowspan="3">Jumlah (sks)</th>
                <th rowspan="3">Rata-rata per Semester <br>(sks)</th>

                <tr>
                  <th colspan="3">Pendidikan: Pembelajaran dan Pembimbingan</th>
                  <th rowspan="2">Penelitian</th>
                  <th rowspan="2">PKM</th>
                  <th rowspan="2">Tugas Tambahan dan/atau Penunjang</th>
                </tr>

                <tr>
                  <th rowspan="1">PS yang Diakreditasi</th>
                  <th rowspan="1">PS Lain di dalam PT</th>
                  <th rowspan="1">PS Lain di luar PT</th>
                </tr>         
            </table>
          </thead>

          <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Nama Dosen</th>
                <th rowspan="1">Pendidikan Pasca Sarjana</th>
                <th rowspan="1">Bidang Keahlian</th>
                <th rowspan="1">Jabatan Akademik</th>
                <th rowspan="1">Sertifikat Pendidik Profesional</th>
                <th rowspan="1">Sertifikat Kompetensi/ Profesi/ Industri</th>
                <th rowspan="1">Mata Kuliah yang Diampu pada PS yang Diakreditasi</th>
                <th rowspan="1">Kesesuaian Bidang Keahlian dengan Mata Kuliah yang Diampu</th>
            </tr>       
          </table>
        </thead>

        <br>
        <thead>
          <table style="width: 90%">
            <tr>
              <th rowspan="3">No.</th>
              <th rowspan="3">Nama Dosen</th>
              <th colspan="6">Jumlah Mahasiswa yang Dibimbing</th>
              <th rowspan="3">Rata-rata Jumlah Bimbingan/ Tahun</th>
              <th rowspan="3">Rata-rata Jumlah Bimbingan di Seluruh Program/ Tahun</th>
            </tr>

            <tr>
              <td colspan="3">pada PS yang Diakreditasi</td>
              <td colspan="3">pada PS Lain pada Program yang sama di PT</td>
            </tr>

            <tr>
              <th rowspan="1">TS-2</th>
              <th rowspan="1">TS-1</th>
              <th rowspan="1">TS</th>
              <th rowspan="1">TS-2</th>
              <th rowspan="1">TS-1</th>
              <th rowspan="1">TS</th>
            </tr>   
          </table>
        </thead>

        <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Nama Dosen <br>Industri/ Praktisi</th>
                <th rowspan="1">Pendidikan Tertinggi</th>
                <th rowspan="1">Bidang Keahlian</th>
                <th rowspan="1">NIDK</th>
                <th rowspan="1">Mata Kuliah yang Diampu</th>
                <th rowspan="1">Bobot Kredit <br>(sks)</th>
            </tr>       
          </table>
        </thead>

        <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="2">No.</th>
                <th rowspan="2">Nama Dosen</th>
                <th rowspan="2">Bidang Keahlian</th>
                <th rowspan="2">Rekognisi dan Bukti Pendukung</th>
                <th colspan="3">Tingkat</th>
                <th rowspan="2">Tahun <br>(YYYY)</th>
              </tr>  
              <tr>
                <th>Wilayah</th>
                <th>Nasional</th>
                <th>Internasional</th>
              </tr>

          </table>
        </thead>

        <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Luaran Penelitian dan PkM</th>
                <th rowspan="1">Tahun <br>(YYYY)</th>
                <th rowspan="1">Keterangan</th>
              </tr>
              <tr>
                <td rowspan="1">I</td>
                <td colspan="3">HKI: a) Paten, b) Paten Sederhana</td>
              </tr>
          </table>
        </thead>

        <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Luaran Penelitian dan PkM</th>
                <th rowspan="1">Tahun <br>(YYYY)</th>
                <th rowspan="1">Keterangan</th>
              </tr>  
              <tr>
                <td rowspan="1">II</td>
                <td colspan="3">HKI: a) Hak Cipta, b) Desain Produk Industri,  c) Perlindungan Varietas Tanaman (Sertifikat Perlindungan Varietas Tanaman, Sertifikat Pelepasan Varietas, Sertifikat Pendaftaran Varietas), d) Desain Tata Letak Sirkuit Terpadu, e) dll.)</td>
              </tr>
          </table>
        </thead>

         <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Luaran Penelitian dan PkM</th>
                <th rowspan="1">Tahun <br>(YYYY)</th>
                <th rowspan="1">Keterangan</th>
              </tr>  
              <tr>
                <td rowspan="1">III</td>
                <td colspan="3">Teknologi Tepat Guna, Produk (Produk Terstandarisasi, Produk Tersertifikasi), Karya Seni, Rekayasa Sosial</td>
              </tr>
          </table>
        </thead>

         <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Luaran Penelitian dan PkM</th>
                <th rowspan="1">Tahun <br>(YYYY)</th>
                <th rowspan="1">Keterangan</th>
              </tr>  
              <tr>
                <td rowspan="1">IV</td>
                <td colspan="3">Buku ber-ISBN, Book Chapter</td>
              </tr>
          </table>
        </thead>

        <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Nama Dosen</th>
                <th rowspan="1">Judul Artikel yang Disitasi (Jurnal, Volume, Tahun, Nomor, Halaman) </th>
                <th rowspan="1">Jumlah Sitasi</th>
              </tr>  
          </table>
        </thead>

        <br>
        //3b7
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Nama Dosen</th>
                <th rowspan="1">Nama Produk/Jasa</th>
                <th rowspan="1">Deskripsi Produk/Jasa</th>
                <th rowspan="1">Bukti</th>
              </tr>  
          </table>
        </thead>

        <br>
        <thead>
          <table style="width: 50%">
            <tr>
              <th rowspan="2">No.</th>
              <th rowspan="2">Semester</th>
              <th rowspan="2">Kode <br>Mata Kuliah</th>
              <th colspan="3">Bobot Kredit</th>
              <th rowspan="2">Koversi Kredit ke Jam</th>
              <th colspan="4">Capaian Pembelajaran</th>
              <th rowspan="2">Dokumen Rencana Pembelajaran</th>
              <th rowspan="2">Unit Penyelenggara</th>
            </tr>

            <tr>
              <th rowspan="1">Kuliah/ Responsi/ Tutorial</th>
              <th rowspan="1">Seminar</th>
              <th rowspan="1">Praktikum/ Praktik/ Praktik Lapangan</th>
              <th rowspan="1">Sikap</th>
              <th rowspan="1">Pengetahuan</th>
              <th rowspan="1">Keterampilan Umum</th>
              <th rowspan="1">Keterampilan Khusus</th>
            </tr>   
          </table>
        </thead>

        <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Judul Penelitian/ PkM</th>
                <th rowspan="1">Nama Dosen</th>
                <th rowspan="1">Mata Kuliah</th>
                <th rowspan="1">Bentuk Integersi</th>
                <th rowspan="1">Tahun <br>(YYYY)</th>
              </tr>  
          </table>
        </thead>

        <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Nama Dosen</th>
                <th rowspan="1">Tema Penelitian sesuai Roadmap</th>
                <th rowspan="1">Nama Mahasiswa</th>
                <th rowspan="1">Judul Kegiatan</th>
                <th rowspan="1">Tahun <br>(YYYY)</th>
              </tr>  
          </table>
        </thead>

        <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Nama Dosen</th>
                <th rowspan="1">Tema Penelitian sesuai Roadmap</th>
                <th rowspan="1">Nama Mahasiswa</th>
                <th rowspan="1">Judul Tesis/Disertasi</th>
                <th rowspan="1">Tahun <br>(YYYY)</th>
              </tr>  
          </table>
        </thead>

        <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Nama Dosen</th>
                <th rowspan="1">Tema PkM sesuai Roadmap</th>
                <th rowspan="1">Nama Mahasiswa</th>
                <th rowspan="1">Judul Kegiatan</th>
                <th rowspan="1">Tahun <br>(YYYY)</th>
              </tr>  
          </table>
        </thead>

        <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="2">No.</th>
                <th rowspan="2">Tahun Lulus</th>
                <th rowspan="2">Jumlah Lulusan</th>
                <th colspan="3">Indeks Prestasi Kumulatif</th>
              </tr> 
              <tr>
                <th rowspan="1">Min.</th>
                <th rowspan="1">Rata-rata</th>
                <th rowspan="1">Maks.</th>
              </tr> 
              <tr>
                <td></td>
                <td>TS-2</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
               <td></td> 
               <td>TS-1</td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td>TS</td>
                <td></td>
                <td></td>
                <td></td>
              </tr> 
          </table>
        </thead>



        <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="2">No.</th>
                <th rowspan="2">Nama Kegiatan</th>
                <th rowspan="2">Waktu Perolehan <br>(YYYY)</th>
                <th colspan="3">Tingkat</th>
                <th rowspan="2">Prestasi yang dicapai</th>
              </tr> 
              <tr>
                <th rowspan="1">Lokal/ Wilayah</th>
                <th rowspan="1">Nasional</th>
                <th rowspan="1">Internasional</th>
              </tr> 
          </table>
        </thead>

        <br>
        //untuk prestasi non akademik
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="2">No.</th>
                <th rowspan="2">Nama Kegiatan</th>
                <th rowspan="2">Waktu Perolehan <br>(YYYY)</th>
                <th colspan="3">Tingkat</th>
                <th rowspan="2">Prestasi yang dicapai</th>
              </tr> 
              <tr>
                <th rowspan="1">Lokal/ Wilayah</th>
                <th rowspan="1">Nasional</th>
                <th rowspan="1">Internasional</th>
              </tr> 
          </table>
        </thead>

        <br>
        //studi lulusan diploma
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="2">Tahun Masuk</th>
                <th rowspan="2">Jumlah Mahasiswa Diterima</th>
                <th colspan="5">Jumlah Mahasiswa Lulus pada</th>
                <th rowspan="2">Jumlah Lulusan s.d. akhir TS</th>
                <th rowspan="2">Rata-rata Masa Studi</th>

              </tr> 
              <tr>
                <th rowspan="1">akhir TS-4</th>
                <th rowspan="1">akhir TS-3</th>
                <th rowspan="1">akhir TS-2</th>
                <th rowspan="1">akhir TS-1</th>
                <th rowspan="1">akhir TS</th>  
              </tr> 
              <tr>
                <td></td>
                <td>TS-4</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
               <td></td> 
               <td>TS-4</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td>TS-2</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr> 
          </table>
        </thead>

          <br>
          //studi lulusan sarjana
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="2">Tahun Masuk</th>
                <th rowspan="2">Jumlah Mahasiswa Diterima</th>
                <th colspan="7">Jumlah Mahasiswa Lulus pada</th>
                <th rowspan="2">Jumlah Lulusan s.d. akhir TS</th>
                <th rowspan="2">Rata-rata Masa Studi</th>
              </tr> 
              <tr>
                <th rowspan="1">akhir TS-6</th>
                <th rowspan="1">akhir TS-5</th>
                <th rowspan="1">akhir TS-4</th>
                <th rowspan="1">akhir TS-3</th>
                <th rowspan="1">akhir TS-2</th>
                <th rowspan="1">akhir TS-1</th>
                <th rowspan="1">akhir TS</th>  
              </tr> 
              <tr>
                <td></td>
                <td>TS-6</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
               <td></td> 
               <td>TS-5</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td>TS-4</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td>TS-3</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
          </table>
        </thead>

        <br>
        //studi lulusan magister
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="2">Tahun Masuk</th>
                <th rowspan="2">Jumlah Mahasiswa Diterima</th>
                <th colspan="5">Jumlah Mahasiswa Lulus pada</th>
                <th rowspan="2">Jumlah Lulusan s.d. akhir TS</th>
                <th rowspan="2">Rata-rata Masa Studi</th>

              </tr> 
              <tr>
                <th rowspan="1">akhir TS-4</th>
                <th rowspan="1">akhir TS-3</th>
                <th rowspan="1">akhir TS-2</th>
                <th rowspan="1">akhir TS-1</th>
                <th rowspan="1">akhir TS</th>  
              </tr> 
              <tr>
                <td></td>
                <td>TS-3</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
               <td></td> 
               <td>TS-2</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td>TS-1</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr> 
          </table>
        </thead>


         <br>
         //studi lulusan doktor
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="2">Tahun Masuk</th>
                <th rowspan="2">Jumlah Mahasiswa Diterima</th>
                <th colspan="7">Jumlah Mahasiswa Lulus pada</th>
                <th rowspan="2">Jumlah Lulusan s.d. akhir TS</th>
                <th rowspan="2">Rata-rata Masa Studi</th>
              </tr> 
              <tr>
                <th rowspan="1">akhir TS-6</th>
                <th rowspan="1">akhir TS-5</th>
                <th rowspan="1">akhir TS-4</th>
                <th rowspan="1">akhir TS-3</th>
                <th rowspan="1">akhir TS-2</th>
                <th rowspan="1">akhir TS-1</th>
                <th rowspan="1">akhir TS</th>  
              </tr> 
              <tr>
                <td></td>
                <td>TS-6</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
               <td></td> 
               <td>TS-5</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td>TS-4</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
               <td></td> 
               <td>TS-3</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td></td>
                <td>TS-2</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tr>
          </table>
        </thead>

        <br>
        //untuk waktu tunggu lulusan d3
        <thead>
          <table style="width: 90%">
            <tr>
              <th rowspan="2">Tahun Lulus</th>
              <th rowspan="2">Jumlah Lulusan</th>
              <th rowspan="2">Jumlah Lulusan yang Terlacak</th>
              <th rowspan="2">Jumlah Lulusan yang Dipesan Sebelum Lulus</th>
              <th colspan="3">Jumlah lulusan dengan waktu tunggu mendapatkan pekerjaan </th>
            </tr> 
            <tr>
              <th rowspan="1">WT < 3 bulan</th>
              <th rowspan="1">3 ≤ WT ≤ 6 bulan</th>
              <th rowspan="1">WT > 6 bulan</th>
            </tr> 
          </table>
        </thead>

        <br>
        //untuk waktu tunggu lulusan sarjana
        <thead>
          <table style="width: 90%">
            <tr>
              <th rowspan="2">Tahun Lulus</th>
              <th rowspan="2">Jumlah Lulusan</th>
              <th rowspan="2">Jumlah Lulusan yang Terlacak</th>
              <th colspan="3">Jumlah lulusan dengan waktu tunggu mendapatkan pekerjaan </th>
            </tr> 
            <tr>
              <th rowspan="1">WT < 6 bulan</th>
              <th rowspan="1">6 ≤ WT ≤ 18 bulan</th>
              <th rowspan="1">WT > 18 bulan</th>
            </tr> 
          </table>
        </thead>

        <br>
        //untuk waktu tunggu lulusan sarjana terapan
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="2">Tahun Lulus</th>
                <th rowspan="2">Jumlah Lulusan</th>
                <th rowspan="2">Jumlah Lulusan yang Terlacak</th>
                <th colspan="3">Jumlah lulusan dengan waktu tunggu mendapatkan pekerjaan </th>
              </tr> 
              <tr>
              <th rowspan="1">WT < 3 bulan</th>
              <th rowspan="1">3 ≤ WT ≤ 6 bulan</th>
              <th rowspan="1">WT > 6 bulan</th>
            </tr> 
          </table>
        </thead>

        <br>
        //kesesuaian bidang kerja
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="2">Tahun Lulus</th>
                <th rowspan="2">Jumlah Lulusan</th>
                <th rowspan="2">Jumlah Lulusan yang Terlacak</th>
                <th colspan="3">Jumlah lulusan dengan tingkat keseuaian bidang kerja</th>
              </tr> 
              <tr>
              <th rowspan="1">Rendah</th>
              <th rowspan="1">Sedang</th>
              <th rowspan="1">Tinggi</th>
            </tr> 
          </table>
        </thead>

        <br>
        //untuk waktu tunggu lulusan sarjana
        <thead>
          <table style="width: 90%">
            <tr>
              <th rowspan="2">Tahun Lulus</th>
              <th rowspan="2">Jumlah Lulusan</th>
              <th rowspan="2">Jumlah Lulusan yang Terlacak</th>
              <th rowspan="2">Jumlah Lulusan yang Telah Bekerja/Berwirausaha</th>
              <th colspan="3">Jumlah Lulusan yang Bekerja Berdasarkan Tingkat/Ukuran Tempat Kerja/Berwirausaha</th>
            </tr> 
            <tr>
              <th rowspan="1">Lokal/Wilayah/Berwirausaha tidak Berbadan Hukum</th>
              <th rowspan="1">Nasional/Berwirausaha Berbadan Hukum</th>
              <th rowspan="1">Multinasional/ Internasional</th>
            </tr> 
          </table>
        </thead>

        <br>
        <thead>
          <table style="width: 90%">
            <tr>
              <th rowspan="1">Tahun Lulus</th>
              <th rowspan="1">Jumlah Lulusan</th>
              <th rowspan="1">Jumlah Tanggapan Kepuasan Pengguna yang Terlacak</th>
            </tr>
          </table>
        </thead>

        <br>
        //Kepuasan Pengguna Lulusan
        <thead>
          <table style="width: 90%">
            <tr>
              <th rowspan="2">No.</th>
              <th rowspan="2">Jenis Kemampuan</th>
              <th colspan="4">Tingkat Kepuasan Pengguna <br>(%)</th>
              <th rowspan="2">Rencana Tidak Lanjur UPPS/PS</th>
            </tr>
            <tr>
              <th rowspan="1">Sangat Baik</th>
              <th rowspan="1">Baik</th>
              <th rowspan="1">Cukup</th>
              <th rowspan="1">Kurang</th>
            </tr>
            <tr>
              <td>1</td>
              <td>Etika</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td> 
            </tr>
            <tr>
              <td>2</td>
              <td>Keahlian pada bidang ilmu (kompetensi utama)</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td> 
            </tr>
            <tr>
              <td>3</td>
              <td>Kemampuan berbahasa asing</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td> 
            </tr>
            <tr>
              <td>4</td>
              <td>Penggunaan teknologi informasi</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td> 
            </tr>
            <tr>
              <td>5</td>
              <td>Kemampuan berkomunikasi</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td> 
            </tr>
            <tr>
              <td>6</td>
              <td>Kerjasama</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td> 
            </tr>
            <tr>
              <td>7</td>
              <td>Pengembangan Diri</td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td> 
            </tr>
          </table>
        </thead>

        <br>
        <thead>
          <table style="width: 90%">
            <tr>
              <th rowspan="1">No.</th>
              <th rowspan="1">Nama Mahasiswa</th>
              <th rowspan="1">Judul Artikel yang Disitasi <br>(Jurnal, Volume, Tahun, Nomor, Halaman)</th>
              <th rowspan="1">Jumlah Sitasi</th>
            </tr>        
          </table>
        </thead>

        <br>
        //tabel sama kaya 3b7
        <thead>
          <table style="width: 90%">
            <tr>
              <th rowspan="1">No.</th>
              <th rowspan="1">Nama Dosen</th>
              <th rowspan="1">Nama Produk/ Jasa</th>
              <th rowspan="1">Bukti</th>
            </tr>        
          </table>
        </thead>

        <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Luaran Penelitian dan PkM</th>
                <th rowspan="1">Tahun <br>(YYYY)</th>
                <th rowspan="1">Keterangan</th>
              </tr>  
              <tr>
                <td rowspan="1">I</td>
                <td colspan="3">HKI: a) Paten, b) Paten Sederhana</td>
              </tr>
          </table>
        </thead>

        <br>
        //Luaran Penelitian/PkM yang Dihasilkan oleh Mahasiswa BEDANYA SAMA 3B5-2 oleh DTPS
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Luaran Penelitian dan PkM</th>
                <th rowspan="1">Tahun <br>(YYYY)</th>
                <th rowspan="1">Keterangan</th>
              </tr>  
              <tr>
                <td rowspan="1">II</td>
                <td colspan="3">HKI: a) Hak Cipta, b) Desain Produk Industri,  c) Perlindungan Varietas Tanaman (Sertifikat Perlindungan Varietas Tanaman, Sertifikat Pelepasan Varietas, Sertifikat Pendaftaran Varietas), d) Desain Tata Letak Sirkuit Terpadu, e) dll.)</td>
              </tr>
          </table>
        </thead>

         <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Luaran Penelitian dan PkM</th>
                <th rowspan="1">Tahun <br>(YYYY)</th>
                <th rowspan="1">Keterangan</th>
              </tr>  
              <tr>
                <td rowspan="1">III</td>
                <td colspan="3">Teknologi Tepat Guna, Produk (Produk Terstandarisasi, Produk Tersertifikasi), Karya Seni, Rekayasa Sosial</td>
              </tr>
          </table>
          </thead>

         <br>
          <thead>
            <table style="width: 90%">
              <tr>
                <th rowspan="1">No.</th>
                <th rowspan="1">Luaran Penelitian dan PkM</th>
                <th rowspan="1">Tahun <br>(YYYY)</th>
                <th rowspan="1">Keterangan</th>
              </tr>  
              <tr>
                <td rowspan="1">IV</td>
                <td colspan="3">Buku ber-ISBN, Book Chapter</td>
              </tr>
          </table>
          </thead>




        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  @endsection