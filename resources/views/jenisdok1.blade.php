@extends('layouts.backend')

@section('title','Jenis Dokumen')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Jenis Dokumen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Jenis Dokumen</i></h5>
            <tr>
              <td>
                <a href="jenisdok2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Jenis Dokumen</i></button></a><br>
              </td>
            </tr>
      </div>

<body>    

    <table class="table table-bordered table-hover table-striped">
      <thead>
          <tr>
            <th width="20">No.</th>
            <th>Jenis Dokumen</th>
            <th width="100">Aksi</th>
            </tr>
      </thead>
    <tbody>
      <?php $i = 0; ?>
       @foreach($jenis_dokumen as $b)
         <tr>
         <td>{{ ++$i }}</td>
         <td>{{ $b->jenis_dokumen }}</td>
         <td>
            <a href="{{route('jenis_dokumen.edit',$b->id_jenis)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

            <a href="{{route('jenis_dokumen.delete',$b->id_jenis)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
         </td>
         </tr>
         @endforeach
    </tbody>
    </table>
</body>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection