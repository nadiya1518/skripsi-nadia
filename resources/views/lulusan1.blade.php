@extends('layouts.backend')

@section('title','Lulusan')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Lulusan</a></li>
              <li class="breadcrumb-item active">Daftar Lulusan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Lulusan</i></h5>
            <tr>
              <td>
                <a href="lulusan2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Daftar Lulusan</i></button></a><br>
              </td><br>
            </tr>

            <br>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                               <th width="20px">No.</th>
                                <th width="500px">Nama Lulusan</th>
                                <th width="200">IPK</th>
                                <th width="400px">Nama Instansi</th>
                                <th width="400px">Tanggal Bekerja</th>
                                <th width="400px">Tanggal Selesai Bekerja</th>
                                <th width="500px">Alamat Instansi</th>
                                <th width="300px">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; ?>
                            @foreach($lulusan as $p)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $p->Mahasiswa->nama_mhs }}</td>
                                <td>{{ $p->ipk }}</td>
                                <td>{{ $p->nama_instansi }}</td>
                                <td>{{ $p->tanggal_bekerja }}</td>
                                <td>{{ $p->tanggal_selesai_bekerja }}</td>
                                <td>{{ $p->alamat }}</td>
                                <td>
                                   <a href="{{route('lulusan.edit',$p->id_lulusan)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

                                   <a href="{{route('lulusan.delete',$p->id_lulusan)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


        </div>
   
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection