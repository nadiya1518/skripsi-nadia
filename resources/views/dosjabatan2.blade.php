@extends('layouts.backend')

@section('title','Tambah Data Jabatan Fungsional Dosen')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jabatan Fungsional Dosen</a></li>
              <li class="breadcrumb-item active">Tambah Data Jabatan Fungsional Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Jabatan Fungsional Dosen</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/dosjabatan2/store" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}

              <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Nama Dosen</label>
                    <select name="id_dosen" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($dosen as $semuadosen)
                     <option value="{{$semuadosen->id_dosen}}">{{ $semuadosen->nama_dosen}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('id_dosen')}}
                                </div>
                    @endif
                </div>

                  <div class="form-group">
                    <label>Jabatan Fungsional</label>
                    <input type="string" name="jabatan_fungsional" class="form-control" placeholder="Jabatan Fungsional">
                @if($errors->has('jabatan'))
                   <div class="text-danger">
                      {{ $errors->first('jabatan fungsional')}}
                   </div>
                      @endif
                </div>


                  <div class="form-group">
                    <label for="exampleInputFile">SK</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input name="sk" type="file" class="custom-file-input" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Pilih file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label>Tanggal Mulai</label>
                    <input type="string" name="tanggal_mulai" class="form-control" placeholder="Tanggal Mulai">
                    @if($errors->has('tanggal mulai'))
                   <div class="text-danger">
                      {{ $errors->first('tanggal mulai')}}
                   </div>
                      @endif
                  </div>

                  <div class="form-group">
                    <label>Tanggal Berakhir</label>
                    <input type="string" name="tanggal_berakhir" class="form-control" placeholder="Tanggal Berakhir">
                    @if($errors->has('tanggal berakhir'))
                   <div class="text-danger">
                      {{ $errors->first('tanggal berakhir')}}
                   </div>
                      @endif
                </div>
                 

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
              </form>
            </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection