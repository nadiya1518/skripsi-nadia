@extends('layouts.backend')

@section('title','Tambah Bidang Keahlian Dosen')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Bidang Keahlian Dosen</a></li>
              <li class="breadcrumb-item active">Tambah Daftar Bidang Keahlian Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Daftar Bidang Keahlian Dosen</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
               <form action="/dosbidang2/store" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}

              <div class="card-body">
                  <div class="form-group">
                    <label>Nama Bidang Keahlian</label>
                    <input type="string" name="nama_jenis_bidang" class="form-control" placeholder="Nama Bidang Keahlian">
                    @if($errors->has('bidang_keahlian_dosen'))
                                <div class="text-danger">
                                    {{ $errors->first('bidang_keahlian_dosen')}}
                                </div>
                    @endif
                  </div>

                <!-- /.card-body -->

                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="dosbidang1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>


      </div>
    </section>
  </div>
@endsection