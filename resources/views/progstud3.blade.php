@extends('layouts.backend')

@section('title','Edit Data Program Studi')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Program Studi</a></li>
              <li class="breadcrumb-item active">Edit Data Program Studi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Data Program Studi</h3>
          </div>

                <form method="post" action="{{route('prodi.update',$program_studi->id_prodi)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Nama Program Studi*</label>
                    <input type="string" name="nama_prodi" class="form-control" placeholder="Nama Program Studi" value=" {{ $program_studi->nama_prodi }} ">

                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('nama')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Status/Peringkat</label>
                    <input type="string" name="status_peringkat" class="form-control" placeholder="Status/Peringkat" value=" {{ $program_studi->status_peringkat }} ">

                    @if($errors->has('status'))
                                <div class="text-danger">
                                    {{ $errors->first('status')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>No. SK</label>
                    <input type="string" name="nomor_sk" class="form-control" placeholder="No. SK" value=" {{ $program_studi->nomor_sk }} ">

                    @if($errors->has('nomor'))
                                <div class="text-danger">
                                    {{ $errors->first('nomor')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Tanggal SK</label>
                    <input type="string" name="tanggal_sk" class="form-control" placeholder="Tanggal SK" value=" {{ $program_studi->tanggal_sk }} ">

                    @if($errors->has('tanggal'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Tanggal Kadaluarsa</label>
                    <input type="string" name="tanggal_kadaluarsa" class="form-control" placeholder="Tanggal Kadaluarsa" value=" {{ $program_studi->tanggal_kadaluarsa}} ">

                    @if($errors->has('tanggal'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal')}}
                                </div>
                    @endif

                  </div>
                
                <!-- /.card-body -->
                  <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </body>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
@endsection