@extends('layouts.backend')

@section('title','Tambah Tahun')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Tambah Tahun Masuk</a></li>
              <li class="breadcrumb-item active">Tambah Jenis Penulis</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Tahun</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
                  <div class="form-group">
                    <label>Tanggal dan Tahun Masuk</label>
                    <input type="string" name="waktu_masuk" class="form-control" placeholder="Tanggal dan Tahun Masuk">
                  </div>
                  <div class="form-group">
                    <label>Tanggal dan Tahun Lulus</label>
                    <input type="string" name="waktu_lulus" class="form-control" placeholder="Tanggal dan Tahun Lulus">
                  </div>
                </div>
                </div>


                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection