@extends('layouts.backend')

@section('title','Kepuasan Mahasiswa')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kepuasan Mahasiswa</a></li>
              <li class="breadcrumb-item active">Daftar Data Kepuasan Mahasiswa</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Data Kepuasan Mahasiswa</i></h5>
            <tr>
              <td>
                <a href="kepuasanmhs2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Daftar Data Kepuasan Mahasiswa</i></button></a><br>
              </td><br>
            </tr>

            <br>
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                               <th width="20px">No.</th>
                                <th width="700px">Aspek yang Di ukur</th>
                                <th width="200">Tingkat</th>
                                <th width="300px">Rencana Tindak Lanjut oleh UPPS/PS</th>
                                <th width="200px">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; ?>
                            @foreach($kepuasan_mahasiswa as $p)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $p->JenisAspek->jenis_aspek_kepuasan }}</td>
                                <td>{{ $p->tingkat }}</td>
                                <td>{{ $p->rencana_tindak_lanjut }}</td>
                                <td>
                                   <a href="{{route('kepuasanmhs.edit',$p->id_kepuasan_mhs)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

                                   <a href="{{route('kepuasanmhs.delete',$p->id_kepuasan_mhs)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


        </div>
   
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection