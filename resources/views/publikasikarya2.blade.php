@extends('layouts.backend')

@section('title','Publikasi Karya Ilmiah')

@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Standar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">

    <div class="card card-info">
              <div class="card-header">
                <h3 class="card-title">Form Tambah Publikasi Karya</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/publikasikarya2/store">
              {{ csrf_field() }}

              <form class="form-horizontal">
                <div class="card-body">
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Kategori Kegiatan</label>
                    <div class="col-10">
                     <select name="id_kategori" class="form-control select2" id="" data-dependent="nama_kategori">
                      @foreach ($kategori_kegiatan as $kategori)
                        <option value="">Pilih Kategori</option>
                        <option value="{{$kategori->id_kategori}}">{{$kategori->nama_kategori}}</option>
                      @endforeach
                    </select>
                 </div>

                 <div class="form-group row">
                    <label class="col-sm-2 control-label">Jenis Publikasi</label>
                    <div class="col-10">
                     <select name="id_jenis_publikasi" class="form-control select2" id="nama_kategori" data-dependent="jenis_publikasi">
                        <option value="">Pilih Jenis Publikasi</option>
                    </select>
                 </div>



                <div class="form-group row">
                    <label class="col-sm-2 control-label">Kategori Capaian</label>
                    <div class="col-10">
                     <select name="id_kat_capaian" class="form-control select2">
                      <option selected="selected">--Pilih--</option>
                      @foreach($kategori_capaian as $katcapaian)
                      <option value= "{{ $katcapaian->id_kat_capaian}}"> {{$katcapaian->kategori_capaian}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('kategori_capaian'))
                                <div class="text-danger">
                                    {{ $errors->first('id_kat_capaian')}}
                                </div>
                    @endif
                  </div>
                </div>

                <div class="form-group row">
                    <label class="col-sm-2 control-label">Jenis Kegiatan</label>
                    <div class="col-sm-10">
                      <input type="string" name="jenis_kegiatan" class="form-control" placeholder="Jenis">
                      @if($errors->has('jenis kegiatan'))
                                <div class="text-danger">
                                    {{ $errors->first('jenis kegiatan')}}
                                </div>
                    @endif
                    </div>
                  </div>

                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Judul Artikel</label>
                    <div class="col-sm-10">
                      <input type="string" name="judul" class="form-control" placeholder="Judul Artikel">
                      @if($errors->has('judul artikel'))
                                <div class="text-danger">
                                    {{ $errors->first('judul artikel')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Nama Jurnal</label>
                    <div class="col-sm-10">
                      <input type="string" name="nama_jurnal" class="form-control" placeholder="Judul Artikel">
                      @if($errors->has('nama jurnal'))
                                <div class="text-danger">
                                    {{ $errors->first('nama jurnal')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Tautan Laman Jurnal</label>
                    <div class="col-sm-10">
                      <input type="string" name="url" class="form-control" placeholder="Tautan Laman Jurnal">
                      @if($errors->has('tautan'))
                                <div class="text-danger">
                                    {{ $errors->first('tautan')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Tanggal Terbit</label>
                    <div class="col-sm-10">
                      <input type="date" name="tanggal_terbit" class="form-control" placeholder="Tanggal Terbit">
                      @if($errors->has('tanggal terbit'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal terbit')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Lokasi Kegiatan</label>
                    <div class="col-sm-10">
                      <input type="string" name="lokasi" class="form-control" placeholder="Lokasi Kegiatan">
                      @if($errors->has('lokasi kegiatan'))
                                <div class="text-danger">
                                    {{ $errors->first('lokasi kegiatan')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Biaya</label>
                    <div class="col-sm-10">
                      <input type="string" name="biaya" class="form-control" placeholder="Biaya">
                      @if($errors->has('biaya'))
                                <div class="text-danger">
                                    {{ $errors->first('biaya')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Volume</label>
                    <div class="col-sm-10">
                      <input type="string" name="vol" class="form-control" placeholder="Volume">
                      @if($errors->has('vol'))
                                <div class="text-danger">
                                    {{ $errors->first('vol')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Nomor</label>
                    <div class="col-sm-10">
                      <input type="string" name="nomor" class="form-control" placeholder="Nomor">
                      @if($errors->has('nomor'))
                                <div class="text-danger">
                                    {{ $errors->first('nomor')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Halaman</label>
                    <div class="col-sm-10">
                      <input type="string" name="halaman" class="form-control" placeholder="Halaman">
                      @if($errors->has('halaman'))
                                <div class="text-danger">
                                    {{ $errors->first('halaman')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Penerbit</label>
                    <div class="col-sm-10">
                      <input type="string" name="penerbit" class="form-control" placeholder="Penerbit/Penyelenggara">
                      @if($errors->has('penerbit'))
                                <div class="text-danger">
                                    {{ $errors->first('penerbit')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">DOI</label>
                    <div class="col-sm-10">
                      <input type="string" name="doi" class="form-control" placeholder="DOI">
                      @if($errors->has('doi'))
                                <div class="text-danger">
                                    {{ $errors->first('doi')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">ISSN</label>
                    <div class="col-sm-10">
                      <input type="string" name="issn" class="form-control" placeholder="ISSN">
                      @if($errors->has('issn'))
                                <div class="text-danger">
                                    {{ $errors->first('issn')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Tahun</label>
                    <div class="col-sm-10">
                      <input type="date" name="tahun" class="form-control" placeholder="Tahun">
                      @if($errors->has('tahun'))
                                <div class="text-danger">
                                    {{ $errors->first('tahun')}}
                                </div>
                    @endif
                    </div>
                  </div>

              <br>

                <div class="card-header">
                  <h3 class="card-title"><b>Dokumen Publikasi</h3>
                </div>
                <br>

                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 control-label">File</label>
                    <div class="col-sm-10">
                      <input type="file" name="file" class="form-control"  placeholder="File">
                      @if($errors->has('file'))
                                <div class="text-danger">
                                    {{ $errors->first('file')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Nama Dokumen</label>
                    <div class="col-sm-10">
                      <input type="string" name="nama_dok" class="form-control" placeholder="Nama Dokumen">
                      @if($errors->has('nama dokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('nama dokumen')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Keterangan</label>
                    <div class="col-sm-10">
                      <input type="string" name="keterangan" class="form-control" placeholder="Keterangan">
                      @if($errors->has('keterangan'))
                                <div class="text-danger">
                                    {{ $errors->first('keterangan')}}
                                </div>
                    @endif
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Jenis Dokumen</label>
                    <div class="col-10">
                     <select name="id_jenis" class="form-control select2">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenis_dokumen as $jenisdok)
                      <option value= "{{ $jenisdok->id_jenis}}"> {{$jenisdok->jenis_dokumen}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('jenis_dokumen'))
                                <div class="text-danger">
                                    {{ $errors->first('id_dokumen')}}
                                </div>
                    @endif
                  </div>
                </div>

                  <div class="form-group row">
                    <label class="col-sm-2 control-label">Tautan Dokumen</label>
                    <div class="col-sm-10">
                      <input type="string" name="link_url" class="form-control" placeholder="Tautan Dokumen">
                      @if($errors->has('tautan'))
                                <div class="text-danger">
                                    {{ $errors->first('tautan')}}
                                </div>
                    @endif
                    </div>
                  </div>

                  <br>

                <div class="card-header">
                    <h3 class="card-title"><b>Anggota Publikasi</h3>
                </div>
                <br>

                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-1 control-label">Penulis 1</label>
                    <div class="col-sm-3">
                      <input type="string" name="penulis1" class="form-control"  placeholder="Nama Anggota Publikasi">
                      @if($errors->has('penulis'))
                                <div class="text-danger">
                                    {{ $errors->first('penulis')}}
                                </div>
                    @endif
                    </div>
                    <label class="col-sm-2 control-label">Jenis Anggota 1</label>
                    <div class="col-2">
                     <select name="id_penulis" class="form-control select2">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenis_penulis as $jenispen)
                      <option value= "{{ $jenispen->id_penulis}}"> {{$jenispen->nama_jenis_penulis}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('jenis_penulis'))
                                <div class="text-danger">
                                    {{ $errors->first('id_penulis')}}
                                </div>
                    @endif
                  </div>
                    <label class="col-sm-2 control-label">Peran Anggota 1</label>
                    <div class="col-sm-2">
                      <input type="string" name="peran" class="form-control" placeholder="Urutan">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-1 control-label">Penulis 2</label>
                    <div class="col-sm-3">
                      <input type="string" name="penulis2" class="form-control"  placeholder="Nama Anggota Publikasi">
                      @if($errors->has('penulis 2'))
                                <div class="text-danger">
                                    {{ $errors->first('penulis 2')}}
                                </div>
                    @endif
                    </div>
                    <label class="col-sm-2 control-label">Jenis Anggota 2</label>
                    <div class="col-2">
                     <select name="id_penulis" class="form-control select2">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenis_penulis as $jenispen)
                      <option value= "{{ $jenispen->id_penulis}}"> {{$jenispen->nama_jenis_penulis}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('jenis_penulis'))
                                <div class="text-danger">
                                    {{ $errors->first('id_penulis')}}
                                </div>
                    @endif
                  </div>
                    <label class="col-sm-2 control-label">Peran Anggota 2</label>
                    <div class="col-sm-2">
                      <input type="string" name="peran" class="form-control" placeholder="Urutan">
                      @if($errors->has('sumber_pembiayaan'))
                                <div class="text-danger">
                                    {{ $errors->first('sumber_pembiayaan')}}
                                </div>
                    @endif
                    </div>
                </div>
                  
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-1 control-label">Penulis 3</label>
                    <div class="col-sm-3">
                      <input type="string" name="penulis3" class="form-control"  placeholder="Nama Anggota Publikasi">
                      @if($errors->has('penulis 3'))
                                <div class="text-danger">
                                    {{ $errors->first('penulis 3')}}
                                </div>
                    @endif
                    </div>
                    <label class="col-sm-2 control-label">Jenis Anggota 3</label>
                    <div class="col-2">
                     <select name="id_penulis" class="form-control select2">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenis_penulis as $jenispen)
                      <option value= "{{ $jenispen->id_penulis}}"> {{$jenispen->nama_jenis_penulis}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('jenis_penulis'))
                                <div class="text-danger">
                                    {{ $errors->first('id_penulis')}}
                                </div>
                    @endif
                  </div>

                  <label class="col-sm-2 control-label">Peran Anggota 2</label>
                    <div class="col-sm-2">
                      <input type="string" name="peran" class="form-control" placeholder="Urutan">
                    </div>
                  </div>
                  
                  <div class="form-group row">
                    <label class="col-sm-1 control-label">Afiliansi</label>
                    <div class="col-sm-11">
                      <input type="string" class="form-control" placeholder="Afiliansi">
                      @if($errors->has('afiliansi'))
                                <div class="text-danger">
                                    {{ $errors->first('afiliansi')}}
                                </div>
                    @endif
                    </div>
                  </div>

                  <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>
                 
      </div>
    </section>
  </div>
</div>
</div>
@endsection

@push('script')
  <script>
    $(document).ready(function () {
      $('#tabel').DataTable();
      $('.dinamis').change(function () {
          if ($(this).val() !='') {
            var pilih = $(this).attr('id_kategori')
            var nilai = $(this).val()
            var depend = $(this).data('dependent')
            var token = $("input[name ='_token']").val();
            $.ajax({
              url :"{{route ('jabar')}}",
              method : 'POST',
              data : { pilih: pilih, nilai: nilai, depend: depend, _token: token},
              succes: function (res) {
                $('#'+depend).html(res)
              }
            })

          }
      })
    })
  </script>
@endpush