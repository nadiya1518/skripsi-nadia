@extends('layouts.backend')

@section('title','Prestasi Mahasiswa')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Prestasi Mahasiswa</a></li>
              <li class="breadcrumb-item active">Daftar Prestasi Mahasiswa</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Prestasi Mahasiswa</i></h5>
            <tr>
              <td>
                <a href="mhsprestasi2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Daftar Prestasi Mahasiswa</i></button></a><br>
              </td><br>
            </tr>

             <table class="table table-bordered table-hover table-striped">
          <thead>
            <tr>
              <th width="20px">No.</th>
              <th width="400px">Nama Mahasiswa</th>
              <th width="400px">Nama Kegiatan</th>
              <th width="200px">Tingkat</th>
              <th width="100">Waktu Perolehan</th>
              <th width="200px">Jenis Prestasi</th>
              <th width="200">Prestasi Yang Dicapai</th>
              <th width="200px">Aksi</th>
            </tr>
          </thead>
        <tbody>
       <?php $i = 0; ?>
       @foreach($prestasi_mahasiswa as $b)
         <tr>
          <td>{{ ++$i }}</td>
         <td>{{ $b->nama_mhs }} </td>
         <td>{{ $b->nama_kegiatan }}</td>
         <td>{{ $b->tingkat}}</td>
         <td>{{ $b->waktu_perolehan}}</td>

         @if($b->jenis_prestasi ==1)
         <td>Akademik</td>
         @elseif ($b->jenis_prestasi ==0)
         <td>Non-Akademik</td>
         @endif()
         </td>

         <td>{{ $b->prestasi_yang_dicapai}}</td>
         <td>
           <a href="{{route('prestasi.edit',$b->id_prestasi_mhs)}}"<i class="far fa-edit btn btn-sm btn-success"></i></a>
           <a href="{{route('prestasi.delete',$b->id_prestasi_mhs)}}"<i class="far fa-trash-alt btn btn-sm btn-danger"></i>
         </td>
         @endforeach
      </tbody>
    </table>
  </body>
</div>
   
        
      </div>
    </section>
  </div>

@endsection