@extends('layouts.backend')

@section('title','Edit Data Kurikulum')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kurikulum</a></li>
              <li class="breadcrumb-item active">Edit Data Kurikulum</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Data Kurikulum</h3>
          </div>

                <form method="post" action="{{route('kurikulum.update',$kurikulum->id_kurikulum)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">
                  <div class="form-group">
                    <label>Nama Kurikulum</label>
                    <input type="string" name="nama_kurikulum" class="form-control" placeholder="Nama Kurikulum" value=" {{ $kurikulum->nama_kurikulum }} ">

                    @if($errors->has('sumberpembiayaan'))
                                <div class="text-danger">
                                    {{ $errors->first('sumberpembiayaan')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Tanggal Mulai Berlaku</label>
                    <input type="date" name="tgl_mulai_berlaku" class="form-control" placeholder="Tanggal Mulai Berlaku" value=" {{ $kurikulum->tgl_mulai_berlaku}} ">

                    @if($errors->has('tanggal'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Tahun Mulai</label>
                    <input type="string" name="tahun_mulai" class="form-control" placeholder="Tahun Mulai" value=" {{ $kurikulum->tahun_mulai}} ">

                    @if($errors->has('tahun'))
                                <div class="text-danger">
                                    {{ $errors->first('tahun')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Apakah Kurikulum Sedang Digunakan?</label>
                    <input type="date" name="apakah_sdg_digunakan" class="form-control" placeholder="Apakah Kurikulum Sedang Digunakan" value=" {{ $kurikulum->apakah_sdg_digunakan}} ">

                    @if($errors->has('apakah_sdg_digunakan'))
                                <div class="text-danger">
                                    {{ $errors->first('apakah_sdg_digunakan')}}
                                </div>
                    @endif

                  </div>

                
                <!-- /.card-body -->
                  <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection