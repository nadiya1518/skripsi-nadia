@extends('layouts.backend')

@section('title','Jabatan Fungsional Dosen')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jabatan Fungsional Dosen</a></li>
              <li class="breadcrumb-item active">Daftar Jabatan Fungsional Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Jabatan Fungsional Dosen</i></h5>
            <tr>
              <td>
                <a href="dosjabatan2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Jabatan Fungsional Dosen</i></button></a><br>
              </td><br>
            </tr>

            <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                              <th width="20px">No.</th>
                              <th width="200px">Jabatan Fungsional</th>
                              <th width="200px">SK Jabatan Fungsional</th>
                              <th width="200px">Tanggal Mulai</th>
                              <th width="200px">Tanggal Selesai</th>
                              <th width="100px">Aksi</th>
                            </tr>
                        </thead>
                       <tbody>
                        <?php $i=0; ?>
                         @foreach($jab_fungsional_dosen as $p)
                        
                        <tr>
                          <td>{{ ++$i }}</td>
                          <td>{{ $p->jabatan_fungsional }}</td>
                          <td>{{ $p->sk }}</td>
                          <td>{{ $p->tanggal_mulai }}</td>
                          <td>{{ $p->jabatan_selesai }}</td>
                          <td>

                          <a href="{{route('jabatan.edit',$p->id_jabatan)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

                          <a href="{{route('jabatan.delete',$p->id_jabatan)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                          </td>
                        </tr>
                        @endforeach
                     </tbody>
                    </table>


        </div>
   
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection