@extends('layouts.backend')

@section('title','Edit Mata Kuliah')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Mata Kuliah</a></li>
              <li class="breadcrumb-item active">Edit Mata Kuliah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Mata Kuliah</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
               <form method="post" action="{{route('matkul.update',$matakuliah->id_matakuliah)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

              <form role="form">
              <div class="card-body">
                 <div class="form-group">
                  <label for="exampleInputBakumutu1">Nama Kurikulum</label>
                    <select name="id_kurikulum" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($kurikulum as $semuakurikulum)
                     <option value="{{$semuakurikulum->id_kurikulum}}">{{ $semuakurikulum->nama_kurikulum}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama kurikulum'))
                                <div class="text-danger">
                                    {{ $errors->first('id_kurikulum')}}
                                </div>
                    @endif
                </div>

                <div class="form-group">
                    <label>Nama Matakuliah</label>
                    <input type="string" name="nama_matkul" class="form-control" placeholder="Nama Mata Kuliah" value=" {{ $matakuliah->nama_matkul }} ">

                    @if($errors->has('nama_matkul'))
                                <div class="text-danger">
                                    {{ $errors->first('nama_matkul')}}
                                </div>
                    @endif

                  </div>

                <div class="form-group">
                    <label>Semester</label>
                    <input type="string" name="semester" class="form-control" placeholder="Semester" value=" {{ $matakuliah->semester }} ">

                    @if($errors->has('semester'))
                                <div class="text-danger">
                                    {{ $errors->first('semester')}}
                                </div>
                    @endif

                  </div>


                  <div class="form-group">
                    <label>Kode Mata Kuliah</label>
                    <input type="string" name="kode_matkul" class="form-control" placeholder="Kode Mata Kuliah" value=" {{ $matakuliah->kode_matkul }} ">

                    @if($errors->has('kode matkul'))
                                <div class="text-danger">
                                    {{ $errors->first('kode matkul')}}
                                </div>
                    @endif

                  </div>


                  <div class="form-group">
                  <label for="exampleInputBakumutu1">Program Studi</label>
                    <select name="id_prodi" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($program_studi as $semuaprodi)
                     <option value="{{$semuaprodi->id_prodi}}">{{ $semuaprodi->nama_prodi}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama prodi'))
                                <div class="text-danger">
                                    {{ $errors->first('id_prodi')}}
                                </div>
                    @endif
                </div>

                  <div class="form-group">
                    <label>Jenis Mata Kuliah</label>
                    <input type="string" name="jenis_matkul" class="form-control" placeholder="Jenis Mata Kuliah" value=" {{ $matakuliah->jenis_matkul }} ">

                    @if($errors->has('jenis matkul'))
                                <div class="text-danger">
                                    {{ $errors->first('jenis matkul')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Kelompok Mata Kuliah</label>
                    <input type="string" name="kelompok_matkul" class="form-control" placeholder="Kelompok Mata Kuliah" value=" {{ $matakuliah->kelompok_matkul }} ">

                    @if($errors->has('kelompok matkul'))
                                <div class="text-danger">
                                    {{ $errors->first('kelompok matkul')}}
                                </div>
                    @endif

                  </div>


                  <div class="form-group">
                    <label>SKS Mata Kuliah</label>
                    <input type="string" name="sks_mk" class="form-control" placeholder="SKS Mata Kuliah" value=" {{ $matakuliah->sks_mk }} ">

                    @if($errors->has('sks mk'))
                                <div class="text-danger">
                                    {{ $errors->first('sks mk')}}
                                </div>
                    @endif

                  </div>

                   <div class="form-group">
                    <label>SKS Responsi/Tutorial</label>
                    <input type="string" name="sks_tm" class="form-control" placeholder="SKS Responsi/Matakuliah" value=" {{ $matakuliah->sks_tm }} ">

                    @if($errors->has('sks mk'))
                                <div class="text-danger">
                                    {{ $errors->first('sks mk')}}
                                </div>
                    @endif

                  </div>

                   <div class="form-group">
                    <label>SKS Praktikum</label>
                    <input type="string" name="sks_prak" class="form-control" placeholder="SKS Praktikum" value=" {{ $matakuliah->sks_prak }} ">

                    @if($errors->has('sks prak'))
                                <div class="text-danger">
                                    {{ $errors->first('sks prak')}}
                                </div>
                    @endif

                  </div>

                   <div class="form-group">
                    <label>SKS Praktik Lapangan</label>
                    <input type="string" name="sks_prak_lap" class="form-control" placeholder="SKS Praktik Lapangan" value=" {{ $matakuliah->sks_prak_lap }} ">

                    @if($errors->has('sks prak lap'))
                                <div class="text-danger">
                                    {{ $errors->first('sks prak lap')}}
                                </div>
                    @endif

                  </div>


                   <div class="form-group">
                    <label>SKS Seminar</label>
                    <input type="string" name="sks_sim" class="form-control" placeholder="SKS Seminar" value=" {{ $matakuliah->sks_sim }} ">

                    @if($errors->has('sks sim'))
                                <div class="text-danger">
                                    {{ $errors->first('sks sim')}}
                                </div>
                    @endif

                  </div>

                  <div class="form-group">
                    <label>Metode Pelakasanaan</label>
                    <input type="string" name="metode_pelaksanaan" class="form-control" placeholder="Metode Pelakasanaan" value=" {{ $matakuliah->metode_pelaksanaan }} ">

                    @if($errors->has('metode pelaksanaan'))
                                <div class="text-danger">
                                    {{ $errors->first('metode pelaksanaan')}}
                                </div>
                    @endif

                  </div>


                  <div class="form-group">
                      <label>Konversi Kredit Ke Jam</label>
                      <input type="string" name="konversi_kredit" class="form-control" placeholder="Konversi Kredit Ke Jam">
                      @if($errors->has('konversi kredit'))
                                <div class="text-danger">
                                    {{ $errors->first('konversi kredit')}}
                                </div>
                    @endif
                  </div>

                   

                   <div class="form-group">
                        <label>Capaian Sikap</label>
                        <select name="cp_sikap" class="form-control select2" style="width: 100%;">
                          <option>--Pilih--</option>
                          <option>Sangat Baik</option>
                          <option>Baik</option>
                          <option>Cukup</option>
                          <option>Kurang</option>
                        </select>
                        @if($errors->has('sikap'))
                                <div class="text-danger">
                                    {{ $errors->first('sikap')}}
                                </div>
                    @endif
                      </div>

                       <div class="form-group">
                        <label>Capaian Pengetahuan</label>
                        <select name="cp_pengetahuan" class="form-control select2" style="width: 100%;">
                          <option>--Pilih--</option>
                          <option>Sangat Baik</option>
                          <option>Baik</option>
                          <option>Cukup</option>
                          <option>Kurang</option>
                        </select>
                        @if($errors->has('pengetahuan'))
                                <div class="text-danger">
                                    {{ $errors->first('pengetahuan')}}
                                </div>
                    @endif
                      </div>

                   <div class="form-group">
                        <label>Capaian Keterampilan Umum</label>
                        <select name="cp_ket_umum" class="form-control select2" style="width: 100%;">
                          <option>--Pilih--</option>
                          <option>Sangat Baik</option>
                          <option>Baik</option>
                          <option>Cukup</option>
                          <option>Kurang</option>
                        </select>
                        @if($errors->has('keterampilan umum'))
                                <div class="text-danger">
                                    {{ $errors->first('keterampilan umum')}}
                                </div>
                    @endif
                      </div>

                   <div class="form-group">
                        <label>Capaian Keterampilan Khusus</label>
                        <select name="cp_ket_khusus" class="form-control select2" style="width: 100%;">
                          <option>--Pilih--</option>
                          <option>Sangat Baik</option>
                          <option>Baik</option>
                          <option>Cukup</option>
                          <option>Kurang</option>
                        </select>
                        @if($errors->has('keterampilan khusus'))
                                <div class="text-danger">
                                    {{ $errors->first('keterampilan khusus')}}
                                </div>
                    @endif
                      </div>


                <!-- /.card-body -->

                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="matakuliah1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection