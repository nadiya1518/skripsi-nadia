@extends('layouts.backend')

@section('title','Edit Jenis Bidang Keahlian')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Jenis Bidang Keahlian</a></li>
              <li class="breadcrumb-item active">Edit Jenis Bidang Keahlian</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Jenis Bidang Keahlian</h3>
          </div>

                <form method="post" action="{{route('bidang.update',$jenis_bidang_keahlian->id_jenis_keahlian)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                   <form role="form">
                    <div class="card-body">
                    <div class="form-group">
                    <label>Nama Bidang Keahlian*</label>
                    <input type="string" name="nama_jenis_bidang" class="form-control" placeholder="Nama Bidang Keahlian" value=" {{ $jenis_bidang_keahlian->nama_jenis_bidang }} ">

                    @if($errors->has('namabidang'))
                                <div class="text-danger">
                                    {{ $errors->first('namabidang')}}
                                </div>
                    @endif
                  </div>
                  
                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="dosbidang1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>

        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection