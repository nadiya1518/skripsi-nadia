@extends('layouts.backend')

@section('title','Tambah Data Dosen')

@section('content')

<?php
$sesuaikompetensi=[0=>'tidak', 1=>'ya'];

$sesuaimatkul = [0=>'tidak',1=>'ya'];
?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Dosen</a></li>
              <li class="breadcrumb-item active">Tambah Data Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Dosen</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->

              <form action="/datadosen2/store" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}

              <div class="card-body">
                  <div class="form-group">
                    <label>Nama Dosen</label>
                    <input type="string" name="nama_dosen" class="form-control" placeholder="Nama Dosen">
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('nama')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>NIDN</label>
                    <input type="string" name="nidn" class="form-control" placeholder="NIDN">
                    @if($errors->has('nidn'))
                                <div class="text-danger">
                                    {{ $errors->first('nidn')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Status</label>
                    <select name="id_jenis_dosen" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenisdosen as $semuajenis)
                     <option value="{{$semuajenis->id_jenis_dosen}}">{{ $semuajenis->jenis_dosen}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('bidangahli'))
                                <div class="text-danger">
                                    {{ $errors->first('id_bidang_dosen')}}
                                </div>
                    @endif
                </div>

                 <div class="form-group">
                    <label for="exampleInputBakumutu1">Program Studi</label>
                    <select name="id_prodi" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($prodi as $semuaprodi)
                     <option value="{{$semuaprodi->id_prodi}}">{{ $semuaprodi->nama_prodi}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('prodi'))
                                <div class="text-danger">
                                    {{ $errors->first('id_prodi')}}
                                </div>
                    @endif
                </div>

                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="datadosen1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>


      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection