@extends('layouts.backend')

@section('title','Tambah Penggunaan Dana')

@section('content')

<?php
$upps=[0=>'tidak', 1=>'ya'];

$Studi = [0=>'tidak',1=>'ya'];
?>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Penggunaan Dana</a></li>
              <li class="breadcrumb-item active">Tambah Penggunaan Dana</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Penggunaan Dana</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/rekappengeluaran2/store">
                {{ csrf_field() }}
              <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Sumber Pembiayaan*</label>
                    <select name="id_sumber_pembiaayaan" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($sumber_pembiayaan as $semuasumber)
                      <option value= "{{ $semuasumber->id_sumber_pembiaayaan}}"> {{$semuasumber->sumber_pembiayaan}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('sumber_pembiayaan'))
                                <div class="text-danger">
                                    {{ $errors->first('id_sumber_pembiaayaan')}}
                                </div>
                    @endif
                   
                  </div>

                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Jenis Pengeluaran*</label>
                    <select name="id_jenis_pengeluaran" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenis_pengeluaran as $semuajenis)
                      <option value="{{$semuajenis->id_jenis_pengeluaran}}">{{ $semuajenis->nama_jenis_pengeluaran}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama_jenis_pengeluaran'))
                                <div class="text-danger">
                                    {{ $errors->first('id_jenis_pengeluaran')}}
                                </div>
                    @endif
                    </select>
                  </div>


                  <div class="form-group">
                    <label>Waktu Pengeluaran*</label>
                    <input type="date" name="waktu_pengeluaran" class="form-control" placeholder="Waktu Pengeluaran">

                    @if($errors->has('waktu_pengeluaran'))
                                <div class="text-danger">
                                    {{ $errors->first('waktu_pengeluaran')}}
                                </div>
                    @endif
                    </div>


                  <div class="form-group">
                  <label>Unit Pengelola Program Studi</label>
                    <select name="apakah_pd_upps" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih</option>
                      @foreach($upps as $upps =>$upp)
                      <option value="{{$upps}}">{{$upp}}</option>
                      @endforeach
                    </select>
                      @if($errors->has('apakah_pd_upps'))
                                <div class="text-danger">
                                    {{ $errors->first('$apakah_pd_upps')}}
                                </div>
                      @endif
                    </select>
                  </div>
                    

                  <div class="form-group">
                    <label>Program Studi</label>
                      <select name="apakah_pd_ps" class="form-control select2" style="width: 100%;">
                        <option selected="selected">Pilih</option>
                          @foreach($Studi as $Studi =>$stu)
                        <option value="{{$Studi}}">{{$stu}}</option>
                          @endforeach
                      </select>
                          @if($errors->has('apakah_pd_ps'))
                            <div class="text-danger">
                              {{ $errors->first('$apakah_pd_ps')}}
                            </div>
                          @endif
                      </select>
                  </div>

                  <div class="form-group">
                    <label>Biaya*</label>
                    <input type="string" name="biaya" class="form-control" placeholder="Biaya">

                    @if($errors->has('biaya'))
                                <div class="text-danger">
                                    {{ $errors->first('biaya')}}
                                </div>
                    @endif
                    </div>  

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection