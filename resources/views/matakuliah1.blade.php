@extends('layouts.backend')

@section('title','Matakuliah')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Matakuliah</a></li>
              <li class="breadcrumb-item active">Daftar Matakuliah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
        <div class="card-body">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Matakuliah</i></h5>
            <tr>
              <td>
                <a href="matakuliah2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Matakuliah</i></button></a><br>
              </td><br>
            </tr>
             <br>
      
            <body>
            <table class="table table-bordered table-hover table-striped">
            <thead>
                <tr>
                  <th width="20px">No.</th>
                  <th width="200px">Nama Kurikulum</th>
                  <th width="200px">Nama Mata Kuliah
                  <th width="100px">Semester</th>
                  <th width="200px">Kode Mata Kuliah</th>
                  <th width="180px">Program Studi</th>
                  <th width="150px">Bobot Sks</th>
                  <th width="120px">Aksi</th>
                  </tr>

            </thead>
            <tbody>
             <?php $i = 0; ?>
             @foreach($matakuliah as $p)
             <tr>
               <td>{{ ++$i }}</td>
               <td>{{ $p->Kurikulum->nama_kurikulum }}</td> 
               <td>{{ $p->nama_matkul }}</td>
               <td>{{ $p->semester }}</td>
               <td>{{ $p->kode_matkul }}</td>
               <td>{{ $p->ProgramStudi->nama_prodi }}</td>
               <td>{{ $p->sks_mk}}</td>
               <td>
                  <a href="{{route('matkul.edit',$p->id_matakuliah)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>
                  <a href="{{route('matkul.delete',$p->id_matakuliah)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
               </td>
             </tr> 
               @endforeach
           </tbody>
           </table>
            </body>


        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection