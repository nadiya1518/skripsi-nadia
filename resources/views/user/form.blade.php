@if (isset($user->id))
	{!! Form::hidden('id', $user->id) !!}
@endif

{{--Nama--}}
@if ($error->any())
	<div class="form-group {{ $errors->has('username') ?
	'has-error' : 'has-success' }}">
@else
	<div class="form-group">
@endif
{!! Form::label('username','Username:',['class' =>'control-label']) !!}
{!! Form::text('name',null, ['class' =>'form-control']) !!}
	@if ($errors->has('username'))
	<span class="help-block">{{ $errors->first('username') }}</span>
	@endif
</div>

{{--Email--}}
if ($error->any())
	<div class="form-group {{ $errors->has('email') ?
	'has-error' : 'has-success' }}">
@else
	<div class="form-group">
@endif
{!! Form::label('username','Email:',['class' =>'control-label']) !!}
{!! Form::text('username',null, ['class' =>'form-control']) !!}
	@if ($errors->has('email'))
	<span class="help-block">{{ $errors->first('email') }}</span>
	@endif
</div>

{{--Role--}}
if ($error->any())
	<div class="form-group {{ $errors->has('role') ?
	'has-error' : 'has-success' }}">
@else
	<div class="form-group">
@endif
{!! Form::label('role','Role:',['class' =>'control-label']) !!}
	<div class="radio">
<label>{!! Form::radio('role','operator') !!} Operator</label>
	</div
	<div class="radio">
<label>{!! Form::radio('role','validator') !!} Validator</label>
	</div

@if ($errors->has('role'))
<span class="help-block"> {{ $errors->first('role') }}</span>
@endif
</div>

{{--Password--}}
@if ($errors->any())
	<div class="form-group" {{ $errors->has('password') ?
		'has-error' : 'has-success' }}">
@else
	<div class="form-group">
@endif
	{!! Form::label('password','Password:',
	['class' => 'control-label']) !!}
	{!! Form::password('password', ['class' =>'form-control']) !!}
		
@if ($errors->has('password'))
	<span class="help-block"> {{ $errors->first('role') }}</span>
@endif
</div>

{{--Password Confirmation--}}
if ($error->any())
	<div class="form-group {{ $errors->has('password_confirmation') ?
	'has-error' : 'has-success' }}">
@else
	<div class="form-group">
		@endif
		{!! Form::label('password_confirmation', 'Password Confirmation:',['class' => 'control-label']) !!}
		{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
@if ($errors->has('password_confirmation'))
	<span class="help-block"> {{ $errors->first('password_confirmation') }}</span>
@endif
</div>

{{--Submit Button--}}
<div class="form-group">
	{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>

	</div>
	}
}
}
	
