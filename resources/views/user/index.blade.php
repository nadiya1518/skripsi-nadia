@extends('template')

@section ('main')

	<div id="user">
		<h2>User</h2>
		@include('_partial.flash_message')

		@if (count($user_list)>0)
			<table class="table">
				<thead>
					<tr>
						<th>No.</th>
						<th>Nama</th>
						<th>Email</th>
						<th>Role</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 0; ?>
					<?php foreach($user_list as $user): ?>
					<tr>
						<td>{{ ++$i }}</td>
						<td>{{ $user->name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->role }}</td>
						<td>
                          <a href="{{route('user.edit',$b->id)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>
                          <a href="{{route('user.delete',$b->id)}}" <i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
					</tr>
				<?php endforeach ?>
				</tbody>
			</table>

			@else
				<p>Tidak ada data user.</p>
			@endif

			<div class="tombol-nav">
				<a href="user/create" class="btn btn-primary"> Tambah User</a>
			</div>

		</div> <!-- #kelas -->
		@stop

		@section('footer')
		@include('footer')
		@stop