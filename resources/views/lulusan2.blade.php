@extends('layouts.backend')

@section('title','Tambah Daftar Lulusan')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Daftar Lulusan</a></li>
              <li class="breadcrumb-item active">Tambah Daftar Lulusan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Daftar Lulusan</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/lulusan2/store" method="POST" enctype="multipart/form-data">
              {{ csrf_field() }}

              <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Nama Lulusan*</label>
                    <select name="id_mhs" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($mahasiswa as $semuamhs)
                     <option value="{{$semuamhs->id_mhs}}">{{ $semuamhs->nama_mhs}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama'))
                                <div class="text-danger">
                                    {{ $errors->first('id_mhs')}}
                                </div>
                    @endif
                </div>

                  <div class="form-group">
                    <label>IPK</label>
                    <input type="string" name="ipk" class="form-control" placeholder="IPK">
                    @if($errors->has('tanggal'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Tanggal Bekerja</label>
                    <input type="date" name="tanggal_bekerja" class="form-control" placeholder="Tanggal Bekerja">
                    @if($errors->has('tanggal'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Tanggal Selesai Bekerja</label>
                    <input type="date" name="tanggal_selesai_bekerja" class="form-control" placeholder="Tanggal Selesai Bekerja">
                    @if($errors->has('tanggal'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Nama Instansi</label>
                    <input type="string" name="nama_instansi" class="form-control" placeholder="Nama Instansi">
                    @if($errors->has('tanggal'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Alamat Instansi</label>
                    <input type="string" name="alamat" class="form-control" placeholder="Alamat">
                    @if($errors->has('tanggal'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal')}}
                                </div>
                    @endif
                  </div>

                  <div class="form-group">
                    <label>Email</label>
                    <input type="string" name="email" class="form-control" placeholder="Email">
                    @if($errors->has('tanggal'))
                                <div class="text-danger">
                                    {{ $errors->first('tanggal')}}
                                </div>
                    @endif
                  </div>
                  
                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection