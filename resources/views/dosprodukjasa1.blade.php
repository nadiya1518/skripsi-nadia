@extends('layouts.backend')

@section('title','Produk/Jasa Dosen')

@section('content')
  
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h4>Dashboard Akreditasi Program Studi</h4>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Produk dan Jasa Dosen</a></li>
              <li class="breadcrumb-item active">Daftar Produk dan Jasa Dosen</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
             
          <h5><i class="nav-icon fas fa-list">&ensp;Daftar Produk/Jasa Dosen</i></h5>
            <tr>
              <td>
                <a href="dosprodukjasa2"><button type="button" class="btn btn-primary btn-small"><i class="nav-icon fas fa-plus">&ensp;Tambah Data Produk/Jasa Dosen</i></button></a><br>
              </td><br>
            </tr>

            <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                              <th width="20px">No.</th>
                              <th width="300px">Nama Dosen</th>
                              <th width="300px">Nama Produk/Jasa</th>
                              <th width="300px">Deskripsi Produk/Jasa</th>
                              <th width="100px">Aksi</th>
                            </tr>
                        </thead>
                       <tbody>
                        <?php $i=0; ?>
                         @foreach($produk_atau_jasa as $p)
                        
                        <tr>
                          <td>{{ ++$i }}</td>
                          <td>{{ $p->nama }}</td>
                          <td>{{ $p->nama_produk_jasa }}</td>
                          <td>{{ $p->deskripsi_produk_jasa }}</td>
                          <td>

                          <a href="{{route('jenjang_pend.edit',$p->id_jenjang)}}" <i class="far fa-edit btn btn-sm btn-success"></i></a>

                          <a href="{{route('jenjang_pend.delete',$p->id_jenjang)}}" "<i class="far fa-trash-alt btn btn-sm btn-danger"></i></a>
                          </td>
                        </tr>
                        @endforeach
                     </tbody>
                    </table>


        </div>
   
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection