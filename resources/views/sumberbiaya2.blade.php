@extends('layouts.backend')

@section('title','Tambah Sumber Pembiayaan')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Sumber Pembiayaan</a></li>
              <li class="breadcrumb-item active">Tambah Sumber Pembiayaan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Sumber Pembiayaan</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="/sumberbiaya2/store">
                {{ csrf_field() }}
              <div class="card-body">

                  <div class="card-body">
                  <div class="form-group">
                    <label>Sumber Pembiayaan</label>
                    <input type="string" name="sumber_pembiayaan" class="form-control" placeholder="Sumber Pembiayaan">
                    @if($errors->has('sumber_pembiayaan'))
                                <div class="text-danger">
                                    {{ $errors->first('sumber_pembiayaan')}}
                                </div>
                    @endif
                  </div> 

                <br>
                  <button type="submit" class="btn btn-primary nav-icon" value="Simpan">Simpan</button>

                <tr>
                  <td>
                  <a href="sumberbiaya1"><button type="button" class="btn btn-danger nav-icon">Kembali ke Halaman Sebelumnya</button></a><br>
                  </td>
                </tr>
              </div>
            </form>
          </div>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection