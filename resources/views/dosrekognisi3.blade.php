@extends('layouts.backend')

@section('title','Edit Data')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Rekognisi/Pengakuan Dosen</a></li>
              <li class="breadcrumb-item active">Edit Data</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Data</h3>
          </div>

                <form method="post" action="{{route('rekognisi.update',$rekognisi_dosen->id_rekognisi)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                   <form role="form">
                    <div class="card-body">
                    <div class="form-group">
                    <label for="exampleInputBakumutu1">Nama Dosen</label>
                    <select name="id_dosen" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($dosen as $semuadosen)
                      <option value= "{{ $semuadosen->id_dosen}}"> {{$semuadosen->nama}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('nama_dosen'))
                                <div class="text-danger">
                                    {{ $errors->first('id_dosen')}}
                                </div>
                    @endif
                   
                  </div>

                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Bidang Keahlian</label>
                    <select name="id_bidang_keahlian" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($bidang_keahlian_dosen as $semuabidang)
                      <option value= "{{ $semuabidang->id_bidang_keahlian}}"> {{$semuabidang->bidang_keahlian_dosen}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('bidang_keahlian'))
                                <div class="text-danger">
                                    {{ $errors->first('id_bidang')}}
                                </div>
                    @endif
                   
                  </div>

                  <div class="form-group">
                    <label for="exampleInputFile">Bukti Pendukung</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input name="bukti_pendukung" type="file" class="custom-file-input" id="exampleInputFile">
                        @if($errors->has('file'))
                                <div class="text-danger">
                                    {{ $errors->first('file')}}
                                </div>
                        @endif
                        <label class="custom-file-label" for="exampleInputFile">Pilih file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div>

                   <div class="form-group">
                        <label>Tingkat</label>
                        <select name="tingkat" class="form-control select2" style="width: 100%;">">
                          <option>--Pilih--</option>
                          <option>Wilayah</option>
                          <option>Nasional</option>
                          <option>Internasional</option>
                        </select>
                         @if($errors->has('tingkat'))
                                <div class="text-danger">
                                    {{ $errors->first('tingkat')}}
                                </div>
                    @endif

                      </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection