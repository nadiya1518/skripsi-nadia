@extends('layouts.backend')

@section('title','Tambah Publikasi')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Publikasi</a></li>
              <li class="breadcrumb-item active">Tambah Publikasi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Publikasi</h3>
          </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
                  <div class="form-group">
                    <label>Judul Publikasi</label>
                    <input type="string" name="judul_publikasi" class="form-control" placeholder="Judul Publikasi">
                  </div>
                  <div class="form-group">
                    <label>Jenis Publikasi</label>
                    <input type="string" name="jenis_publikasi" class="form-control" placeholder="Jenis Publikasi">
                  </div>
                  <div class="form-group">
                    <label>Jenis Penulis</label>
                    <input type="string" name="jenis_penulis" class="form-control" placeholder="Jenis Penulis">
                  </div>
                  <div class="form-group">
                    <label>Urutan Penulis</label>
                    <input type="string" name="urutan_penulis" class="form-control" placeholder="Urutan Penulis">
                  </div>
                  <div class="form-group">
                    <label>Abstrak</label>
                    <input type="string" name="abstrak" class="form-control" placeholder="Abstrak">
                  </div>
                  <div class="form-group">
                    <label>Nama Jurnal</label>
                    <input type="string" name="nama_jurnal" class="form-control" placeholder="Nama Jurnal">
                  </div>
                  <div class="form-group">
                    <label>Hal</label>
                    <input type="string" name="hal" class="form-control" placeholder="Hal">
                  </div>
                  <div class="form-group">
                    <label>Vol</label>
                    <input type="string" name="vol" class="form-control" placeholder="Vol">
                  </div>
                  <div class="form-group">
                    <label>url</label>
                    <input type="string" name="url" class="form-control" placeholder="URL">
                  </div>
                  <div class="form-group">
                    <label>Afiliansi</label>
                    <input type="string" name="afiliansi" class="form-control" placeholder="Afiliansi">
                  </div>
                  <div class="form-group">
                    <label>Email Penulis</label>
                    <input type="string" name="email_penulis" class="form-control" placeholder="Email Penulis">
                  </div>
                </div>

                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon far fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection