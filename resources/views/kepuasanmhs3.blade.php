@extends('layouts.backend')

@section('title','Edit Data Kepuasan Mahasiswa')

@section('content')

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Dashboard Akreditasi Program Studi</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kepuasan Mahasiswa</a></li>
              <li class="breadcrumb-item active">Edit Data Kepuasan Mahasiswa</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <!-- Main content -->
    <section class="content">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Edit Riwayat Pendidikan</h3>
          </div>

                <form method="post" action="{{route('kepuasanmhs.update',$kepuasan_mahasiswa->id_kepuasan_mhs)}}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                  <form role="form">
                  <div class="card-body">

                  <div class="form-group">
                    <label for="exampleInputBakumutu1">Aspek Kepuasan Mahasiswa</label>
                    <select name="id_jenis_aspek" class="form-control select2" style="width: 100%;">
                      <option selected="selected">--Pilih--</option>
                      @foreach($jenisaspek as $semuaspek)
                     <option value="{{kepuasan_mahasiswa->id_jenis_aspek}}">{{ $semuaspek->jenis_aspek_kepuasan}}</option>
                      @endforeach
                    </select>
                    @if($errors->has('aspek'))
                                <div class="text-danger">
                                    {{ $errors->first('id_aspek')}}
                                </div>
                    @endif
                </div>


                  <div class="form-group">
                        <label>Tingkat</label>
                        <select name="tingkat" class="form-control select2" style="width: 100%;" value="{{ $kepuasan_mahasiswa->tingkat }}">
                          <option>--Pilih--</option>
                          <option>Wilayah/Lokal</option>
                          <option>Nasional</option>
                          <option>Internasional</option>
                        </select>
                        @if($errors->has('tingkat'))
                                <div class="text-danger">
                                    {{ $errors->first('tingkat')}}
                                </div>
                    @endif
                      </div>
                
                  <div class="form-group">
                      <label>Rencana Tindak Lanjut oleh UPPS/PS</label>
                      <input type="string" name="rencana_tindak_lannjut" class="form-control" placeholder="Rencana Tindak Lanjut oleh UPPS/PS">
                      @if($errors->has('rencana_tindak_lanjut'))
                                <div class="text-danger">
                                    {{ $errors->first('rencana_tindak_lanjut')}}
                                </div>
                    @endif
                  </div>

                <!-- /.card-body -->
                  <<div class="card-footer">
                  <button type="submit" class="btn btn-primary nav-icon fas fa-save" value="Simpan">&ensp;Simpan</button>
                </div>
              </div>
            </div>
          </form>
        </div>

        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>

@endsection