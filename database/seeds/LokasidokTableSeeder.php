<?php

use Illuminate\Database\Seeder;

class LokasidokTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lokasi_dokumen')->insert([
        [
        	'lokasi_dokumen' => 'Universitas'
        ],
        [
        	'lokasi_dokumen' => 'Fakultas Teknik'
        ],
        [
        	'lokasi_dokumen' => 'Jurusan Teknik Elektro'
        ]

        ]);
    }
}
 