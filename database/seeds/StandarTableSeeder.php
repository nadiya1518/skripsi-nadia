<?php

use Illuminate\Database\Seeder;
use App\Standarr;

class StandarTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('standar')->insert([
        [
        	'kode_standar' => '1',
        	'nama_standar' => 'Standar 1'
        ],
        [
            'kode_standar' => '2',
            'nama_standar' => 'Standar 2'
            
        ],
        [
            'kode_standar' => '3',
            'nama_standar' => 'Standar 3'
        ],
        [
            'kode_standar' => '4',
            'nama_standar' => 'Standar 4'
        ],
        [
            'kode_standar' => '5',
            'nama_standar' => 'Standar 5'
        ],
        [
            'kode_standar' => '6',
            'nama_standar' => 'Standar 6'
        ],
        [
            'kode_standar' => '7',
            'nama_standar' => 'Standar 7'
        ],
        [
            'kode_standar' => '8',
            'nama_standar' => 'Standar 8'
        ],
        [
            'kode_standar' => '9',
            'nama_standar' => 'Standar 9'
        ]
        ]);
    }
}
