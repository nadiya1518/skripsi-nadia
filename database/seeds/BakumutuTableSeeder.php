<?php

use Illuminate\Database\Seeder;

class BakumutuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('baku_mutu')->insert([
        [
        	'standar' => '1',
        	'butir' => '1.1',
        	'baku_mutu' => 'Surat Rektor No. 1934/UN26/DT/2012 tanggal 10 april 2012.Pada Tanggal 18 Juni 2014, permohonan pembentukan prodi baru ke Kemendikbud'
        ],
        [
            'standar' => '2',
            'butir' => '2.1',
            'baku_mutu' => 'Surat Keputusan (SK) Ditjen Dikti Nomor 441/E.E2/DT/2014 tentang Penugasan Penyelenggaraan PSTI.Penyelenggaraan PSTI'    
        ],
        [
            'standar' => '3',
            'butir' => '3.1',
            'baku_mutu' => 'Surat Keputusan Menteri Pendidikan dan Kebudayaan Republik Indonesia nomor 397/E/O/2014 tanggal 10 september 2014'    
        ]
    ]);
    }
}
