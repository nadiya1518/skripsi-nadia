<?php

use Illuminate\Database\Seeder;

class JenislokTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jenis_lokasi = ['Fakultas Teknik','Jurusan Teknik Elektro','Program Studi Teknik Informatika','Universitas'];
        foreach ($jenis_lokasi as $jl) {
        	DB::table('jenis_lokasi')->insert([
        	'jenis_lokasi' => $jl
    	]);
  
}
