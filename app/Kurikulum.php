<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kurikulum extends Model
{
    protected $table ='kurikulum';
    protected $fillable = ['nama_kurikulum','tgl_mulai_berlaku','tahun_mulai','apakah_sdg_digunakan'];
    protected $primaryKey ='id_kurikulum';

    public $timestamps =false;

}
