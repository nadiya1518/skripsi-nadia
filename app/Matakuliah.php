<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matakuliah extends Model
{
    protected $table ='matakuliah';
    protected $fillable = ['id_kurikulum','id_prodi','nama_matkul','semester','kode_matkul','jenis_matkul','kelompok_matkul','sks_mk','sks_tm','sks_prak','sks_prak_lap','sks_sim','metode_pelaksanaan','cp_sikap','cp_pengetahuan','cp_ket_umum','cp_ket_khusus',];
    protected $primaryKey ='id_matakuliah';

    public $timestamps =false;

    public function Kurikulum()
    {
    	return $this->belongsTo('App\Kurikulum','id_kurikulum','id_kurikulum');
    }

    public function ProgramStudi()
    {
    	return $this->belongsTo('App\ProgramStudi','id_prodi','id_prodi');
    }
}
