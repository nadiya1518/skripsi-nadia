<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisKemampuan extends Model
{
    protected $table ='jenis_kemampuan';
    protected $fillable = ['jenis_kemampuan'];
    protected $primaryKey ='id_jenis_kemampuan';

    public $timestamps =false;
}
 