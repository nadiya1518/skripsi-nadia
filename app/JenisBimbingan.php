<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisBimbingan extends Model
{
    protected $table ='jenis_bimbingan';
    protected $fillable = ['jenis_bimbingan'];
    protected $primaryKey ='id_jenis_bimbingan';

    public $timestamps =false;
}
