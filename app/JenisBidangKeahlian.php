<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisBidangKeahlian extends Model
{
    protected $table = 'jenis_bidang_keahlian';
    protected $fillable = ['nama_jenis_bidang'];
    protected $primaryKey = 'id_jenis_keahlian';

    public $timestamps=false;

    
}
