<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPublikasi extends Model
{
    protected $table = 'jenis_publikasi';
    protected $fillable = ['jenis_publikasi'];
    protected $primaryKey = 'id_jenis_publikasi';

    public $timestamps=false;
}
