<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LokasiDokumen extends Model
{
    protected $table = 'jenis_lokasi';
    protected $fillable = ['lokasi_dokumen'];
    protected $primaryKey = 'id_lok';

    public $timestamps =false; 
 
}
 