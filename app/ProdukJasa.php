<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProdukJasa extends Model
{
	protected $table = 'produk_atau_jasa';
    protected $fillable = ['nama','nama_produk_jasa','deskripsi_produk_jasa'];
    protected $primaryKey = 'id_produk_jasa';

    public $timestamps=false;

    public function Dosen() 
    {
    	return $this->belongsTo('App\Dosen','id_dosen','id_dosen');
    }
}
