<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JabatanFungsional extends Model
{
    protected $table = 'jab_fungsional_dosen';
    protected $fillable = ['jabatan_fungsional','sk','tanggal_mulai','tanggal_berakhir'];
    protected $primaryKey = 'id_jabatan';

    public $timestamps=false;

    public function Dosen() 
    {
    	return $this->belongsTo('App\Dosen','id_dosen','id_dosen');
    }
}
