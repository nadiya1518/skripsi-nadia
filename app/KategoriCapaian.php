<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class KategoriCapaian extends Model
{
    protected $table = 'kategori_capaian';
    protected $fillable = ['kategori_capaian'];
    protected $primaryKey = 'id_kat_capaian';

    public $timestamps=false;
}
