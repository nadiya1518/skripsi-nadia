<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramStudi extends Model
{
    protected $table ='program_studi';
    protected $fillable = ['nama_prodi','status_peringkat','nomor_sk','tanggal_kadaluarsa','tanggal_sk'];
    protected $primaryKey ='id_prodi';

    public $timestamps =false;
}
