<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisBimbingan;

class JenisBimbinganController extends Controller
{
    public function index()
    {
  	    $jenis_bimbingan = JenisBimbingan::all();
        return view('jenisbimbingan1', ['jenis_bimbingan' => $jenis_bimbingan]);
    }
    public function tambah()
    {
    	return view('jenisbimbingan2');
    }
    public function store(Request $request)
    {
        $jenis_bimbingan = new JenisBimbingan();
            $jenis_bimbingan->jenis_bimbingan = $request->jenis_bimbingan;
            $jenis_bimbingan->save();

    return redirect('/jenisbimbingan1');
    }
    public function edit($id)
    {
    $jenis_bimbingan = JenisBimbingan::find($id);
    return view('jenisbimbingan3', ['jenis_bimbingan' => $jenis_bimbingan]);
    }

    public function update(Request $request, $id)
    {
            $jenis_bimbingan = JenisBimbingan::find($id);
            $jenis_bimbingan->save();

        return redirect('/jenisbimbingan1');
    }
     public function delete($id)
    {
            $jenis_bimbingan = JenisBimbingan::find($id);
            $jenis_bimbingan->delete();
            return redirect('/jenisbimbingan1');
    }
}
