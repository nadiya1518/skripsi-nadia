<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisPrestasi;

class JenisPrestasiController extends Controller
{
    public function index()
    {
    	$jenis_prestasi = JenisPrestasi::all();
        return view('jenisprestasi1', ['jenis_prestasi' => $jenis_prestasi]);
    }
    public function tambah()
    {
    	return view('jenisprestasi2');
    }
     public function store(Request $request)
    {
    DB::table('jenis_prestasi')->insert([
    	'jenis_prestasi' => $request->jenis_prestasi,
    	]);
    return redirect('/jenisprestasi1');
    } 
    public function edit($id)
    {
    $jenis_prestasi = JenisPrestasi::find($id);
    return view('editprestasi', ['jenis_prestasi' => $jenis_prestasi]);
    }

    public function update(Request $request, $id)
    {
            $jenis_prestasi = JenisPrestasi::find($id);
            $jenis_prestasi->jenis_prestasi = $request->jenis_prestasi;
            $jenis_prestasi->save();
            return redirect('/jenisprestasi1');
    }
     public function delete($id)
    {
            $jenis_prestasi = JenisPrestasi::find($id);
            $jenis_prestasi->delete();
            return redirect('/jenisprestasi1');
    }
}
