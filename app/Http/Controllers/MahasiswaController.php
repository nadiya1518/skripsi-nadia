<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisPenerimaan;
use App\Mahasiswa;
use App\ProgramStudi;


class MahasiswaController extends Controller
{
    public function index()
    {
    	
        $mahasiswa = Mahasiswa::all(); 
        return view ('mhs1',['mahasiswa' =>$mahasiswa]);

    }
    public function tambah()
    {
    	$jenispenerimaan=JenisPenerimaan::Orderby("jenis_penerimaan","asc")->get();
        $program_studi=ProgramStudi::Orderby("nama_prodi","asc")->get();
        
        return view('mhs2',compact("jenispenerimaan","program_studi"));
    }

    public function store(Request $request)
    {
        $mahasiswa = new Mahasiswa();
            $mahasiswa->id_jenis_penerimaan = $request->id_jenis_penerimaan;
            $mahasiswa->nama_mhs = $request->nama_mhs;
            $mahasiswa->npm = $request->npm;
            $mahasiswa->id_prodi = $request->id_prodi;
            $mahasiswa->waktu_masuk = $request->waktu_masuk; 
            $mahasiswa->waktu_lulus = $request->waktu_lulus;
            $mahasiswa->tanggal_dan_tahun_transfer = $request->tanggal_dan_tahun_transfer;
            $mahasiswa->status_keaktifan = $request->status_keaktifan;  
            $mahasiswa->apakah_mhs_asing = $request->apakah_mhs_asing;
            $mahasiswa->jenis_mahasiswa = $request->jenis_mahasiswa;   
            $mahasiswa->save();

    return redirect('/mhs1');
    }
    public function edit($id)
    {
    $jenispenerimaan=JenisPenerimaan::Orderby("jenis_penerimaan","asc")->get();
    $program_studi=ProgramStudi::Orderby("nama_prodi","asc")->get();
    $mahasiswa = Mahasiswa::find($id);
    return view('mhs3', ['mahasiswa' => $mahasiswa], compact("jenispenerimaan"));
    }
    public function update($id, Request $request)
    {
            $mahasiswa = Mahasiswa::find($id);
            $mahasiswa->id_jenis_penerimaan = $request->id_jenis_penerimaan;
            $mahasiswa->nama_mhs =$request->nama_mhs;
            $mahasiswa->npm = $request->npm;
            $mahasiswa->id_prodi = $request->id_prodi;
            $mahasiswa->waktu_masuk = $request->waktu_masuk;
            $mahasiswa->waktu_lulus = $request->waktu_lulus;
            $mahasiswa->tanggal_dan_tahun_transfer = $request->tanggal_dan_tahun_transfer;
            $mahasiswa->status_keaktifan = $request->status_keaktifan;  
            $mahasiswa->apakah_mhs_asing = $request->apakah_mhs_asing;
            $mahasiswa->jenis_mahasiswa = $request->jenis_mahasiswa;  
            $mahasiswa->save();

            return redirect('/mhs1');
    }
    public function delete($id)
    {
            $mahasiswa = Mahasiswa::find($id);
            $mahasiswa->delete();
            return redirect('/mhs1');
    }

}
