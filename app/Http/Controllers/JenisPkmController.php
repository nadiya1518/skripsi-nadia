<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisPkm;

class JenisPkmController extends Controller
{
    public function jenispkm()
    {
        $jenis_pkm = JenisPkm::all();
        return view('jenispkm1', ['jenis_pkm' => $jenis_pkm]);
    }
    public function tjenispkm()
    {
    	return view('jenispkm2');
    }
    public function store(Request $request)
    {
    DB::table('jenis_pkm')->insert([
    	'nama_jenis_pkm' => $request->nama_jenis_pkm,	
    	]);
    	return redirect('/jenispkm1');
    }
    public function edit($id)
    {
    $jenis_pkm = JenisPkm::find($id);
    return view('jenispkm3', ['jenis_pkm' => $jenis_pkm]);
    }

    public function update(Request $request, $id)
    {
            $jenis_pkm = JenisPkm::find($id);
            $jenis_pkm->nama_jenis_pkm = $request->nama_jenis_pkm;
            $jenis_pkm->save();
            return redirect('/jenispkm1');
    }
     public function delete($id)
    {
            $jenis_pkm = JenisPkm::find($id);
            $jenis_pkm->delete();
            return redirect('/jenispkm1');
    }

}
