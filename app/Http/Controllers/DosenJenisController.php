<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisDosen;


class DosenJenisController extends Controller
{
    public function index()
    {
    	$jenis_dosen = JenisDosen::all();
        return view('dosjenis1', ['jenis_dosen' => $jenis_dosen]);   	
    }
    public function tambah()
    {
    	return view('dosjenis2');
    }
    public function store(Request $request)
    {
    	DB::table('jenis_dosen')->insert([
    	'jenis_dosen' => $request->jenis_dosen,
    	]);
    return redirect('/dosjenis1');
    }
    public function edit($id)
    {
    $jenis_dosen = JenisDosen::find($id);
    return view('dosjenis3', ['jenis_dosen' => $jenis_dosen]);
    }

    public function update(Request $request, $id)
    {
            $jenis_dosen = JenisDosen::find($id);
            $jenis_dosen->jenis_dosen = $request->jenis_dosen;
            $jenis_dosen->save();
            return redirect('/dosjenis1');
    }
     public function delete($id)
    {
            $jenis_dosen = JenisDosen::find($id);
            $jenis_dosen->delete();
            return redirect('/dosjenis1');
    }
}
