<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PenelitianController extends Controller
{
    public function penelitian()
    {
    	return view('dospenelitian1');
    }
    public function tpenelitian()
    {
        return view('dospenelitian2');
    } 

    public function jenispenelitian()
    {
    	return view('jenispenelitian1');
    }
    public function tjenispenelitian()
    {
    	return view('jenispenelitian2');
    }


    public function penelitiadosmhs()
    {
        return view('penelitiandosmhs1');
    }
    public function tambahpen()
    {
        return view('penelitiandosmhs2');
    }


    public function rujukanpenelitian()
    {
        return view('rujukanpenelitian1');
    }
    public function trujukan()
    {
        return view('rujukanpenelitian2');
    }


    public function luaranpenelitian()
    {
        return view('luaranpenelitian1');
    }
    public function tluaran()
    {
        return view('luaranpenelitian2');
    }


     public function integrasipen()
    {
        return view('integrasipenelitian1');
    }
    public function tintegrasi()
    {
        return view('integrasipenelitian2');
    }

}
