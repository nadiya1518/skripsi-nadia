<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriCapaian;

class KategoriCapaianController extends Controller
{
	public function index()
    {
    	$kategori_capaian = KategoriCapaian::all();
        return view('kategoricapaian1', ['kategori_capaian' => $kategori_capaian]);
    }
    public function tambah()
    {
    	return view('kategoricapaian2');
    }
    public function store(Request $request)
    {
        $kategori_capaian = new KategoriCapaian();
        $kategori_capaian->kategori_capaian = $request->kategori_capaian;  
        $kategori_capaian->save();

    return redirect('/kategoricapaian1');
    }
    public function edit($id)
    {
    $kategori_capaian = KategoriCapaian::find($id);
    return view('kategoricapaian3', ['kategori_capaian' => $kategori_capaian]);
    }

    public function update(Request $request, $id)
    {
            $kategori_capaian = KategoriCapaian::find($id);
            $kategori_capaian->kategori_capaian = $request->kategori_capaian; 
            $kategori_capaian->save();

        return redirect('/kategoricapaian1');
    }
     public function delete($id)
    {
            $kategori_capaian = KategoriCapaian::find($id);
            $kategori_capaian->delete();
            return redirect('/kategoricapaian1');
    }
}


