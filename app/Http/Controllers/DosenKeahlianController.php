<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BidangKeahliann;
use App\Dosen;
use App\JenisBidangKeahlian;

class DosenKeahlianController extends Controller
{
    public function index()
    {
    	$bidang_keahlian = BidangKeahliann::all();
    	return view('bidangahli1', ['bidang_keahlian' => $bidang_keahlian]);
    }
    public function tambah()
    {
        $datadosen=Dosen::Orderby("nama_dosen","asc")->get();
        $jenisbidang=JenisBidangKeahlian::Orderby("nama_jenis_bidang","asc")->get();
       
        return view('bidangahli2',compact("datadosen","jenisbidang"));
    }
    public function store(Request $request)
    {
    $bidang_keahlian = new BidangKeahliann();
        $bidang_keahlian->id_dosen = $request->id_dosen; 
        $bidang_keahlian->id_jenis_keahlian = $request->id_jenis_keahlian;  
        $bidang_keahlian->bid_sesuai_komp = $request->bid_sesuai_komp; 
        $bidang_keahlian->bid_sesuai_matkul = $request->bid_sesuai_matkul;  
        $bidang_keahlian->matkul_dlm_ps_a = $request->matkul_dlm_ps_a;    
        $bidang_keahlian->save();
    }
    public function edit($id)
    {
    $datadosen=Dosen::Orderby("nama_dosen","asc")->get();
    $jenisbidang=JenisBidangKeahlian::Orderby("nama_jenis_bidang","asc")->get();
    $bidang_keahlian = BidangKeahliann::find($id);
    return view('bidangahli3', ['bidang_keahlian' => $bidang_keahlian], compact("datadosen","jenisbidang"));
    }

    public function update(Request $request, $id)
    {
        $bidang_keahlian = BidangKeahliann::find($id);
        $bidang_keahlian->id_dosen = $request->id_dosen; 
        $bidang_keahlian->id_jenis_keahlian = $request->id_jenis_keahlian;  
        $bidang_keahlian->bid_sesuai_komp = $request->bid_sesuai_komp; 
        $bidang_keahlian->bid_sesuai_matkul = $request->bid_sesuai_matkul;  
        $bidang_keahlian->matkul_dlm_ps_a = $request->matkul_dlm_ps_a;    
        $bidang_keahlian->save();
            return redirect('/bidangahli1');
    }
     public function delete($id)
    {
            $bidang_keahlian = BidangKeahliann::find($id);
            $bidang_keahlian->delete();
            return redirect('/bidangkeahlian1');
    }


}
