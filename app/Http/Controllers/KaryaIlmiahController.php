<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KaryaIlmiahController extends Controller
{
    public function karyailmiah()
    {
    	return view('dospenelitian1');
    }
    public function tkaryailmiah()
    {
        return view('doskarya2');
    } 

    public function jenispenelitian()
    {
    	return view('jenispenelitian1');
    }
    public function tjenispenelitian()
    {
    	return view('jenispenelitian2');
    }
}
