<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{
    public function login()
    {
    	return view('auth.login');
    }
    public function postlogin(Request $request)
    {
    	if(Auth::attempt($request->only('email','password'))) {
            return redirect('dashboard');
        }
        return redirect('login');
    }
    public function logout()
    {
    	Auth::logout();
    	return redirect()->route('auth.login');
    }
    public function register()
    {
        return view('auth.register');
    }
    public function postregister(Request $request){
        $data =  new User();
        $data->username = $request->username;
        $data->email = $request->email;
        $data->password = bcrypt($request->password);
        $data->role = $request->role;
        $data->save();
        return redirect('login')->with('alert-success','Kamu berhasil Register');
    }
}

