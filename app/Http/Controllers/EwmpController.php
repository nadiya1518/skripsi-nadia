<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facade\DB;
use App\Dosen;
use App\JenisDosen;
use App\ProgramStudi;

class EwmpController extends Controller
{
    public function index()
    {
    	return view('ewmp1');
    }
    public function tambah()
    {
    	$dosen=Dosen::Orderby("nama_dosen","asc")->get();
    	$jenisdosen=JenisDosen::Orderby("jenis_dosen","asc")->get();
    	$prodi=ProgramStudi::Orderby("nama_prodi","asc")->get();
       
        return view('ewmp2',compact("dosen","jenisdosen","prodi"));
    }
}
