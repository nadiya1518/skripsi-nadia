<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisPenulis;

class JenisPenulisController extends Controller
{
    public function index()
    {
    	$jenis_penulis = JenisPenulis::all();
        return view('jenispenulis1', ['jenis_penulis' => $jenis_penulis]);
    }
    public function tambah()
    {
    	return view('jenispenulis2');
    }
    public function store(Request $request)
    {
        $jenis_penulis = new JenisPenulis();
        $jenis_penulis->nama_jenis_penulis = $request->nama_jenis_penulis;  
        $jenis_penulis->save();
    return redirect('/jenispenulis1');
    } 
    public function edit($id)
    {
    $jenis_penulis = JenisPenulis::find($id);
    return view('editpenulis', ['jenis_penulis' => $jenis_penulis]);
    }

    public function update(Request $request, $id)
    {
            $jenis_penulis = JenisPenulis::find($id);
            $jenis_penulis->nama_jenis_penulis = $request->nama_jenis_penulis;
            $jenis_penulis->save();
            return redirect('/jenispenulis1');
    }
     public function delete($id)
    {
            $jenis_penulis = JenisPenulis::find($id);
            $jenis_penulis->delete();
            return redirect('/jenispenulis1');
    }
} 
