<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProdukJasa;
use App\Dosen;

class ProdukJasaController extends Controller
{
    public function index()
    {
    	$produk_atau_jasa = ProdukJasa::all();
    	return view('dosprodukjasa1', ['produk_atau_jasa' => $produk_atau_jasa]);
    }
    public function tambah()
    {
    	return view ('dosprodukjasa2');
    }
    public function store(Request $request) 
    {
    $produk_atau_jasa = new ProdukJasa();
            $produk_atau_jasa->nama = $request->nama;
            $produk_atau_jasa->nama_produk_jasa = $request->nama_produk_jasa;
            $produk_atau_jasa->deskripsi_produk_jasa = $request->deskripsi_produk_jasa;
            $produk_atau_jasa->save();

    return redirect('/dosprodukjasa1');
    }
    public function edit($id)
    {
    $produk_atau_jasa = ProdukJasa::find($id);
    return view('dosprodukjasa3', ['produk_atau_jasa' => $produk_atau_jasa]);
    }

    public function update(Request $request, $id)
    {
            $produk_atau_jasa = ProdukJasa::find($id);
            $produk_atau_jasa->nama = $request->nama;
            $produk_atau_jasa->nama_produk_jasa = $request->nama_produk_jasa;
            $produk_atau_jasa->deskripsi_produk_jasa = $request->deskripsi_produk_jasa;
            $produk_atau_jasa->save();

    return redirect('/dosprodukjasa1');
    }
     public function delete($id)
    {
            $produk_atau_jasa = ProdukJasa::find($id);
            $produk_atau_jasa->delete();
            return redirect('/dosprodukjasa1');
    }
}
