<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PembimbingTa;
use App\Dosen;
use App\Mahasiswa;
use App\JenisBimbingan;
use App\ProgramStudi;

class PembimbingTaController extends Controller
{
    public function index()
    {
        $pembimbing_tugas_akhir = PembimbingTa::all();
        return view('dospembimbing1', ['pembimbing_tugas_akhir' => $pembimbing_tugas_akhir]);
    }
    public function tambah()
    {
    	$dosen=Dosen::Orderby("nama_dosen","asc")->get();
    	$mahasiswa=Mahasiswa::Orderby("nama_mhs","asc")->get();
    	$jenis_bimbingan=JenisBimbingan::Orderby("jenis_bimbingan","asc")->get();
        $prodi=ProgramStudi::Orderby("nama_prodi","asc")->get();
    	return view('dospembimbing2',compact("dosen","mahasiswa","jenis_bimbingan","prodi"));
    }
    public function store(Request $request)
    {
        $pembimbing_tugas_akhir = new PembimbingTa();
            $pembimbing_tugas_akhir->id_dosen = $request->id_dosen;
            $pembimbing_tugas_akhir->id_mhs = $request->id_mhs;
            $pembimbing_tugas_akhir->id_prodi = $request->id_prodi;
            $pembimbing_tugas_akhir->id_jenis_bimbingan = $request->id_jenis_bimbingan;
            $pembimbing_tugas_akhir->save();

    return redirect('/dospembimbing1');
    }
    public function edit($id)
    {
    $dosen=Dosen::Orderby("nama_dosen","asc")->get();
    $mahasiswa=Mahasiswa::Orderby("nama_mhs","asc")->get();
    $jenis_bimbingan=JenisBimbingan::Orderby("jenis_bimbingan","asc")->get();
    $prodi=ProgramStudi::Orderby("nama_prodi","asc")->get();

    $pembimbing_tugas_akhir = PembimbingTa::find($id);
    return view('dospembimbing3', ['pembimbing_tugas_akhir' => $pembimbing_tugas_akhir], compact("dosen","mahasiswa","jenis_bimbingan","prodi"));
    }
    public function update(Request $request, $id)
    {  
          $pembimbing_tugas_akhir = PembimbingTa::find($id);
            $pembimbing_tugas_akhir->id_dosen = $request->id_dosen;
            $pembimbing_tugas_akhir->id_mhs = $request->id_mhs;
            $pembimbing_tugas_akhir->id_prodi = $request->id_prodi;
            $pembimbing_tugas_akhir->id_jenis_bimbingan = $request->id_jenis_bimbingan;
            $pembimbing_tugas_akhir->save();
        return redirect('/dospembimbing1');
    }
     public function delete($id)
    {
            $pembimbing_tugas_akhir = PembimbingTa::find($id);
            $pembimbing_tugas_akhir->delete();
            return redirect('/dospembimbing1');
    }
}
