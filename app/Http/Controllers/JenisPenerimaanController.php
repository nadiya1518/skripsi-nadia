<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisPenerimaan;

class JenisPenerimaanController extends Controller
{
    public function index()
    {
    	$jenis_penerimaan = JenisPenerimaan::all();
    	return view('jenispenerimaan1', ['jenis_penerimaan' => $jenis_penerimaan]);
    }
    public function tambah()
    {
    	return view('jenispenerimaan2');
    }
    public function store(Request $request)
    {
    	DB::table('jenis_penerimaan')->insert([
    	'jenis_penerimaan' => $request->jenis_penerimaan,
    	]);
    return redirect('/jenispenerimaan1');
	} 
	 public function edit($id)
    {
    $jenis_penerimaan = JenisPenerimaan::find($id);
    return view('jenispenerimaan3', ['jenis_penerimaan' => $jenis_penerimaan]);
    }

    public function update(Request $request, $id)
    {
            $jenis_penerimaan = JenisPenerimaan::find($id);
            $jenis_penerimaan->jenis_penerimaan = $request->jenis_penerimaan;
            $jenis_penerimaan->save();
            return redirect('/jenispenerimaan1');
    }
     public function delete($id)
    {
            $jenis_penerimaan = JenisPenerimaan::find($id);
            $jenis_penerimaan->delete();
            return redirect('/jenispenerimaan1');
    }
}

