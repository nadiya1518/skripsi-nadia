<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JabatanFungsional;
use App\Dosen;

class DosenJabatanController extends Controller
{
    public function index()
    {
    	$jab_fungsional_dosen = JabatanFungsional::all();
    	return view('dosjabatan1', ['jab_fungsional_dosen' => $jab_fungsional_dosen]);
    }
    public function tambah()
    {
        $dosen=Dosen::Orderby("nama_dosen","asc")->get();
        return view('dosjabatan2',compact("dosen"));
    }
    public function store(Request $request) 
    {
    $jab_fungsional_dosen = new JabatanFungsional();
            $jab_fungsional_dosen->jabatan_fungsional = $request->jabatan_fungsional;
            $jab_fungsional_dosen->sk = $request->sk;
            $jab_fungsional_dosen->tanggal_mulai = $request->tanggal_mulai;
            $jab_fungsional_dosen->tanggal_berakhir = $request->tanggal_berakhir;
            $jab_fungsional_dosen->save();

    return redirect('/dosjabatan1');
    }
    public function edit($id)
    {
    $jab_fungsional_dosen = JabatanFungsional::find($id);
    return view('dosjabatan3', ['jabatan_fungsional_dosen' => $jabatan_fungsional_dosen]);
    }

    public function update(Request $request, $id)
    {
            $jab_fungsional_dosen = JabatanFungsional::find($id);
            $jab_fungsional_dosen->jabatan_fungsional = $request->jabatan_fungsional;
            $jab_fungsional_dosen->sk = $request->sk;
            $jab_fungsional_dosen->tanggal_mulai = $request->tanggal_mulai;
            $jab_fungsional_dosen->tanggal_berakhir = $request->tanggal_berakhir;
            $jab_fungsional_dosen->save();

    return redirect('/dosjabatan1');
    }
     public function delete($id)
    {
            $jab_fungsional_dosen = JabatanFungsional::find($id);
            $jab_fungsional_dosen->delete();
            return redirect('/dosjabatan1');
    }

}
