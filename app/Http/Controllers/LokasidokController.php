<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\LokasiDokumen;

class LokasidokController extends Controller
{
    public function index()
    {
        $lokasi_dokumen = LokasiDokumen::all();
        return view('lokasidok1', ['lokasi_dokumen' => $lokasi_dokumen]);
    }
    public function tlokasi() 
    {
    	return view ('lokasidok2');
    }
    public function store(Request $request)
    {
    		 DB::table('jenis_lokasi')->insert([
    		'lokasi_dokumen' => $request->lokasi_dokumen,
    		]);
            return redirect('/lokasidok1');
    }
    public function edit($id)
    {
    $lokasi_dokumen = LokasiDokumen::find($id);
            return view('editlokasi', ['lokasi_dokumen' => $lokasi_dokumen]);
    }
    public function update($id, Request $request)
    {
            $lokasi_dokumen = LokasiDokumen::find($id);
            $lokasi_dokumen->lokasi_dokumen = $request->lokasi_dokumen;
            $lokasi_dokumen->save();

            return redirect('/lokasidok1');
    }
    public function delete($id)
    {
        $lokasi_dokumen = LokasiDokumen::find($id);
        $lokasi_dokumen->delete();
        
        return redirect('/lokasidok1');
    }

}
