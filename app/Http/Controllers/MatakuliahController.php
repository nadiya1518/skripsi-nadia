<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Matakuliah;
use App\Kurikulum;
use App\ProgramStudi;

class MatakuliahController extends Controller
{
   public function index()
    {
    	
        $matakuliah = Matakuliah::all();
        return view('matakuliah1', ['matakuliah' => $matakuliah]);
    }
    public function tambah()
    {
        $kurikulum=Kurikulum::Orderby("nama_kurikulum","asc")->get();
        $program_studi=ProgramStudi::Orderby("nama_prodi","asc")->get();

    	return view('matakuliah2',compact("kurikulum","program_studi"));
    }
    public function store(Request $request)
    {
        $matakuliah = new Matakuliah();
            $matakuliah->id_kurikulum = $request->id_kurikulum;
            $matakuliah->id_prodi = $request->id_prodi;
            $matakuliah->nama_matkul = $request->nama_matkul;
            $matakuliah->semester = $request->semester;
            $matakuliah->kode_matkul = $request->kode_matkul;
            $matakuliah->jenis_matkul = $request->jenis_matkul;
            $matakuliah->kelompok_matkul = $request->kelompok_matkul;
            $matakuliah->sks_mk = $request->sks_mk;
            $matakuliah->sks_tm = $request->sks_tm;
            $matakuliah->sks_prak = $request->sks_prak;
            $matakuliah->sks_prak_lap = $request->sks_prak_lap;
            $matakuliah->sks_sim = $request->sks_sim;
            $matakuliah->metode_pelaksanaan = $request->metode_pelaksanaan;
            $matakuliah->cp_sikap = $request->cp_sikap;
            $matakuliah->cp_pengetahuan = $request->cp_pengetahuan;
            $matakuliah->cp_ket_umum = $request->cp_ket_umum;
            $matakuliah->cp_ket_khusus = $request->cp_ket_khusus;
            $matakuliah->save();

    return redirect('/matakuliah1');
    }
    public function edit($id)
    {
    $kurikulum=Kurikulum::Orderby("nama_kurikulum","asc")->get();
    $program_studi=ProgramStudi::Orderby("nama_prodi","asc")->get();
    $matakuliah = Matakuliah::find($id);
    return view('matakuliah3', ['matakuliah' => $matakuliah], compact("kurikulum","program_studi"));
    }

    public function update(Request $request, $id)
    {
            $matakuliah = Matakuliah::find($id);
            $matakuliah->id_kurikulum = $request->id_kurikulum;
            $matakuliah->id_prodi = $request->id_prodi;
            $matakuliah->nama_matkul = $request->nama_matkul;
            $matakuliah->semester = $request->semester;
            $matakuliah->kode_matkul = $request->kode_matkul;
            $matakuliah->jenis_matkul = $request->jenis_matkul;
            $matakuliah->kelompok_matkul = $request->kelompok_matkul;
            $matakuliah->sks_mk = $request->sks_mk;
            $matakuliah->sks_tm = $request->sks_tm;
            $matakuliah->sks_prak = $request->sks_prak;
            $matakuliah->sks_prak_lap = $request->sks_prak_lap;
            $matakuliah->sks_sim = $request->sks_sim;
            $matakuliah->metode_pelaksanaan = $request->metode_pelaksanaan;
            $matakuliah->cp_sikap = $request->cp_sikap;
            $matakuliah->cp_pengetahuan = $request->cp_pengetahuan;
            $matakuliah->cp_ket_umum = $request->cp_ket_umum;
            $matakuliah->cp_ket_khusus = $request->cp_ket_khusus;
            $matakuliah->save();

        return redirect('/matakuliah1');
    }
     public function delete($id)
    {
            $matakuliah = Matakuliah::find($id);
            $matakuliah->delete();
            return redirect('/matakuliah1');
    }

}
