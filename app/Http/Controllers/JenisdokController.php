<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisDokumen;

class JenisdokController extends Controller
{
    public function index()
    {
        $jenis_dokumen = JenisDokumen::all();
        return view('jenisdok1', ['jenis_dokumen' => $jenis_dokumen]);
    }
    public function jenis()
    {
    	return view ('jenisdok2');
    }
     public function store(Request $request)
    {
    DB::table('jenis_dokumen')->insert([
    	'jenis_dokumen' => $request->jenis_dokumen,
    	]);
    return redirect('/jenisdok1');
    }
    public function edit($id)
    {
            $jenis_dokumen = JenisDokumen::find($id);
            return view('editjenis', ['jenis_dokumen' => $jenis_dokumen]);
    }

    public function update($id, Request $request)
    {
            $jenis_dokumen = JenisDokumen::find($id);
            $jenis_dokumen->jenis_dokumen = $request->jenis_dokumen;
            $jenis_dokumen->save();
            return redirect('/jenisdok1');
    }
    public function delete($id)
    {
            $jenis_dokumen = JenisDokumen::find($id);
            $jenis_dokumen->delete();
            return redirect('/jenisdok1');
    }
}
