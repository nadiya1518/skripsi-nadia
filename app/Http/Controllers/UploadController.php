<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\BakuMutu;
use App\LokasiDokumen;
use App\JenisDokumen;
use App\Standarr;
use App\Dokumenn;
use App\Uploadd;

class UploadController extends Controller
{
    public function index()
    {
        $upload = Uploadd::all(); 
    	return view ('uploaddokumen1',['upload' =>$upload]);

    }
    public function tambah()
    {
    	$bakumutu=BakuMutu::Orderby("standar","asc")->Orderby('butir',"asc")->get();
    	$lokasidokumen=LokasiDokumen::Orderby("lokasi_dokumen","asc")->get();
    	$jenisdokumen=JenisDokumen::Orderby("jenis_dokumen","asc")->get();
        $standar=Standarr::Orderby("standar","asc")->get();
    	return view ('uploaddokumen2',compact("bakumutu","lokasidokumen","jenisdokumen","standar"));

    }
    public function store(Request $request)
    { 
            $file = $request->file;
            $mime = $file->getClientMimeType();
            $nama_asli = $file->getClientOriginalName();
            $bytea = base64_encode(file_Get_contents($file->getPathName()));

            $dokumen = new Dokumenn();
            $dokumen->nama_dok = $request->nama_dok;
            $dokumen->file = $bytea;
            $dokumen->mime_type = $mime;
            $dokumen->nama_file = $nama_asli;
            $dokumen->id_jenis = $request->id_jenis;
            $dokumen->keterangan = $request->keterangan;
            $dokumen->link_url = $request->link_url;
            $dokumen->waktu_unggah = date('Y-m-d H:i:s');
            $file->move($file->getClientOriginalName());    
            $dokumen->save();

            $upload = Uploadd::create([
                'id_dok' => $dokumen->id_dok,
                'id_baku' => $request->id_baku,
                'id_standar' => $request->id_standar,
                'id_lok' => $request->id_lok,
            ]);

            return redirect('/uploaddokumen2');
    }
    public function edit($id)
    {
            $dokumen = Dokumenn::find($id);
            return view('uploaddokumen3', ['dokumen' => $dokumen]);
    }
    public function update($id, Request $request)
    {
        $file = $request->file;
            $mime = $file->getClientMimeType();
            $nama_asli = $file->getClientOriginalName();
            $bytea = base64_encode(file_Get_contents($file->getPathName()));

            $dokumen = Dokumenn::find($id);
            $dokumen = Dokumenn::where('id',$id)->first();
            $dokumen->nama_dok = $request->input('nama_dok');
            $dokumen->file =$request->input('file');
            $dokumen->keterangan = $request->input('keterangan');
            $dokumen->link_url =$request->input('link_url');
            $dokumen->id_jenis =$request->input('id_jenis');
            $dokumen->waktu_unggah = date('Y-m-d H:i:s');
            $file->move($file->getClientOriginalName()); 
            $dokumen->save();

            $upload = Uploadd::find($id);
            $upload = Uploadd::where('id',$id)->first();
                $upload->id_dok = $request->$dokumen->input('id_dok');
                $upload->id_baku = $request->input('id_baku');
                $upload->id_standar = $request->input('id_standar');
                $upload->id_lok = $request->input('id_lok');
                $upload->save();
            
            return redirect('/uploaddokumen1');
    }
    public function delete($id)
    {
            $upload = Uploadd::find($id);
            $upload->delete();
            return redirect('/uploaddokumen1');
    }

    public function download($id, Request $request)
    {
        $dokumen = Dokumenn::findOrFail($id);
        if(!is_null($dokumen->file)){
        $content = base64_decode(stream_get_contents($dokumen->file));
        return response($content)
          ->header('Content-Type',$dokumen->mime_type)
          ->header('Content-Disposition', "attachment; filename={$dokumen->nama_file}");
    }
    else{
     abort(404);
    }

    }
} 
