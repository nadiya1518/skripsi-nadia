<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\BakuMutu;

class BakumutuController extends Controller
{
   public function index()
    {
        $baku_mutu = BakuMutu::all();
        return view('halbaku', ['baku_mutu' => $baku_mutu]);
    }
    public function tbaku()
    {
    	return view ('tbaku');
    }
     public function store(Request $request) 
    {
     DB::table('baku_mutu')->insert([
    		'standar' => $request->standar,
    		'butir' => $request->butir,
    		'baku_mutu' => $request->baku_mutu,
    	]);
 
    	return redirect('/halbaku');
    }

    public function edit($id)
    {
            $baku_mutu = BakuMutu::find($id);
            return view('editbaku', ['baku_mutu' => $baku_mutu]);
    }

    public function update($id, Request $request)
    {
            $baku_mutu = BakuMutu::find($id);
            $baku_mutu->butir = $request->butir;
            $baku_mutu->baku_mutu =$request->baku_mutu;
            $baku_mutu->save();
            return redirect('/halbaku');
    }
    public function delete($id)
    {
            $baku_mutu = BakuMutu::find($id);
            $baku_mutu->delete();
            return redirect('/halbaku');
    }
}