<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ProgramStudi;


class ProgStudiController extends Controller
{
    public function index()
    {
        $program_studi = ProgramStudi::all(); 
        return view ('progstud1',['program_studi' =>$program_studi]);
    }
    public function tambah()
    {
    	return view('progstud2');
    }
    public function store(Request $request)
    {
        $program_studi = new ProgramStudi();
            $program_studi->nama_prodi = $request->nama_prodi;
            $program_studi->status_peringkat = $request->status_peringkat;
            $program_studi->nomor_sk = $request->nomor_sk;
            $program_studi->tanggal_sk = $request->tanggal_sk;
            $program_studi->tanggal_kadaluarsa = $request->tanggal_kadaluarsa;
            $program_studi->save();

    return redirect('/progstud1');
    }
    public function edit($id)
    {
    $program_studi = ProgramStudi::find($id);
    return view('progstud3', ['program_studi' => $program_studi]);
    }

    public function update(Request $request, $id)
    {
            $program_studi = ProgramStudi::find($id);
            $program_studi->nama_prodi = $request->nama_prodi;
            $program_studi->status_peringkat = $request->status_peringkat;
            $program_studi->nomor_sk = $request->nomor_sk;
            $program_studi->tanggal_sk = $request->tanggal_sk;
            $program_studi->tanggal_kadaluarsa = $request->tanggal_kadaluarsa;
            $program_studi->save();

        return redirect('/progstud1');
    }
     public function delete($id)
    {
            $program_studi = ProgramStudi::find($id);
            $program_studi->delete();
            return redirect('/progstudi1');
    }
}