<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisPublikasi;

class JenisPubController extends Controller
{
    public function index()
    {
    	$jenis_publikasi = JenisPublikasi::all();
        return view('jenispub1', ['jenis_publikasi' => $jenis_publikasi]);
    	
    }
    public function tambah()
    {
    	return view('jenispub2');
    }
    public function store(Request $request)
    {
    DB::table('jenis_publikasi')->insert([
    	'jenis_publikasi' => $request->jenis_publikasi,
    	]);
    return redirect('/jenispub1');
	}
    public function edit($id)
    {
            $jenis_publikasi = JenisPublikasi::find($id);
            return view('jenispub3', ['jenis_publikasi' => $jenis_publikasi]);
    }

    public function update($id, Request $request)
    {
            $jenis_publikasi = JenisPublikasi::find($id);
            $jenis_publikasi->jenis_publikasi = $request->jenis_publikasi;
            $jenis_publikasi->save();
            return redirect('/jenispub1');
    }
    public function delete($id)
    {
            $jenis_publikasi = JenisPublikasi::find($id);
            $jenis_publikasi->delete();
            return redirect('/jenispub1');
    }
}
