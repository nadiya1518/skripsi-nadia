<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\KategoriKegiatan;

class KategoriKegiatanController extends Controller
{
    public function index()
    {
    	$kategori_kegiatan = KategoriKegiatan::all();
        return view('kategorikegiatan1', ['kategori_kegiatan' => $kategori_kegiatan]);
    }
    public function tambah()
    {
    	return view('kategorikegiatan2');
    }
    public function store(Request $request)
    {
        $kategori_kegiatan = new KategoriKegiatan();
            $kategori_kegiatan->nama_kategori = $request->nama_kategori;  
            $kategori_kegiatan->save();

    return redirect('/kategorikegiatan1');
    }
    public function edit($id)
    {
    $kategori_kegiatan = KategoriKegiatan::find($id);
    return view('kategorikegiatan3', ['kategori_kegiatan' => $kategori_kegiatan]);
    }

    public function update(Request $request, $id)
    {
            $kategori_kegiatan = KategoriKegiatan::find($id);
            $kategori_kegiatan->nama_kategori = $request->nama_kategori; 
            $kategori_kegiatan->save();

        return redirect('/kategorikegiatan1');
    }
     public function delete($id)
    {
            $kategori_kegiatan = KategoriKegiatan::find($id);
            $kategori_kegiatan->delete();
            return redirect('/kategorikegiatan1');
    }
}

