<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisPkm;

class PkmController extends Controller
{
    public function pkm()
    {
    	return view('dospkm1');
    }
    public function tpkm()
    {
        return view('dospkm2');
    }


    public function pkmdosmhs()
    {
        return view('pkmdosmhs1');
    }
    public function tpkmdosmhs()
    {
        return view('pkmdosmhs2');
    }
}
