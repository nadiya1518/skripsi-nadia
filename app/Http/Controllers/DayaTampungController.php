<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facade\DB;
use App\DayaTampung;
use App\JenisPenerimaan;

class DayaTampungController extends Controller
{
    public function index()
    {
    	$daya_tampung = DayaTampung::all();
        return view('dayatampung1', ['daya_tampung' => $daya_tampung]);
    }
    public function tambah()
    {
    	$jenispenerimaan=JenisPenerimaan::Orderby("jenis_penerimaan","asc")->get();
       
        return view('dayatampung2',compact("jenispenerimaan"));
    }
    public function store(Request $request)
    {
        $daya_tampung = new DayaTampung();
            $daya_tampung->pelaksana = $request->pelaksana;
            $daya_tampung->tahun_masuk = date('Y-m-d H:i:s');
            $daya_tampung->id_jenis_penerimaan = $request->id_jenis_penerimaan;
            $daya_tampung->total_daya_tampung = $request->total_daya_tampung;
            $daya_tampung->pendaftar = $request->pendaftar;
            $daya_tampung->lulus_seleksi = $request->lulus_seleksi;
            $daya_tampung->save();

    return redirect('/dayatampung1');
    }
    public function edit($id)
    {

    $jenispenerimaan=JenisPenerimaan::Orderby("jenis_penerimaan","asc")->get();
    $daya_tampung = DayaTampung::find($id);
    return view('dayatampung3', ['daya_tampung' => $daya_tampung], compact("jenispenerimaan"));

    }

    public function update(Request $request, $id)
    {
            $daya_tampung = DayaTampung::find($id);
            $daya_tampung->pelaksana = $request->pelaksana;
            $daya_tampung->tahun_masuk = date('Y-m-d H:i:s');
            $daya_tampung->id_jenis_penerimaan = $request->id_jenis_penerimaan;
            $daya_tampung->total_daya_tampung = $request->total_daya_tampung;
            $daya_tampung->pendaftar = $request->pendaftar;
            $daya_tampung->lulus_seleksi = $request->lulus_seleksi;
            $daya_tampung->save();

        return redirect('/dayatampung1');
    }
     public function delete($id)
    {
            $daya_tampung = DayaTampung::find($id);
            $daya_tampung->delete();
            return redirect('/dayatampung1');
    }
}


