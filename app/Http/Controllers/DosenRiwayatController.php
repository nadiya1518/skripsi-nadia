<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RiwayatPendidikan;
use App\JenjangPendidikan;
use App\Dosen;

class DosenRiwayatController extends Controller
{ 
    public function index()
    {
    	$riwayat_pend_formal_dosen = RiwayatPendidikan::all();
    	return view('dosriwayat1', ['riwayat_pend_formal_dosen' => $riwayat_pend_formal_dosen]);
    }
    public function tambah()
    {		
    	$dosen=Dosen::Orderby("nama","asc")->get();
    	$jenjang_pendidikan=JenjangPendidikan::Orderby("nama_jenjang","asc")->get();
       
        return view('dosriwayat2',compact("dosen","jenjang_pendidikan"));
    }
     public function store(Request $request)
    {
        $riwayat_pend_formal_dosen = new RiwayatPendidikan();
        	$riwayat_pend_formal_dosen->id_dosen = $request->id_dosen;
            $riwayat_pend_formal_dosen->perguruan_tinggi = $request->perguruan_tinggi;
            $riwayat_pend_formal_dosen->id_jenjang =$request->id_jenjang;
            $riwayat_pend_formal_dosen->fakultas = $request->fakultas;
            $riwayat_pend_formal_dosen->jurusan = $request->jurusan;
            $riwayat_pend_formal_dosen->prodi = $request->prodi;
            $riwayat_pend_formal_dosen->judul_ta = $request->judul_ta;

            $riwayat_pend_formal_dosen->save();

    return redirect('/dosriwayat1');
    }
    public function edit($id)
    {

    $jenjang_pendidikan=JenjangPendidikan::Orderby("nama_jenjang","asc")->get();
    $dosen=Dosen::Orderby("nama","asc")->get();

    $riwayat_pend_formal_dosen = RiwayatPendidikan::find($id);

    return view('dosriwayat3', ['riwayat_pend_formal_dosen' => $riwayat_pend_formal_dosen], compact("jenjang_pendidikan","dosen"));

    }

    public function update(Request $request, $id)
	{
    $riwayat_pend_formal_dosen = new RiwayatPendidikan();
        	$riwayat_pend_formal_dosen->id_dosen = $request->id_dosen;
            $riwayat_pend_formal_dosen->perguruan_tinggi = $request->perguruan_tinggi;
            $riwayat_pend_formal_dosen->id_jenjang =$request->id_jenjang;
            $riwayat_pend_formal_dosen->jurusan = $request->jurusan;
            $riwayat_pend_formal_dosen->prodi = $request->prodi;
            $riwayat_pend_formal_dosen->judul_ta = $request->judul_ta;

            $riwayat_pend_formal_dosen->save();

    return redirect('/dosriwayat1');
    }
     public function delete($id)
    {
            $riwayat_pend_formal_dosen = RiwayatPendidikan::find($id);
            $riwayat_pend_formal_dosen->delete();
            return redirect('/dosriwayat1');
    }
}


