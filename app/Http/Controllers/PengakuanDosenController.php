<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PengakuanDosen;
use App\Dosen;
use App\JenisBidangKeahlian;

class PengakuanDosenController extends Controller
{
    public function index()
    {
  	    $rekognisi_dosen = PengakuanDosen::all();
        return view('dosrekognisi1', ['rekognisi_dosen' => $rekognisi_dosen]);
    }
    public function tambah()
    {
    	$dosen=Dosen::Orderby("nama","asc")->get();
    	$bidang_keahlian_dosen=JenisBidangKeahlian::Orderby("bidang_keahlian_dosen","asc")->get();
    	return view('dosrekognisi2',compact("dosen","bidang_keahlian_dosen"));
    }
    public function store(Request $request)
    {
        $rekognisi_dosen = new PengakuanDosen();
            $rekognisi_dosen->id_kategori = $request->id_kategori;
            $rekognisi_dosen->id_bidang_keahlian = $request->id_bidang_keahlian;
            $rekognisi_dosen->bukti_pendukung = $request->bukti_pendukung;
            $rekognisi_dosen->tingkat = $request->tingkat;
            $rekognisi_dosen->tahun = $request->tahun;
            $rekognisi_dosen->save();

    return redirect('/dosrekognisi1');
    }
    public function edit($id)
    {
    $rekognisi_dosen = PengakuanDosen::find($id);
    return view('dosrekognisi3', ['rekognisi_dosen' => $rekognisi_dosen]);
    }

    public function update(Request $request, $id)
    {
           $rekognisi_dosen = new PengakuanDosen();
            $rekognisi_dosen->id_kategori = $request->id_kategori;
            $rekognisi_dosen->id_bidang_keahlian = $request->id_bidang_keahlian;
            $rekognisi_dosen->bukti_pendukung = $request->bukti_pendukung;
            $rekognisi_dosen->tingkat = $request->tingkat;
            $rekognisi_dosen->tahun = $request->tahun;
            $rekognisi_dosen->save();

        return redirect('/dosrekognisi1');
    }
     public function delete($id)
    {
            $rekognisi_dosen = PengakuanDosen::find($id);
            $rekognisi_dosen->delete();
            return redirect('/dosrekognisi1');
    }
}


