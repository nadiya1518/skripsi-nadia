<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kurikulum;

class KurikulumController extends Controller
{
    public function index()
    {
    	
        $kurikulum = Kurikulum::all();
        return view('kurikulum1', ['kurikulum' => $kurikulum]);
    }
    public function tambah()
    {
    	return view('kurikulum2');
    }
    public function store(Request $request)
    {
        $kurikulum = new Kurikulum();
            $kurikulum->nama_kurikulum = $request->nama_kurikulum;
            $kurikulum->tgl_mulai_berlaku = $request->tgl_mulai_berlaku;
            $kurikulum->tahun_mulai = $request->tahun_mulai;
            $kurikulum->apakah_sdg_digunakan = $request->apakah_sdg_digunakan;
            $kurikulum->save();

    return redirect('/kurikulum1');
    }
    public function edit($id)
    {
    $kurikulum = Kurikulum::find($id);
    return view('kurikulum3', ['kurikulum' => $kurikulum]);
    }

    public function update(Request $request, $id)
    {
            $kurikulum = Kurikulum::find($id);
            $kurikulum->nama_kurikulum = $request->nama_kurikulum;
            $kurikulum->tgl_mulai_berlaku = $request->tgl_mulai_berlaku;
            $kurikulum->tahun_mulai = $request->tahun_mulai;
            $kurikulum->apakah_sdg_digunakan = $request->apakah_sdg_digunakan;
            $kurikulum->save();

        return redirect('/kurikulum1');
    }
     public function delete($id)
    {
            $kurikulum = Kurikulum::find($id);
            $kurikulum->delete();
            return redirect('/kurikulum1');
    }
}

