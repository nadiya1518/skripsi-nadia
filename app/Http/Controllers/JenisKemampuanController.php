<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisKemampuan;

class JenisKemampuanController extends Controller
{
    public function index()
    {
    	$jenis_kemampuan = JenisKemampuan::all();
        return view('jeniskemampuan1', ['jenis_kemampuan' => $jenis_kemampuan]);
    }
    public function tambah()
    {
    	return view('jeniskemampuan2');
    }
     public function store(Request $request)
    {
    DB::table('jenis_kemampuan')->insert([
    	'jenis_kemampuan' => $request->jenis_kemampuan,
    	]);
    return redirect('/jeniskemampuan1');
	} 
    public function edit($id)
    {
    $jenis_kemampuan = JenisKemampuan::find($id);
    return view('editkemampuan', ['jenis_kemampuan' => $jenis_kemampuan]);
    }

    public function update(Request $request, $id)
    {
            $jenis_kemampuan = JenisKemampuan::find($id);
            $jenis_kemampuan->jenis_kemampuan = $request->jenis_kemampuan;
            $jenis_kemampuan->save();
            return redirect('/jeniskemampuan1');
    }
     public function delete($id)
    {
            $jenis_kemampuan = JenisKemampuan::find($id);
            $jenis_kemampuan->delete();
            return redirect('/jeniskemampuan1');
    }
}
