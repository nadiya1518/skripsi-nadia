<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\RekapPengeluaran;
use App\SumberPembiayaan; 
use App\JenisPengeluaran;

class RekapPengeluaranController extends Controller
{
    public function index()
    {
    	$rekap_pengeluaran = RekapPengeluaran::all();
        return view('rekappengeluaran1', ['rekap_pengeluaran' => $rekap_pengeluaran]);
    }
    public function tambah()
    {
    	$sumber_pembiayaan=SumberPembiayaan::Orderby("sumber_pembiayaan","asc")->get();
    	$jenis_pengeluaran=JenisPengeluaran::Orderby("jenis_pengeluaran","asc")->get();
    	return view('rekappengeluaran2',compact("sumber_pembiayaan","jenis_pengeluaran"));
    }
    public function store(Request $request)
    {
        $rekap_pengeluaran = new RekapPengeluaran();
            $rekap_pengeluaran->id_sumber_pembiaayaan = $request->id_sumber_pembiaayaan;
            $rekap_pengeluaran->id_jenis_pengeluaran = $request->id_jenis_pengeluaran;
            $rekap_pengeluaran->waktu_pengeluaran = $request->waktu_pengeluaran;
            $rekap_pengeluaran->apakah_pd_upps = $request->apakah_pd_upps;
            $rekap_pengeluaran->apakah_pd_ps = $request->apakah_pd_ps;
            $rekap_pengeluaran->biaya = $request->biaya;
            $rekap_pengeluaran->save();

    return redirect('/rekappengeluaran1');
    }
    public function edit($id)
    {
    $sumber_pembiayaan=SumberPembiayaan::Orderby("sumber_pembiayaan","asc")->get();
    $jenis_pengeluaran=JenisPengeluaran::Orderby("jenis_pengeluaran","asc")->get();

    $rekap_pengeluaran = RekapPengeluaran::find($id);
    return view('rekappengeluaran3', ['rekap_pengeluaran' => $rekap_pengeluaran], compact("sumber_pembiayaan","jenis_pengeluaran"));
    }

    public function update(Request $request, $id)
    {
       $rekap_pengeluaran = RekapPengeluaran::find($id);
            $rekap_pengeluaran->id_sumber_pembiaayaan = $request->id_sumber_pembiaayaan;
            $rekap_pengeluaran->id_jenis_pengeluaran = $request->id_jenis_pengeluaran;
            $rekap_pengeluaran->waktu_pengeluaran = $request->waktu_pengeluaran;
            $rekap_pengeluaran->apakah_pd_upps = $request->apakah_pd_upps;
            $rekap_pengeluaran->apakah_pd_ps = $request->apakah_pd_ps;
            $rekap_pengeluaran->biaya = $request->biaya;
            $rekap_pengeluaran->save();

    return redirect('/rekappengeluaran1');
    }
     public function delete($id)
    {
            $rekap_pengeluaran = RekapPengeluaran::find($id);
            $rekap_pengeluaran->delete();
            return redirect('/rekappengeluaran1');
    }
}
