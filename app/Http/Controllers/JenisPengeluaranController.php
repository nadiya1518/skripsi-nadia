<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisPengeluaran;

class JenisPengeluaranController extends Controller
{
     public function index()
    {
    	$jenis_pengeluaran = JenisPengeluaran::all();
        return view('jenispengeluarandana1', ['jenis_pengeluaran' => $jenis_pengeluaran]);
    }
    public function tambah()
    {
    	return view('jenispengeluarandana2');
    }
    public function store(Request $request)
    {
    	DB::table('jenis_pengeluaran')->insert([
    	'nama_jenis_pengeluaran' => $request->nama_jenis_pengeluaran,
    	]);
    return redirect('/jenispengeluarandana1');
    } 
    public function edit($id)
    {
    $jenis_pengeluaran = JenisPengeluaran::find($id);
    return view('jenispengeluarandana3', ['jenis_pengeluaran' => $jenis_pengeluaran]);
    }

    public function update(Request $request, $id)
    {
            $jenis_pengeluaran = JenisPengeluaran::find($id);
            $jenis_pengeluaran->nama_jenis_pengeluaran = $request->nama_jenis_pengeluaran;
            $jenis_pengeluaran->save();
            return redirect('/jenispengeluarandana1');
    }
     public function delete($id)
    {
            $jenis_pengeluaran = JenisPengeluaran::find($id);
            $jenis_pengeluaran->delete();
            return redirect('/jenispengeluarandana1');
    }
}
