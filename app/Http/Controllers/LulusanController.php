<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facade\DB;
use App\Lulusan;
use App\Mahasiswa;

class LulusanController extends Controller
{
    public function index()
    {
    	$lulusan = Lulusan::all();
        return view('lulusan1', ['lulusan' => $lulusan]);
    }
    public function tambah()
    {   
        $mahasiswa=Mahasiswa::Orderby("nama_mhs","asc")->get();
       
        return view('lulusan2',compact("mahasiswa"));
    }
    public function store(Request $request)
    {
        $lulusan = new Lulusan();
            $lulusan->id_mhs = $request->id_mhs;
            $lulusan->ipk = $request->ipk;
            $lulusan->tanggal_bekerja = $request->tanggal_bekerja;
            $lulusan->tanggal_selesai_bekerja = $request->tanggal_selesai_bekerja;
            $lulusan->nama_instansi = $request->nama_instansi;
            $lulusan->alamat = $request->alamat;
            $lulusan->email = $request->email;
            $lulusan->save();

    return redirect('/lulusan1');
    }
    public function edit($id)
    {
    $mahasiswa=Mahasiswa::Orderby("nama_mhs","asc")->get();
    $lulusan = Lulusan::find($id);
    return view('lulusan3', ['lulusan' => $lulusan], compact("mahasiswa"));
    }

    public function update(Request $request, $id)
    {
            $lulusan = Lulusan::find($id);
            $lulusan->id_mhs = $request->id_mhs;
            $lulusan->ipk = $request->ipk;
            $lulusan->tanggal_bekerja = $request->tanggal_bekerja;
            $lulusan->tanggal_selesai_bekerja = $request->tanggal_selesai_bekerja;
            $lulusan->nama_instansi = $request->nama_instansi;
            $lulusan->alamat = $request->alamat;
            $lulusan->email = $request->email;
            $lulusan->save();

        return redirect('/lulusan1');
    }
     public function delete($id)
    {
            $lulusan = Lulusan::find($id);
            $lulusan->delete();
            return redirect('/lulusan1');
    }
}
