<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Dosen;
use App\JenisDosen;
use App\ProgramStudi;

class DosenController extends Controller
{
    public function index()
    {
    	$dosen = Dosen::all(); 
        return view ('datadosen1',['dosen' =>$dosen]);
 
    }
    public function tambah()
    {
    	$jenisdosen=JenisDosen::Orderby("jenis_dosen","asc")->get();
        $prodi=ProgramStudi::Orderby("nama_prodi","asc")->get();
       
        return view('datadosen2',compact("jenisdosen","prodi"));
    }

    public function store(Request $request)
    {
        $dosen = new Dosen();
            $dosen->nama_dosen = $request->nama_dosen;
            $dosen->nidn = $request->nidn;
            $dosen->id_jenis_dosen = $request->id_jenis_dosen;
            $dosen->id_prodi = $request->id_prodi;
            $dosen->save();

    return redirect('/datadosen1');
    }
    public function edit($id)
    {
    $jenisdosen=JenisDosen::Orderby("jenis_dosen","asc")->get();
    $prodi=ProgramStudi::Orderby("nama_prodi","asc")->get();

    $dosen = Dosen::find($id);
    return view('datadosen3', ['dosen' => $dosen], compact("jenisdosen","prodi"));
    }
    public function update($id, Request $request)
    {
            $dosen = Dosen::find($id);
            $dosen->nama_dosen = $request->nama_dosen;
            $dosen->nidn =$request->nidn;
            $dosen->id_jenis_dosen = $request->id_jenis_dosen;
            $dosen->id_prodi = $request->id_prodi;
            $dosen->save();

            return redirect('/datadosen1');
    }
    public function delete($id)
    {
            $dosen = Dosen::find($id);
            $dosen->delete();
            return redirect('/datadosen1');
    }

}


    