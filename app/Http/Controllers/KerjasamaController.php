<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Kerjasama;
use App\Dokumenn;

class KerjasamaController extends Controller
{
    public function index()
    {
    	$kerjasama = Kerjasama::all(); 
    	return view ('kerjasama1',['kerjasama' =>$kerjasama]);
    }
    public function tambah()
    {
    	return view('kerjasama2');
    }
    public function store(Request $request)
    {
        DB::table('kerjasama')->insert([
        'lembaga_mitra' => $request->lembaga_mitra,
        'tingkat'  => $request->tingkat,
        'judul_kegiatan_kerjasama' => $request->judul_kegiatan_kerjasama,
        'manfaat_kerjasama' => $request->manfaat_kerjasama,
        'waktu_mulai_kerjasama' => $request->waktu_mulai_kerjasama,
        'waktu_berakhir_kerjasama' => $request->waktu_berakhir_kerjasama,
        ]);
    return redirect('/kerjasama1');
    } 
	public function edit($id)
    {
    $kerjasama = Kerjasama::find($id);
    return view('kerjasama3', ['kerjasama' => $kerjasama]);
    }

    public function update(Request $request, $id)
    {
            $kerjasama = Kerjasama::find($id);
            $kerjasama->kerjasama = $request->kerjasama;
            $kerjasama->save();
            return redirect('/kerjasama1');
    }
     public function delete($id)
    {
            $kerjasama = Kerjasama::find($id);
            $kerjasama->delete();
            return redirect('/kerjasama1');
    }
}
