<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Standarr;

class StandarrController extends Controller
{
    public function index()
    {
    	$standar = Standarr::all();
    	return view('halstan', ['standar' => $standar]);
    }
    public function tst()
    {
    	return view ('tstandar');
    }
     public function store(Request $request) 
    {
    DB::table('standar')->insert([
    	'kode_standar' => $request->kode_standar,
    	'nama_standar' => $request->nama_standar
    	]);
  
    	return redirect('/halstan'); 
    }
    public function edit($id)
    {
    $standar = Standarr::find($id);
    return view('editstandar', ['standar' => $standar]);
    }

    public function update(Request $request, $id)
    {
            $standar = Standarr::find($id);
            $standar->kode_standar = $request->kode_standar;
            $standar->nama_standar =$request->nama_standar;
            $standar->save();
            return redirect('/halstan');
    }
     public function delete($id)
    {
            $standar = Standarr::find($id);
            $standar->delete();
            return redirect('/halstan');
    }
}
