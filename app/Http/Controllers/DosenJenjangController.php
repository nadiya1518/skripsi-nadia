<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenjangPendidikan;

class DosenJenjangController extends Controller
{
    public function index()
    {
    	$jenjang_pendidikan = JenjangPendidikan::all();
    	return view('dosjenjang1', ['jenjang_pendidikan' => $jenjang_pendidikan]);
    }
    public function tambah()
    {
    	return view ('dosjenjang2');
    }
     public function store(Request $request)
    {
    DB::table('jenjang_pendidikan')->insert([
    	'nama_jenjang' => $request->nama_jenjang,	
    	]);
    	return redirect('/dosjenjang1');
    }
    public function edit($id)
    {
    $jenjang_pendidikan = JenjangPendidikan::find($id);
    return view('dosjenjang3', ['jenjang_pendidikan' => $jenjang_pendidikan]);
    }

    public function update(Request $request, $id)
    {
            $jenjang_pendidikan = JenjangPendidikan::find($id);
            $jenjang_pendidikan->nama_jenjang = $request->nama_jenjang;
            $jenjang_pendidikan->save();
            return redirect('/dosjenjang1');
    }
     public function delete($id)
    {
            $jenjang_pendidikan = JenjangPendidikan::find($id);
            $jenjang_pendidikan->delete();
            return redirect('/dosjenjang1');
    }

}
