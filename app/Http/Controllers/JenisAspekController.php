<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisAspek;

class JenisAspekController extends Controller
{
    public function index()
    {
    	$jenis_aspek_kepuasan_mhs = JenisAspek::all();
        return view('jenisaspek1', ['jenis_aspek_kepuasan_mhs' => $jenis_aspek_kepuasan_mhs]);
    }
    public function tambah()
    {
    	return view('jenisaspek2');
    }
    public function store(Request $request)
    {
    DB::table('jenis_aspek_kepuasan_mhs')->insert([
    	'jenis_aspek_kepuasan' => $request->jenis_aspek_kepuasan,
    	]);

    return redirect('/jenisaspek1');
    }

}
