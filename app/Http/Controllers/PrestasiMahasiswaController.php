<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisPrestasi;
use App\PrestasiMahasiswa;
use App\Mahasiswa;
use App\ProgramStudi;


class PrestasiMahasiswaController extends Controller
{
     public function index()
    {
    	$prestasi_mahasiswa = PrestasiMahasiswa::all();
        return view('mhsprestasi1', ['prestasi_mahasiswa' => $prestasi_mahasiswa]);
    }
    public function tambah()
    {
    	$mahasiswa=Mahasiswa::Orderby("nama_mhs","asc")->Orderby("npm","asc")->Orderby("id_prodi","asc")->get();
    	$jenis_prestasi=JenisPrestasi::Orderby("jenis_prestasi","asc")->get();
    	return view('mhsprestasi2',compact("mahasiswa","jenis_prestasi"));
    }
    public function store(Request $request)
    {
        $prestasi = new PrestasiMahasiswa();
            $prestasi->nama_mhs = $request->nama_mhs;
            $prestasi->nama_kegiatan = $request->nama_kegiatan;
            $prestasi->waktu_perolehan = $request->waktu_perolehan;
            $prestasi->tingkat = $request->tingkat;
            $prestasi->id_jenis_prestasi = $request->id_jenis_prestasi;
            $prestasi->prestasi_yang_dicapai = $request->prestasi_yang_dicapai;    
            $prestasi->save();

    return redirect('/mhsprestasi1');
    }
    public function edit($id)
    {
    $prestasi = PrestasiMahasiswa::find($id);
    return view('mhsprestasi3', ['prestasi' => $prestasi]);
    }

    public function update(Request $request, $id)
    {
            $prestasi = PrestasiMahasiswa::find($id);
            $prestasi->nama_mhs = $request->nama_mhs;
            $prestasi->nama_kegiatan = $request->nama_kegiatan;
            $prestasi->waktu_perolehan = $request->waktu_perolehan;
            $prestasi->tingkat = $request->tingkat;
            $prestasi->id_jenis_prestasi = $request->id_jenis_prestasi;
            $prestasi->prestasi_yang_dicapai = $request->prestasi_yang_dicapai;  
            $prestasi->save();

        return redirect('/mhsprestasi1');
    }
     public function delete($id)
    {
            $prestasi = PrestasiMahasiswa::find($id);
            $prestasi->delete();
            return redirect('/mhsprestasi1');
    }
}

