<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\SumberPembiayaan;

class SumberPembiayaanController extends Controller
{ 
    public function index()
    {
    	$sumber_pembiayaan = SumberPembiayaan::all();
    	return view('sumberbiaya1', ['sumber_pembiayaan' => $sumber_pembiayaan]);
    }
    public function tambah()
    {
    	return view ('sumberbiaya2');
    }
    public function store(Request $request) 
    {
    DB::table('sumber_pembiayaan')->insert([
    'sumber_pembiayaan' => $request->sumber_pembiayaan,
    ]);
 
    	return redirect('/sumberbiaya1');
    }
    public function edit($id)
    {
    $sumber_pembiayaan = SumberPembiayaan::find($id);
    return view('sumberbiaya3', ['sumber_pembiayaan' => $sumber_pembiayaan]);
    }

    public function update(Request $request, $id)
    {
            $sumber_pembiayaan = SumberPembiayaan::find($id);
            $sumber_pembiayaan->sumber_pembiayaan = $request->sumber_pembiayaan;
            $sumber_pembiayaan->save();
            return redirect('/sumberbiaya1');
    }
     public function delete($id)
    {
            $sumber_pembiayaan = SumberPembiayaan::find($id);
            $sumber_pembiayaan->delete();
            return redirect('/sumberbiaya1');
    }
}
