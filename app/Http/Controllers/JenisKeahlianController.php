<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisBidangKeahlian;

class JenisKeahlianController extends Controller
{
    public function index()
    {
    	$jenis_bidang_keahlian = JenisBidangKeahlian::all();
    	return view('dosbidang1', ['jenis_bidang_keahlian' => $jenis_bidang_keahlian]);
    }
    public function tambah()
    {
    	return view ('dosbidang2');
    }
    public function store(Request $request)
    {
        $jenis_bidang_keahlian = new JenisBidangKeahlian();
            $jenis_bidang_keahlian->nama_jenis_bidang = $request->nama_jenis_bidang;
            $jenis_bidang_keahlian->save();
        return redirect('/dosbidang1');
    }
    public function edit($id)
    {
    $jenis_bidang_keahlian= JenisBidangKeahlian::find($id);
    return view('dosbidang3', ['jenis_bidang_keahlian' => $jenis_bidang_keahlian]);
    }

    public function update(Request $request, $id)
    {
            $jenis_bidang_keahlian = JenisBidangKeahlian::find($id);
            $jenis_bidang_keahlian->nama_jenis_bidang = $request->nama_jenis_bidang;
            $jenis_bidang_keahlian->save();
            return redirect('/dosbidang1');
    }
     public function delete($id)
    {
            $jenis_bidang_keahlian = JenisBidangKeahlian::find($id);
            $jenis_bidang_keahlian->delete();
            return redirect('/dosbidang1');
    }

}

 
