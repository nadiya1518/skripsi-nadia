<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\JenisPenulis;
use App\JenisDokumen;
use App\KategoriKegiatan;
use App\DokumenPublikasi;
use App\Dokumenn;
use App\AnggotaPublikasi;
use App\PublikasiKarya;
use App\LokasiDokumen;
use App\KategoriCapaian;

class PublikasiKaryaController extends Controller
{
	public function index()
    {
    	$publikasi_karya_ilmiah = PublikasiKarya::all();
    	return view('publikasikarya1', ['publikasi_karya_ilmiah' => $publikasi_karya_ilmiah]);
    	
    }
    public function tambah()
    {   
    	$jenis_penulis=JenisPenulis::Orderby("jenis_penulis","asc")->get();
    	$jenis_dokumen=JenisDokumen::Orderby("jenis_dokumen","asc")->get();
    	$lokasi_dokumen=LokasiDokumen::Orderby("lokasi_dokumen","asc")->get();
    	$kategori_kegiatan=KategoriKegiatan::groupBy("id_kategori")->get();
        $kategori_capaian=KategoriCapaian::Orderby("kategori_capaian","asc")->get();
    	return view('publikasikarya2',compact("jenis_penulis","jenis_dokumen","lokasi_dokumen","kategori_kegiatan","kategori_capaian"));
    }
    public function store(Request $request)
    { 
            $publikasi_karya = PublikasiKarya::create([
                'kategori_kegiatan' => $request->kategori_kegiatan,
                'jenis_kegiatan' => $request->jenis_kegiatan,
                'judul_artikel' => $request->judul_artikel,
                'nama_jurnal' => $request->nama_jurnal,
                'tautan_laman_jurnal' =>$request->tautan_laman_jurnal,
                'tgl_terbit' => $request->tgl_terbit,
                'lokasi' => $request->lokasi,
                'biaya' => $request->biaya,
                'vol' => $request->vol,
                'nomor' =>$request->nomor,
                'halaman' =>$request->halaman,
                'penerbit'=>$request->penerbit,
                'doi' =>$request->doi,
                'issn' =>$request->issn,
                'tema_kegiatan' =>$request->tema_kegiatan,
                'tahun' =>$request->tahun
            ]);

            $dokumen_publikasi = DokumenPublikasi::create([
                'id_pub_karya_ilmiah' => $publikasi_karya->id_pub_karya_ilmiah,
                'id_dok' =>$dokumen->id_dok
               
            ]);

            $anggota_publikasi = AnggotaPublikasi::create([
                'id_pub_karya_ilmiah' => $publikasi_karya->id_pub_karya_ilmiah,
                'penulis1' => $request->penulis1,
                'penulis2' => $request->penulis2,
                'penulis3' => $request->penulis3,
                'jns_penulis1' => $request->id_penulis,
                'jns_penulis2' => $request->id_penulis,
                'jns_penulis3' => $request->id_penulis,
                'peran_anggota1' => $request->id_peran,
                'peran_anggota2' => $request->id_peran,
                'peran_anggota3' => $request->id_peran,
                'afiliansi' => $request->afiliansi
            ]);

            $file = $request->file;
            $mime = $file->getClientMimeType();
            $nama_asli = $file->getClientOriginalName();
            $bytea = base64_encode(file_Get_contents($file->getPathName()));

            $dokumen = new Dokumenn();
            $dokumen->nama_dok = $request->nama_dok;
            $dokumen->file = $bytea;
            $dokumen->mime_type = $mime;
            $dokumen->nama_file = $nama_asli;
            $dokumen->id_jenis = $request->id_jenis;
            $dokumen->keterangan = $request->keterangan;
            $dokumen->link_url = $request->link_url;
            $dokumen->waktu_unggah = date('Y-m-d H:i:s');
            $file->move($file->getClientOriginalName());    
            $dokumen->save();
            return redirect('/publikasikarya1');
    }
    public function delete($id)
    {
            $publikasi_karya = PublikasiKarya::find($id);
            $publikasi_karya->delete();
            return redirect('/publikasikarya1');
    }

    public function jabar(Request $request)
    {
        $pilih = $request->get('pilih');
        $nilai = $request->get('nilai');
        $depend = $request->get('depend');

        $data = DB::table('kategori_kegiatan')
            ->where($pilih,$nilai)
            ->Groupby($depend)
            ->get();
        $hasil = ' <option value="">Pilih Kategori Kegiatan'.ucfirst($depend).'</option>';
        foreach ($data as $value) {
            $hasil .= '<option value="'.$value->$depend.'">'.$value->$depend.'</option>';
        }
        echo $hasil;

    }

} 
