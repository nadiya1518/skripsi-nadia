<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Standarr;

class StandarController extends Controller
{
    public function index()
    {
    	$nama_standar = Standarr::all();
    	return view('halstan', ['standar' => $nama_standar]);
    }
    public function tst()
    {
    	return view ('tstandar');
    }
     public function store(Request $request) 
    {
 
    		Standarr::create([
    		'kode_standar' => $request->kode_standar,
    		'nama_standar' => $request->nama_standar
    	]);
 
    	return redirect('/halstan');
    }
    public function edit($id)
    {
            $standar = Standar::find($id_standar);
            return view('editstandar', ['standar' => $standar]);
    }

    public function update($id, Request $request)
    {
            $standar = Standar::find($id_standar);
            $standar->nama_standar = $request->nama_standar;
            $standar->save();
            return redirect('/halstan');
    }
    public function delete($id)
    {
            $standar = Standar::find($id_standar);
            $standar->delete();
            return redirect('/halstan');
    }
    
}
 