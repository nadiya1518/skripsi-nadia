<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Uploadd;
use App\Dokumenn;
use App\LokasiDokumen;
use App\JenisDokumen;

class ValidasiController extends Controller
{
    public function index(Request $request)
    {
        if(!$request->has('kode')){
            $kode ='DIAJUKAN';
            $status =0;
        }else{
            if($request->kode== 'DISETUJUI'){
                $kode ='DISETUJUI';
                $status =1;
            }else{
                $kode ='DITOLAK';
                $status =2;
            }
        }
        $upload = Uploadd::where('is_acc', $status)->get();
        return view('validasi1', ['upload' => $upload,'kode'=>$kode, 'status'=>$status]);
        
    }

    public function disetujui(Request $request, $id)
    {

        Uploadd::where('id_upload',$id)->update([
         'is_acc'=>1   
        ]);

        return redirect()->back();
    }
    public function ditolak(Request $request, $id)
    {
        Uploadd::where('id_upload',$id)->update([
         'is_acc'=>2   
        ]);

        return redirect()->back();
    }
}
    