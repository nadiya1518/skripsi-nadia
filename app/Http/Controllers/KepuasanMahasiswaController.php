<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KepuasanMahasiswa;
use App\JenisAspek;

class KepuasanMahasiswaController extends Controller
{
    public function index()
    {
    	$kepuasan_mahasiswa = KepuasanMahasiswa::all();
        return view('kepuasanmhs1', ['kepuasan_mahasiswa' => $kepuasan_mahasiswa]);
    }
    public function tambah()
    {  
    	$jenisaspek= JenisAspek::Orderby("jenis_aspek_kepuasan","asc")->get();
       
        return view('kepuasanmhs2',compact("jenisaspek"));
    }
    public function store(Request $request)
    {
        $kepuasan_mahasiswa = new KepuasanMahasiswa();
            $kepuasan_mahasiswa->id_jenis_aspek = $request->id_jenis_aspek;
            $kepuasan_mahasiswa->tingkat = $request->tingkat;
            $kepuasan_mahasiswa->rencana_tindak_lanjut = $request->rencana_tindak_lanjut;
            $kepuasan_mahasiswa->save();

    return redirect('/kepuasanmhs1');
    }
    public function edit($id)
    {

    $jenisaspek=JenisAspek::Orderby("jenis_aspek_kepuasan","asc")->get();
    $kepuasan_mahasiswa = KepuasanMahasiswa::find($id);
    return view('kepuasanmhs3', ['kepuasan_mahasiswa' => $kepuasan_mahasiswa], compact("jenis_aspek_kepuasan"));
    }

    public function update(Request $request, $id)
    {
            $kepuasan_mahasiswa = KepuasanMahasiswa::find($id);
            $kepuasan_mahasiswa->id_jenis_aspek = $request->id_jenis_aspek;
            $kepuasan_mahasiswa->tingkat = $request->tingkat;
            $kepuasan_mahasiswa->rencana_tindak_lanjut = $request->rencana_tindak_lanjut;
            $kepuasan_mahasiswa->save();

        return redirect('/kepuasanmhs1');
    }
     public function delete($id)
    {
            $kepuasan_mahasiswa = KepuasanMahasiswa::find($id);
            $kepuasan_mahasiswa->delete();
            return redirect('/kepuasanmhs1');
    }

}
