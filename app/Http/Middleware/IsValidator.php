<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsValidator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
    {
        //return $next($request);
        $requestedUserId = $request->route()->parameter('id');

        if(Auth::user()->role === 'validator' || Auth::user()->id == $requestedUserId) {
            return $next($request);
        }
        else{
            return response()->json(['error' => 'U Must Be Admin!'], 403);
        }
    }
}
