<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenjangPendidikan extends Model
{
    protected $table ='jenjang_pendidikan';
    protected $fillable = ['nama_jenjang'];
    protected $primaryKey ='id_jenjang';

    public $timestamps =false;
}
