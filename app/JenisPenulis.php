<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPenulis extends Model
{
    protected $table ='jenis_penulis';
    protected $fillable = ['nama_jenis_penulis'];
    protected $primaryKey ='id_penulis';

    public $timestamps =false;
}
