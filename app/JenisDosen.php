<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisDosen extends Model
{
    protected $table = 'jenis_dosen';
    protected $fillable = ['jenis_dosen'];
    protected $primaryKey = 'id_jenis_dosen';

    public $timestamps=false;
}
