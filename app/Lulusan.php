<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lulusan extends Model
{
    protected $table ='lulusan';
    protected $fillable = ['id_mhs','ipk','tanggal_bekerja','tanggal_selesai_bekerja','nama_instansi','alamat','email'];
    protected $primaryKey ='id_lulusan';

    public $timestamps =false;

     
    public function Mahasiswa() 
    {
    	return $this->belongsTo('App\Mahasiswa','id_mhs','id_mhs');
    }
}
