<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnggotaPublikasi extends Model
{
    protected $table = 'anggota_publikasi';
    protected $fillable = ['penulis1','penulis2','penulis3','peran','afiliansi'];
    protected $primaryKey = 'id_anggota_publikasi';

    public $timestamps =false;
}
