<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPrestasi extends Model
{
    protected $table = 'jenis_prestasi';
    protected $fillable = ['jenis_prestasi'];
    protected $primaryKey = 'id_prestasi';

    public $timestamps =false;

}
