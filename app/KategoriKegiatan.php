<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriKegiatan extends Model
{
    protected $table = 'kategori_kegiatan';
    protected $fillable = ['nama_kategori'];
    protected $primaryKey = 'id_kategori';

    public $timestamps=false;
} 
