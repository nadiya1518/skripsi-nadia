<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KepuasanMahasiswa extends Model
{
    protected $table = 'kepuasan_mahasiswa';
    protected $fillable = ['id_jenis_aspek','tingkat','rencana_tindak_lanjut'];
    protected $primaryKey = 'id_kepuasan_mhs';

    public $timestamps =false;

    public function JenisAspek()
    {
    	return $this->belongsTo('App\JenisAspek','id_jenis_aspek','id_jenis_aspek');
    }

}
