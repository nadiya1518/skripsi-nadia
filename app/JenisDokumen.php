<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisDokumen extends Model
{
    protected $table = 'jenis_dokumen';
    protected $fillable = ['jenis_dokumen'];
    protected $primaryKey = 'id_jenis';

    public $timestamps =false;

    public function Upload()
    {
    	return $this->belongsTo('App\Uploadd','id_standar','id_standar');
    }
    public function Dokumen()
    {
    	return $this->belongsTo('App\Dokumenn','id_dok','id_dok');
    }
}
