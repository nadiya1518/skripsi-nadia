<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SumberPembiayaan extends Model
{
    protected $table = 'sumber_pembiayaan';
    protected $fillable = ['sumber_pembiayaan'];
    protected $primaryKey = 'id_sumber_pembiaayaan';

    public $timestamps =false;
}
