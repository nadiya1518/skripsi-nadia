<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DayaTampung extends Model
{
    protected $table ='daya_tampung';
    protected $fillable = ['pelaksana','tahun_masuk','jenis_penerimaan','total_daya_tampung','pendaftar','lulus_seleksi'];
    protected $primaryKey ='id_daya_tampung';

    public $timestamps =false;

    public function JenisPenerimaan() 
    {
    	return $this->belongsTo('App\JenisPenerimaan','id_jenis_penerimaan','id_jenis_penerimaan');
    }
}
