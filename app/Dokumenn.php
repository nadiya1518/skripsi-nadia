<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokumenn extends Model
{
    protected $table = "dokumen";
 
    protected $fillable = ['id_dok','nama_dok','nama_file','file','mime_type','keterangan','link_url','tgl_upload'];

    protected $primaryKey = 'id_dok';

    public $timestamps =false;

    public function DokumenPub()
    {
    	return $this->belongsTo('App\DokumenPublikasi','id_dokumen_publikasi','id_dokumen_publikasi');
    }
    public function JenisDokumen()
    {
    	return $this->belongsTo('App\JenisDokumen','id_jenis','id_jenis');
    }
    public function Dokumen()
    {
        return $this->belongsTo('App\Dokumenn','id_dok','id_dok');
    }
    public function LokasiDokumen()
    {
        return $this->belongsTo('App\LokasiDokumen','id_lok','id_lok');
    }
    public function BakuMutu()
    {
        return $this->belongsTo('App\BakuMutu','id_baku','id_baku');
    }
    public function Standar()
    {
        return $this->belongsTo('App\Standarr','id_standar','id_standar');
    }

}
 