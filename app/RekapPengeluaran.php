<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RekapPengeluaran extends Model
{
    protected $table ='rekap_pengeluaran';
    protected $fillable = ['id_jenis_pengeluaran','waktu_pengeluaran', 'apakah_pd_upps', 'apakah_pd_ps', 'biaya'];
    protected $primaryKey = 'id_rekap_pengeluaran';

    public $timestamps =false;

    public function Pengeluaran() 
    {
    	return $this->belongsTo('App\JenisPengeluaran','id_jenis_pengeluaran','id_jenis_pengeluaran');
    }

    public function Sumber()
    {
    	return $this->belongsTo('App\SumberPembiayaan','id_sumber_pembiaayaan','id_sumber_pembiaayaan');
    }

}
