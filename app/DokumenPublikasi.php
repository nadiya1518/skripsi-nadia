<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DokumenPublikasi extends Model
{
    protected $table = 'dokumen_publikasi';
    protected $fillable = ['id_pub_karya_ilmiah'];
    protected $primaryKey = 'id_dokumen_publikasi';

    public $timestamps =false;
}
