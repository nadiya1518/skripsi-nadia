<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $table = "upload";
 
    protected $fillable = ['id_dok','baku_mutu','lokasi_dok','jenis_dok'];

    protected $primaryKey = 'id';

    public $timestamps =false;
}
