<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $table = 'mahasiswa';
    protected $fillable = ['id_jenis_penerimaan','id_prestasi_mhs','id_pub_karya_ilmiah','nama_mhs','npm','waktu_masuk','waktu_lulus','tanggal_dan_tahun_transfer','status_keaktifan','apakah_mhs_asing','jenis_mhs'];
    protected $primaryKey = 'id_mhs';

    public $timestamps =false; 

    public function JenisPenerimaan() 
    {
    	return $this->belongsTo('App\JenisPenerimaan','id_jenis_penerimaan','id_jenis_penerimaan');
    }
    public function PrestasiMahasiswa() 
    {
    	return $this->belongsTo('App\PrestasiMahasiswa','id_prestasi_mhs','id_prestasi_mhs');
    }
    public function ProgramStudi() 
    {
        return $this->belongsTo('App\ProgramStudi','id_prodi','id_prodi');
    }
}
