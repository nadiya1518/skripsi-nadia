<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengakuanDosen extends Model
{
    protected $table = 'rekognisi_dosen';
    protected $fillable = ['bukti_pendukung','tingkat','tahun'];
    protected $primaryKey = 'id_rekognisi';

    public $timestamps =false;

    public function Dosen()
    {
    	return $this->belongsTo('App\Dosen','id_dosen','id_dosen');
    }
    public function BidangKeahlian()
    {
    	return $this->belongsTo('App\BidangKeahlian','id_bidang_keahlian','id_bidang_keahlian');
    }
}
