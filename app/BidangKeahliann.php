<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BidangKeahliann extends Model
{
    protected $table = 'bidang_keahlian_dosen';
    protected $fillable = ['bid_sesuai_komp','bid_sesuai_matkul','matkul_dlm_ps_a','matkul_pd_ps_lain'];
    protected $primaryKey = 'id_bidang_dosen';

    public $timestamps=false;

    public function Dosen() 
    {
    	return $this->belongsTo('App\Dosen','id_dosen','id_dosen');
    }
    public function JenisBidang()
    {
    	return $this->belongsTo('App\JenisBidangKeahlian','id_jenis_keahlian','id_jenis_keahlian');
    }

}
