<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Standarr extends Model
{
    protected $table = 'standar';
    protected $fillable = ['kode_standar','nama_standar'];
    protected $primaryKey = 'id_standar';

    public $timestamps =false;

    public function Upload()
    {
    	return $this->belongsTo('App\Uploadd','id_standar','id_standar');
    }
    public function Dokumen()
    {
    	return $this->belongsTo('App\Dokumenn','id_dok','id_dok');
    }
}
 