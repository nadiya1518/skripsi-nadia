<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublikasiKarya extends Model
{
    protected $table ='publikasi_karya_ilmiah';
    protected $fillable = ['nama','judul','lokasi','abstrak','biaya','hal','vol','nomor','url','tahun','jurnal','tema_kegiatan','kategori_kegiatan','jenis_kegiatan','tanggal_terbit','nama_jurnal'];
    protected $primaryKey ='id_pub_karya_ilmiah';

    public $timestamps =false;

    public function KategoriKegiatan() 
    {
    	return $this->belongsTo('App\KategoriKegiatan','id_kategori','id_kategori');
    }
    public function JenisPenulis() 
    {
    	return $this->belongsTo('App\JenisPenulis','id_penulis','id_penulis');
    }
    public function JenisDokumen() 
    {
    	return $this->belongsTo('App\JenisDokumen','id_jenis','id_jenis');
    }
    public function LokasiDokumen() 
    {
    	return $this->belongsTo('App\LokasiDokumen','id_lok','id_prestasi_lok');
    }
    public function DokumenPublikasi()
    {
    	return $this->belongsTo('App\DokumenPublikasi','id_dokumen_publikasi','id_dokumen_publikasi');
    }
    public function AnggotaPublikasi()
    {
    	return $this->belongsTo('App\AnggotaPublikasi','id_anggota_publikasi','id_anggota_publikasi');
    }
    public function Dokumenn()
    {
    	return $this->belongsTo('App\Dokumenn','id_dok','id_dok');
    }
}
