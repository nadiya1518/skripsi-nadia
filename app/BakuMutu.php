<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BakuMutu extends Model
{
    protected $table = 'baku_mutu';
    protected $fillable = ['standar','butir','baku_mutu'];
    protected $primaryKey = 'id_baku';

    public $timestamps =false;

    public function Upload()
    {
    	return $this->belongsTo('App\Uploadd','id_baku','id_baku');
    }
}
  