<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kerjasama extends Model
{
    protected $table = 'kerjasama';
    protected $fillable = ['lembaga_mitra','tingkat','judul_kegiatan_kerjasama','manfaat_kerjasama','waktu_mulai_kerjasama','waktu_berakhir_kerjasama'];
    protected $primaryKey = 'id_kerjasama';

    public $timestamps=false;

    public function Dokumen()
    {
    	return $this->belongsTo('App\Dokumenn','id_dok','id_dok');
    }
}
