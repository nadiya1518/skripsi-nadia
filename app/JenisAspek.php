<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisAspek extends Model
{
    protected $table ='jenis_aspek_kepuasan_mhs';
    protected $fillable = ['jenis_aspek_kepuasan'];
    protected $primaryKey ='id_jenis_aspek';

    public $timestamps =false;
}
