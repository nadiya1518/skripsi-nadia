<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiwayatPendidikan extends Model
{
    protected $table = 'riwayat_pend_formal_dosen';
    protected $fillable = ['id_dosen','id_jenjang','perguruan_tinggi','perguruan_tinggi','fakultas','jurusan','prodi','judul_ta'];
    protected $primaryKey = 'id_riwayatt';

    public $timestamps=false;

    public function Dosen() 
    {
    	return $this->belongsTo('App\Dosen','id_dosen','id_dosen');
    }

    public function JenjangPendidikan()
    {
    	return $this->belongsTo('App\JenjangPendidikan','id_jenjang','id_jenjang');
    }
}
