<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPengeluaran extends Model
{
    protected $table = 'jenis_pengeluaran';
    protected $fillable = ['nama_jenis_pengeluaran'];
    protected $primaryKey = 'id_jenis_pengeluaran';

    public $timestamps=false;
}
