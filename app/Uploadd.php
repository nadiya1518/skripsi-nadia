<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uploadd extends Model
{
    protected $table = "upload";

    protected $fillable = ['id_dok','id_lok','id_baku','id_standar'];

    protected $primaryKey = 'id_upload';

    public $timestamps =false;

    public function Dokumen()
    {
    	return $this->belongsTo('App\Dokumenn','id_dok','id_dok');
    }
    public function LokasiDokumen()
    {
    	return $this->belongsTo('App\LokasiDokumen','id_lok','id_lok');
    }
    public function BakuMutu()
    {
    	return $this->belongsTo('App\BakuMutu','id_baku','id_baku');
    }
    public function Standar()
    {
    	return $this->belongsTo('App\Standarr','id_standar','id_standar');
    }
    public function JenisDokumen()
    {
        return $this->belongsTo('App\JenisDokumen','id_jenis','id_jenis');
    }
}

 