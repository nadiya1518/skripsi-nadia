<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrestasiMahasiswa extends Model
{
    protected $table = 'prestasi_mahasiswa';
    protected $fillable = ['nama_mhs','nama_kegiatan','waktu_perolehan','tingkat','jenis_prestasi','prestasi_yang_dicapai'];
    protected $primaryKey = 'id_prestasi_mhs';

    public $timestamps =false; 

    public function Mahasiswa() 
    {
    	return $this->belongsTo('App\Mahasiswa','id_mhs','id_mhs');
    }

    public function ProgramStudi() 
    {
    	return $this->belongsTo('App\ProgramStudi','id_prodi','id_prodi');
    }
}
