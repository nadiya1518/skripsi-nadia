<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PembimbingTa extends Model
{
    protected $table ='pembimbing_tugas_akhir';
    protected $fillable = ['id_dosen','id_prodi','id_jenis_bimbingan','id_mhs'];
    protected $primaryKey ='id_pembimbing_utama';

    public $timestamps =false;

    public function Dosen()
    {
    	return $this->belongsTo('App\Dosen','id_dosen','id_dosen');
    }
    public function Mahasiswa()
    {
    	return $this->belongsTo('App\Mahasiswa','id_mhs','id_mhs');
    }
    public function JenisBimbingan()
    {
    	return $this->belongsTo('App\JenisBimbingan','id_jenis_bimbingan','id_jenis_bimbingan');
    }
    public function ProgramStudi()
    {
    	return $this->belongsTo('App\ProgramStudi','id_prodi','id_prodi');
    }
}
