<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    protected $table = 'dosen';
    protected $fillable = ['id_jenis_dosen','id_prodi','nama_dosen','nidn'];
    protected $primaryKey = 'id_dosen';

    public $timestamps =false;

    public function JenisDosen() 
    {
    	return $this->belongsTo('App\JenisDosen','id_jenis_dosen','id_jenis_dosen');
    }
    public function ProgramStudi() 
    {
        return $this->belongsTo('App\ProgramStudi','id_prodi','id_prodi');
    }
    
}

