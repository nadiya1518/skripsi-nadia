<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPenerimaan extends Model
{
    protected $table ='jenis_penerimaan';
    protected $fillable = ['jenis_penerimaan'];
    protected $primaryKey ='id_jenis_penerimaan';

    public $timestamps =false;
}
