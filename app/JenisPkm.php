<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPkm extends Model
{
    protected $table = 'jenis_pkm';
    protected $fillable = ['nama_jenis_pkm'];
    protected $primaryKey = 'id_jenis_pkm';

    public $timestamps =false;
}
